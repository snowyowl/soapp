--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: plpgsql; Type: PROCEDURAL LANGUAGE; Schema: -; Owner: -
--

CREATE PROCEDURAL LANGUAGE plpgsql;


SET search_path = public, pg_catalog;

--
-- Name: add_group(text, text, text, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION add_group(p_title text, p_upload_dir text, p_users text, p_created_by integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
  s_name text;
  n_role integer;
  n_users integer[];
  i integer;
BEGIN
  SELECT INTO s_name fmt_group(p_title);
  SELECT INTO n_role role FROM auth_user WHERE id = p_created_by;
  IF n_role <> 1 THEN
    RAISE EXCEPTION 'You have no permission to create %.', s_name;
  ELSE
    INSERT INTO auth_group (id, title, upload_dir) 
      VALUES (NEXTVAL('seq_gid'), p_title, p_upload_dir);
    n_users = string_to_array(p_users,',');
    FOR i IN 1 .. array_upper(n_users, 1) LOOP
      INSERT INTO auth_usergroup (user_id, group_id) VALUES (n_users[i], CURRVAL('seq_gid'));
    END LOOP;
    RAISE NOTICE 'The % has been created successfully.', s_name;
    RETURN CURRVAL('seq_gid');
  END IF;
END;$$;


--
-- Name: add_project(text, text, integer, integer, text, text, text, text, integer, integer, integer, integer, integer, integer, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION add_project(p_title text, p_description text, p_gid integer, p_uid integer, p_mrna text, p_genome text, p_rnaseq text, p_masked text, p_phred64 integer, p_tophat integer, p_maskseq integer, p_refseq integer, p_imin integer, p_imax integer, p_output text) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
  s_name text;
  n_output integer[];
BEGIN
  INSERT INTO core_project (id, title, description, group_id, created_on, created_by)
    VALUES (NEXTVAL('seq_pid'), p_title, p_description, p_gid, CURRENT_TIMESTAMP, p_uid);
  INSERT INTO core_project_param (project_id, mrna, genome, rnaseq, masked, phred64, tophat, maskseq, refseq, imin, imax)
    VALUES (CURRVAL('seq_pid'), p_mrna, p_genome, p_rnaseq, p_masked, p_phred64, p_tophat, p_maskseq, p_refseq, p_imin, p_imax);
	INSERT INTO core_project_status (project_id) VALUES (CURRVAL('seq_pid'));
    n_output = string_to_array(p_output,',');
    FOR i IN 1 .. array_upper(n_output, 1) LOOP
      INSERT INTO core_project_file (project_id, file_id) VALUES (CURRVAL('seq_pid'), n_output[i]);
    END LOOP;
  SELECT INTO s_name fmt_project(id, title) FROM core_project WHERE id = CURRVAL('seq_pid');
  RAISE NOTICE 'The % has been created successfully.', s_name;
	RETURN CURRVAL('seq_pid');
END;$$;


--
-- Name: add_user(text, text, text, integer, text, integer, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION add_user(p_name text, p_username text, p_pass text, p_role integer, p_groups text, p_created_by integer, p_email text) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
  s_name text;
  n_role integer;
  n_groups integer[];
  i integer;
BEGIN
  SELECT INTO s_name fmt_account(p_name, p_username);
  SELECT INTO n_role role FROM auth_user WHERE id = p_created_by;
  IF n_role <> 1 THEN
    RAISE EXCEPTION 'You have no permission to modify %.', s_name;
  ELSE
    INSERT INTO auth_user (id, name, username, pass, role, email) VALUES (NEXTVAL('seq_uid'), p_name, p_username, p_pass, p_role, p_email);
		n_groups = string_to_array(p_groups,',');
    FOR i IN 1 .. array_upper(n_groups, 1) LOOP
      INSERT INTO auth_usergroup (user_id, group_id) VALUES (CURRVAL('seq_uid'), n_groups[i]);
    END LOOP;
    RAISE NOTICE 'The % has been created successfully.', s_name;
    RETURN CURRVAL('seq_uid');
  END IF;
END;$$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    name text NOT NULL,
    username text NOT NULL,
    pass character(32) NOT NULL,
    login_count integer DEFAULT 0 NOT NULL,
    login_last timestamp with time zone,
    status integer DEFAULT 1 NOT NULL,
    registered_on timestamp(6) with time zone DEFAULT now() NOT NULL,
    role integer DEFAULT 0 NOT NULL,
    email text NOT NULL,
    notify integer DEFAULT 0 NOT NULL
);


--
-- Name: TABLE auth_user; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE auth_user IS 'User login accounts.';


--
-- Name: COLUMN auth_user.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_user.id IS 'user identification number';


--
-- Name: COLUMN auth_user.name; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_user.name IS 'user''s full name (e.g. John Doe)';


--
-- Name: COLUMN auth_user.username; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_user.username IS 'user login name';


--
-- Name: COLUMN auth_user.pass; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_user.pass IS 'user login password (encrpted: md5)';


--
-- Name: COLUMN auth_user.login_count; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_user.login_count IS 'number of successful login attempts';


--
-- Name: COLUMN auth_user.login_last; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_user.login_last IS 'the timestamp of the last successful login';


--
-- Name: COLUMN auth_user.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_user.status IS 'user account current status (see table user_status) ';


--
-- Name: COLUMN auth_user.registered_on; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_user.registered_on IS 'user account creation timestamp';


--
-- Name: COLUMN auth_user.role; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_user.role IS 'user role (see table auth_role)';


--
-- Name: COLUMN auth_user.email; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_user.email IS 'user email address';


--
-- Name: COLUMN auth_user.notify; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_user.notify IS '1 = send email notifications';


--
-- Name: authenticate(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION authenticate(p_username text, p_pass text) RETURNS auth_user
    LANGUAGE plpgsql
    AS $$DECLARE
   details auth_user;

BEGIN
  
  SELECT * INTO details FROM auth_user
    WHERE username = p_username AND pass = p_pass;

  IF FOUND THEN
    IF details.status = 1 THEN
      UPDATE auth_user SET login_count = login_count + 1, login_last = CURRENT_TIMESTAMP
        WHERE id = details.id;
      RETURN details;
    ELSE
			RAISE EXCEPTION 'user account is not active';
      RETURN NULL;
    END IF;
  ELSE
    RETURN NULL;
  END IF;
END
$$;


--
-- Name: change_notification(integer, text, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION change_notification(p_user_id integer, p_email text, p_notify integer, p_changed_by integer) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
  s_name text;
  n_role integer;
BEGIN
  SELECT INTO s_name fmt_account(name, username) FROM auth_user WHERE id = p_user_id;
  SELECT INTO n_role role FROM auth_user WHERE id = p_changed_by;
  IF n_role <> 1 AND p_user_id <> p_changed_by THEN
    RAISE EXCEPTION 'You have no permission to change email notification settings for %.', s_name;
  ELSIF p_notify NOT IN (0,1) THEN
    RAISE EXCEPTION 'Invilid value: "%" for parameter: "p_notify". Must be 0 or 1.', p_notify;
  ELSE
    UPDATE auth_user SET email = p_email, notify = p_notify WHERE id = p_user_id;
    RAISE NOTICE 'The email notification settings for % have been changed successfully.', s_name;
  END IF;
EXCEPTION
	WHEN unique_violation THEN
		RAISE EXCEPTION 'The email address: "%" is already in use, please enter anther.', p_email;
END;$$;


--
-- Name: change_password(integer, text, text, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION change_password(p_user_id integer, p_pass text, p_confirm text, p_changed_by integer) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
  s_name text;
  n_role integer;
BEGIN
  SELECT INTO s_name fmt_account(name, username) FROM auth_user WHERE id = p_user_id;
  SELECT INTO n_role role FROM auth_user WHERE id = p_changed_by;
  IF n_role <> 1 AND p_user_id <> p_changed_by THEN
    RAISE EXCEPTION 'You have no permission to change password for %.', s_name;
  ELSIF p_pass <> p_confirm THEN
    RAISE EXCEPTION 'Cannot change password for %. Password and confirmation do not match.', s_name;
  ELSE
    -- password and confirmation should be encrypted using md5 algorithm
    -- do not send password to database in clear text (in case db will be hosted on separate machine)
    UPDATE auth_user SET pass = p_pass WHERE id = p_user_id;
    RAISE NOTICE 'The password for % has been changed successfully.', s_name;
  END IF;
END;$$;


--
-- Name: check_usergroup(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION check_usergroup() RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
  r_user record;
BEGIN
	-- for each user in the given group
	FOR r_user IN SELECT user_id FROM auth_usergroup LOOP
		-- check if the user belongs to at least one other active group
    IF NOT EXISTS (
      SELECT * FROM auth_group
      JOIN auth_usergroup
        ON auth_group.id = auth_usergroup.group_id
      WHERE user_id = r_user.user_id
        AND auth_group.status = 1
		) THEN
			RETURN r_user.user_id;
		END IF;
	END LOOP;
  RETURN NULL;
END;$$;


--
-- Name: delete_group(integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION delete_group(p_group_id integer, p_deleted_by integer) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
  s_name text;
  s_user text;
  n_role integer;
  n_projects integer;
  n_user_id integer;
BEGIN
  SELECT INTO s_name fmt_group(title) FROM auth_group WHERE id = p_group_id;
  SELECT INTO n_role role FROM auth_user WHERE id = p_deleted_by;
  SELECT INTO n_projects COUNT(*) FROM core_project WHERE group_id = p_group_id;
  IF n_role <> 1 THEN
    RAISE EXCEPTION 'You have no permission to delete %.', s_name;
  ELSIF NOT EXISTS (SELECT id FROM auth_group WHERE id = p_group_id) THEN
    RAISE EXCEPTION 'Group id: % does not exist.', p_group_id;
  ELSIF n_projects > 0 THEN
    RAISE EXCEPTION 'The % cannot be deleted. The group has one or more projects. Consider disabling the group.', s_name;
  ELSE
    DELETE FROM auth_usergroup WHERE group_id = p_group_id;
    DELETE FROM auth_group WHERE id = p_group_id;
    SELECT INTO n_user_id check_usergroup();
    IF n_user_id IS NOT NULL THEN
			SELECT INTO s_user fmt_account(name, username) FROM auth_user WHERE id = n_user_id;
			RAISE EXCEPTION 'The % cannot be deleted. The % is not in any other active group.', s_name, s_user;
		END IF;
	RAISE NOTICE 'The % has been deleted successfully.', s_name;
  END IF;
END;$$;


--
-- Name: delete_project(integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION delete_project(p_project_id integer, p_deleted_by integer) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
  s_name text;
	n_role integer;
  n_status integer;
  n_owner integer;
BEGIN
  SELECT INTO s_name fmt_project(id, title) FROM core_project WHERE id = p_project_id;
  SELECT INTO n_status status FROM core_project_status WHERE project_id = p_project_id;
  SELECT INTO n_role role FROM auth_user WHERE id = p_deleted_by;
  SELECT INTO n_owner created_by FROM core_project WHERE id = p_project_id;
  IF n_role <> 1 AND n_owner <> p_deleted_by THEN
    RAISE EXCEPTION 'You have no permission to delete %.', s_name;
	ELSIF n_status = 1 THEN
    RAISE EXCEPTION 'The % cannot be deleted. The project is currently running.', s_name;
	ELSE
    EXECUTE delete_project(p_project_id);
    RAISE NOTICE 'The % has been deleted successfully.', s_name;
	END IF;
END;$$;


--
-- Name: delete_project(integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION delete_project(p_project_id integer) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
BEGIN
    DELETE FROM core_project_progress WHERE project_id = p_project_id;
    DELETE FROM core_project_status WHERE project_id = p_project_id;
    DELETE FROM core_project_param WHERE project_id = p_project_id;
    DELETE FROM core_project_file WHERE project_id = p_project_id;
    DELETE FROM core_project WHERE id = p_project_id;
    INSERT INTO project_deleted (project_id) VALUES (p_project_id);
END;$$;


--
-- Name: delete_user(integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION delete_user(p_user_id integer, p_deleted_by integer) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
  s_name text;
  n_role integer;
	n_projects integer;
BEGIN
  SELECT INTO s_name fmt_account(name, username) FROM auth_user WHERE id = p_user_id;
  SELECT INTO n_projects COUNT(id) FROM core_project WHERE created_by = p_user_id;
  SELECT INTO n_role role FROM auth_user WHERE id = p_deleted_by;
  IF n_role <> 1 THEN
    RAISE EXCEPTION 'You have no permission to delete %.', s_name;
  ELSIF n_projects > 0 THEN
    RAISE EXCEPTION 'The % cannot be deleted. The user has one or more projects. Consider disabling the account.', s_name;
  ELSE
    DELETE FROM auth_usergroup WHERE user_id = p_user_id;
    DELETE FROM auth_user WHERE id = p_user_id;
    RAISE NOTICE 'The % has been deleted successfully.', s_name;
  END IF;
END;$$;


--
-- Name: fmt_account(text, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION fmt_account(p_name text, p_username text) RETURNS text
    LANGUAGE plpgsql
    AS $$BEGIN
  RETURN 'user account: "' || p_name || '" <' || p_username || '>';
END;$$;


--
-- Name: fmt_group(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION fmt_group(p_title text) RETURNS text
    LANGUAGE plpgsql
    AS $$BEGIN
  RETURN 'group: "' || p_title || '"';
END;$$;


--
-- Name: fmt_project(integer, text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION fmt_project(p_project_id integer, p_title text) RETURNS text
    LANGUAGE plpgsql
    AS $$BEGIN
  RETURN 'project: "' || p_title || '" <Project ID ' || p_project_id || '>';
END;$$;


--
-- Name: modify_group(integer, text, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION modify_group(p_group_id integer, p_users text, p_status integer, p_modified_by integer) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
  s_name text;
  s_user text;
  n_role integer;
  n_users integer[];
  i integer;
  n_user_id integer;
BEGIN
  SELECT INTO s_name fmt_group(title) FROM auth_group WHERE id = p_group_id;
  SELECT INTO n_role role FROM auth_user WHERE id = p_modified_by;
  IF n_role <> 1 THEN
    RAISE EXCEPTION 'You have no permission to modify %.', s_name;
  ELSE
    UPDATE auth_group SET status = p_status WHERE id = p_group_id;
    n_users = string_to_array(p_users,',');
    DELETE FROM auth_usergroup WHERE group_id = p_group_id;
    FOR i IN 1 .. array_upper(n_users, 1) LOOP
      INSERT INTO auth_usergroup (user_id, group_id) VALUES (n_users[i], p_group_id);
    END LOOP;
    SELECT INTO n_user_id check_usergroup();
    IF n_user_id IS NOT NULL THEN
      SELECT INTO s_user fmt_account(name, username) FROM auth_user WHERE id = n_user_id;
			RAISE EXCEPTION 'The % cannot be modified. The % is not in any other active group.', s_name, s_user;
    END IF;
    RAISE NOTICE 'The % has been modified successfully.', s_name;
  END IF;
END;$$;


--
-- Name: modify_user(integer, integer, text, integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION modify_user(p_user_id integer, p_modified_by integer, p_groups text, p_role integer, p_status integer) RETURNS void
    LANGUAGE plpgsql
    AS $$DECLARE
  s_name text;
  n_role integer;
	n_groups integer[];
	i integer;
BEGIN
  SELECT INTO s_name fmt_account(name, username) FROM auth_user WHERE id = p_user_id;
  SELECT INTO n_role role FROM auth_user WHERE id = p_modified_by;
  IF n_role <> 1 THEN
    RAISE EXCEPTION 'You have no permission to modify %.', s_name;
  ELSE
    -- modify account
    UPDATE auth_user SET role = p_role, status = p_status WHERE id = p_user_id;
    DELETE FROM auth_usergroup WHERE user_id = p_user_id;
    n_groups = string_to_array(p_groups,',');
    FOR i IN 1 .. array_upper(n_groups, 1) LOOP
      INSERT INTO auth_usergroup (user_id, group_id) VALUES (p_user_id, n_groups[i]);
    END LOOP;
    RAISE NOTICE 'The % has been modified successfully.', s_name;
  END IF;
END;$$;


--
-- Name: restart_project(integer, integer); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION restart_project(p_project_id integer, p_restarted_by integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
  s_name text;
	n_role integer;
  n_status integer;
BEGIN
	IF NOT EXISTS (SELECT 1 FROM core_project WHERE id = p_project_id) THEN
    RAISE EXCEPTION 'Project id % does not exist.', p_project_id;
  END IF; 
  SELECT INTO s_name fmt_project(id, title) FROM core_project WHERE id = p_project_id;
  SELECT INTO n_status status FROM core_project_status WHERE project_id = p_project_id;
  SELECT INTO n_role role FROM auth_user WHERE id = p_restarted_by;
  IF n_role <> 1 THEN
    RAISE EXCEPTION 'You have no permission to restart %.', s_name;
	ELSIF n_status NOT IN (2,3) THEN
    RAISE EXCEPTION 'The % cannot be restarted. The project is either running, pending, or completed.', s_name;
	ELSE
    DELETE FROM core_project_progress WHERE project_id = p_project_id;
    UPDATE core_project_status 
      SET status = 0, started_on = NULL, completed_on = NULL 
      WHERE project_id = p_project_id;
    RAISE NOTICE 'The % has been restarted successfully.', s_name;
		RETURN p_project_id;
	END IF;
END;$$;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    title text NOT NULL,
    upload_dir text NOT NULL,
    status integer DEFAULT 1 NOT NULL
);


--
-- Name: TABLE auth_group; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE auth_group IS 'User groups.';


--
-- Name: COLUMN auth_group.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_group.id IS 'user group identification number';


--
-- Name: COLUMN auth_group.title; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_group.title IS 'user group display name';


--
-- Name: COLUMN auth_group.upload_dir; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_group.upload_dir IS 'user group upload directory (base)name';


--
-- Name: COLUMN auth_group.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_group.status IS 'user group current status (see table user_status)';


--
-- Name: auth_role; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_role (
    id integer NOT NULL,
    title text NOT NULL
);


--
-- Name: TABLE auth_role; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE auth_role IS 'User account roles.';


--
-- Name: COLUMN auth_role.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_role.id IS 'user role identification number';


--
-- Name: COLUMN auth_role.title; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_role.title IS 'the display name of the role';


--
-- Name: auth_usergroup; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE auth_usergroup (
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


--
-- Name: TABLE auth_usergroup; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE auth_usergroup IS 'Group memberships.';


--
-- Name: COLUMN auth_usergroup.user_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_usergroup.user_id IS 'user identification number';


--
-- Name: COLUMN auth_usergroup.group_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN auth_usergroup.group_id IS 'group identification number';


--
-- Name: core_project; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_project (
    id integer NOT NULL,
    title text NOT NULL,
    description text NOT NULL,
    group_id integer NOT NULL,
    created_on timestamp with time zone NOT NULL,
    created_by integer NOT NULL
);


--
-- Name: TABLE core_project; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE core_project IS 'SnowyOwl projcets.';


--
-- Name: COLUMN core_project.id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project.id IS 'project identification number';


--
-- Name: COLUMN core_project.title; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project.title IS 'project title (may contain spaces, keep it short)';


--
-- Name: COLUMN core_project.description; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project.description IS 'long description of the project';


--
-- Name: COLUMN core_project.group_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project.group_id IS 'the identification number of the group to which the project belongs.';


--
-- Name: COLUMN core_project.created_on; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project.created_on IS 'the timestamp of the creation of the project.';


--
-- Name: COLUMN core_project.created_by; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project.created_by IS 'the identification number of the user who submitted the project.';


--
-- Name: core_project_file; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_project_file (
    project_id integer NOT NULL,
    file_id integer NOT NULL,
    size integer DEFAULT 0 NOT NULL
);


--
-- Name: core_project_param; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_project_param (
    project_id integer NOT NULL,
    genome text NOT NULL,
    rnaseq text NOT NULL,
    masked text,
    phred64 integer,
    tophat integer NOT NULL,
    mrna text,
    maskseq integer DEFAULT 0 NOT NULL,
    refseq integer NOT NULL,
    imin integer NOT NULL,
    imax integer NOT NULL
);


--
-- Name: TABLE core_project_param; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE core_project_param IS 'Project parameters set by the user.';


--
-- Name: COLUMN core_project_param.project_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project_param.project_id IS 'project identification number';


--
-- Name: COLUMN core_project_param.genome; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project_param.genome IS 'the name of the whole genome sequence file in FASTA (not including any directory path)';


--
-- Name: COLUMN core_project_param.rnaseq; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project_param.rnaseq IS 'the name of the RNA-Seq reads file in FASTQ (not including any directory path)';


--
-- Name: COLUMN core_project_param.masked; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project_param.masked IS 'the name of the masekd genome sequence file in FASTA (not including any directory path)';


--
-- Name: COLUMN core_project_param.phred64; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project_param.phred64 IS 'FASTQ quality score type (0 = phred33, 1 = phred64)';


--
-- Name: COLUMN core_project_param.tophat; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project_param.tophat IS 'mapping package to use (1=tophat,0=tuque)';


--
-- Name: COLUMN core_project_param.mrna; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project_param.mrna IS 'preassembled transcripts in FASTA filename';


--
-- Name: COLUMN core_project_param.maskseq; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project_param.maskseq IS '0=dont mask, >0=mask %identity';


--
-- Name: core_project_progress; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_project_progress (
    project_id integer NOT NULL,
    progress_id integer NOT NULL,
    completed_on timestamp with time zone DEFAULT now() NOT NULL
);


--
-- Name: TABLE core_project_progress; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE core_project_progress IS 'Project current progress. Every row represents a completed step.';


--
-- Name: COLUMN core_project_progress.project_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project_progress.project_id IS 'project identification number';


--
-- Name: COLUMN core_project_progress.progress_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project_progress.progress_id IS 'progress step identification number (see table project_progress)';


--
-- Name: COLUMN core_project_progress.completed_on; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project_progress.completed_on IS 'step completion timestamp';


--
-- Name: core_project_status; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_project_status (
    project_id integer NOT NULL,
    status integer DEFAULT 0 NOT NULL,
    started_on timestamp with time zone,
    completed_on timestamp with time zone
);


--
-- Name: TABLE core_project_status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE core_project_status IS 'Project current status.';


--
-- Name: COLUMN core_project_status.project_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project_status.project_id IS 'project identification number';


--
-- Name: COLUMN core_project_status.status; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project_status.status IS 'project current status (see projcet_status)';


--
-- Name: COLUMN core_project_status.started_on; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project_status.started_on IS 'the timestamp at which the last project run was started';


--
-- Name: COLUMN core_project_status.completed_on; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN core_project_status.completed_on IS 'the timestamp at which the last successful project run was completed';


--
-- Name: core_refseq; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE core_refseq (
    id integer NOT NULL,
    name text NOT NULL,
    title text NOT NULL
);


--
-- Name: group_status; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE group_status (
    status integer NOT NULL,
    title text NOT NULL
);


--
-- Name: project_deleted; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE project_deleted (
    project_id integer NOT NULL
);


--
-- Name: TABLE project_deleted; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON TABLE project_deleted IS 'Stores identification numbers of projects that have been removed from the core_project table and require later removal from the filesystem.';


--
-- Name: COLUMN project_deleted.project_id; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON COLUMN project_deleted.project_id IS 'project identification number';


--
-- Name: project_file; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE project_file (
    id integer NOT NULL,
    name text NOT NULL,
    filename text NOT NULL,
    description text NOT NULL,
    required integer DEFAULT 0 NOT NULL,
    file_group integer NOT NULL
);


--
-- Name: project_filegroup; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE project_filegroup (
    id integer NOT NULL,
    name text NOT NULL
);


--
-- Name: project_progress; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE project_progress (
    id integer NOT NULL,
    title text NOT NULL
);


--
-- Name: project_status; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE project_status (
    status integer NOT NULL,
    title text NOT NULL
);


--
-- Name: seq_gid; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE seq_gid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: SEQUENCE seq_gid; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON SEQUENCE seq_gid IS 'Group identification number generator.';


--
-- Name: seq_pid; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE seq_pid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: SEQUENCE seq_pid; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON SEQUENCE seq_pid IS 'Project identification number generator.';


--
-- Name: seq_uid; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE seq_uid
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


--
-- Name: SEQUENCE seq_uid; Type: COMMENT; Schema: public; Owner: -
--

COMMENT ON SEQUENCE seq_uid IS 'User identification number generator.';


--
-- Name: user_status; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE user_status (
    status integer NOT NULL,
    title text NOT NULL
);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_group_title_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_title_key UNIQUE (title);


--
-- Name: auth_role_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_role
    ADD CONSTRAINT auth_role_pkey PRIMARY KEY (id);


--
-- Name: auth_usergroup_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_usergroup
    ADD CONSTRAINT auth_usergroup_pkey PRIMARY KEY (user_id, group_id);


--
-- Name: core_project_file_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_project_file
    ADD CONSTRAINT core_project_file_pkey PRIMARY KEY (project_id, file_id);


--
-- Name: core_project_param_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_project_param
    ADD CONSTRAINT core_project_param_pkey PRIMARY KEY (project_id);


--
-- Name: core_project_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_project
    ADD CONSTRAINT core_project_pkey PRIMARY KEY (id);


--
-- Name: core_project_progress_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_project_progress
    ADD CONSTRAINT core_project_progress_pkey PRIMARY KEY (project_id, progress_id);


--
-- Name: core_project_progress_project_id_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_project_progress
    ADD CONSTRAINT core_project_progress_project_id_key UNIQUE (project_id, progress_id);


--
-- Name: core_project_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_project_status
    ADD CONSTRAINT core_project_status_pkey PRIMARY KEY (project_id);


--
-- Name: core_project_title_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_project
    ADD CONSTRAINT core_project_title_key UNIQUE (title, group_id);


--
-- Name: core_refseq_name_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_refseq
    ADD CONSTRAINT core_refseq_name_key UNIQUE (name);


--
-- Name: core_refseq_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY core_refseq
    ADD CONSTRAINT core_refseq_pkey PRIMARY KEY (id);


--
-- Name: group_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY group_status
    ADD CONSTRAINT group_status_pkey PRIMARY KEY (status);


--
-- Name: project_deleted_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY project_deleted
    ADD CONSTRAINT project_deleted_pkey PRIMARY KEY (project_id);


--
-- Name: project_file_name_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY project_file
    ADD CONSTRAINT project_file_name_key UNIQUE (name);


--
-- Name: project_file_path_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY project_file
    ADD CONSTRAINT project_file_path_key UNIQUE (filename);


--
-- Name: project_file_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY project_file
    ADD CONSTRAINT project_file_pkey PRIMARY KEY (id);


--
-- Name: project_filegroup_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY project_filegroup
    ADD CONSTRAINT project_filegroup_pkey PRIMARY KEY (id);


--
-- Name: project_progress_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY project_progress
    ADD CONSTRAINT project_progress_pkey PRIMARY KEY (id);


--
-- Name: project_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY project_status
    ADD CONSTRAINT project_status_pkey PRIMARY KEY (status);


--
-- Name: user_email_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT user_email_key UNIQUE (email);


--
-- Name: user_name_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT user_name_key UNIQUE (name);


--
-- Name: user_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: user_status_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY user_status
    ADD CONSTRAINT user_status_pkey PRIMARY KEY (status);


--
-- Name: user_username_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT user_username_key UNIQUE (username);


--
-- Name: auth_group_status_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_status_fkey FOREIGN KEY (status) REFERENCES group_status(status) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: auth_user_role_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_role_fkey FOREIGN KEY (role) REFERENCES auth_role(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: auth_user_status_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_status_fkey FOREIGN KEY (status) REFERENCES user_status(status) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: auth_usergroup_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_usergroup
    ADD CONSTRAINT auth_usergroup_group_id_fkey FOREIGN KEY (group_id) REFERENCES auth_group(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: auth_usergroup_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY auth_usergroup
    ADD CONSTRAINT auth_usergroup_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: core_project_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY core_project
    ADD CONSTRAINT core_project_created_by_fkey FOREIGN KEY (created_by) REFERENCES auth_user(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: core_project_file_file_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY core_project_file
    ADD CONSTRAINT core_project_file_file_id_fkey FOREIGN KEY (file_id) REFERENCES project_file(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: core_project_file_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY core_project_file
    ADD CONSTRAINT core_project_file_project_id_fkey FOREIGN KEY (project_id) REFERENCES core_project(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: core_project_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY core_project
    ADD CONSTRAINT core_project_group_id_fkey FOREIGN KEY (group_id) REFERENCES auth_group(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: core_project_param_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY core_project_param
    ADD CONSTRAINT core_project_param_project_id_fkey FOREIGN KEY (project_id) REFERENCES core_project(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: core_project_param_refseq_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY core_project_param
    ADD CONSTRAINT core_project_param_refseq_fkey FOREIGN KEY (refseq) REFERENCES core_refseq(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: core_project_progress_progress_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY core_project_progress
    ADD CONSTRAINT core_project_progress_progress_id_fkey FOREIGN KEY (progress_id) REFERENCES project_progress(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: core_project_progress_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY core_project_progress
    ADD CONSTRAINT core_project_progress_project_id_fkey FOREIGN KEY (project_id) REFERENCES core_project(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: core_project_status_project_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY core_project_status
    ADD CONSTRAINT core_project_status_project_id_fkey FOREIGN KEY (project_id) REFERENCES core_project(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: core_project_status_status_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY core_project_status
    ADD CONSTRAINT core_project_status_status_fkey FOREIGN KEY (status) REFERENCES project_status(status) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: project_file_file_group_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY project_file
    ADD CONSTRAINT project_file_file_group_fkey FOREIGN KEY (file_group) REFERENCES project_filegroup(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- PostgreSQL database dump complete
--

