<?php
/**
 * Get list of users in a given group.
 * 
 * @package SnowyOwl
 * @subpackage Ajax
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 * @see /www/js/group/list.js
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

$_Engine->requireAdmin();

$params = array();
$params[] = (int) $_REQUEST['id'];
$sql = "SELECT auth_user.id, 
               auth_user.name,
               auth_user.username 
        FROM auth_usergroup
        JOIN auth_user
          ON auth_user.id = auth_usergroup.user_id
        WHERE auth_usergroup.group_id = $1";
$_Engine->assign('users', $_Engine->selectAll($sql, $params));
$_Engine->data = $_Engine->template('group/users.tpl');

?>
