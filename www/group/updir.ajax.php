<?php
/**
 * Check group upload directory.
 * 
 * Make sure that the path is a readible directory within the global upload
 * root directory as specified by the {updir_root} configuration directive.
 * 
 * @package SnowyOwl
 * @subpackage Ajax
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 * @see /www/js/group/add.js
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

$_Engine->requireAdmin();

$dirpath = realpath(DS . $_REQUEST['updir']); // do not accept relative paths
$root = realpath($_Engine->config('updir_root'));
$_Engine->data = is_dir($dirpath) && strpos($dirpath, $root) !== false && $dirpath != $root;
        
?>
