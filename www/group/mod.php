<?php
/**
 * Modify User Group Form.
 * 
 * @package SnowyOwl
 * @subpackage Interface
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

require_once LIB . 'core.func.php';

$_Engine->requireAdmin();

if (!isset($_REQUEST['id']) || !is_numeric($_REQUEST['id'])) {
    header('Location: list.php');
    exit();
}

$group = CORE_get_group($_REQUEST['id']);

// add array of ids of group members
$group['users'] = array();
foreach (CORE_get_users($_REQUEST['id']) as $user) {
    array_push($group['users'], $user['id']);
}

//var_dump($group, CORE_get_users());

$_Engine->assign('statuses', CORE_get_user_statuses());
$_Engine->assign('users', CORE_get_users());
$_Engine->assign('group', $group);

?>
