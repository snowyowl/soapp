<?php
/**
 * Check group title.
 * 
 * @package SnowyOwl
 * @subpackage Ajax
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 * @see /www/js/group/add.js
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

$_Engine->requireAdmin();

$params = array();
$params[] = trim($_REQUEST['title']);
$sql = "SELECT id FROM auth_group WHERE title = $1";
$res = $_Engine->query($sql, $params);
$_Engine->data = ($res->num_rows() == 0);

?>
