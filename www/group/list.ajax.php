<?php
/**
 * Group List.
 * 
 * This script provides table data for the Group List interface file.
 * 
 * @package SnowyOwl
 * @subpackage Ajax
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 * @see /www/group/list.php
 * @see /www/js/group/list.js
 * @uses AjaxTable
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

$_Engine->requireAdmin();


$params = array();

$sql = 'SELECT auth_group.id AS "ID",
               auth_group.title AS "Title",
               auth_group.upload_dir AS "Upload Directory",
               group_status.title AS "Status",
               COUNT(auth_user.id) AS "Total Users"
        FROM auth_group
        JOIN auth_usergroup ON auth_usergroup.group_id = auth_group.id
        JOIN auth_user ON auth_usergroup.user_id = auth_user.id
        JOIN group_status ON auth_group.status = group_status.status
        GROUP BY auth_group.id, auth_group.title, 
                 auth_group.upload_dir, group_status.title';

$projectTable = new AjaxTable();
$projectTable->submitQuery($sql, $params);
$projectTable->sendJSON();

?>
