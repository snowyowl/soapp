<?php
/**
 * Group List.
 * 
 * @package SnowyOwl
 * @subpackage Interface
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

require_once LIB . 'core.func.php';

$_Engine->requireAdmin();

$_Engine->includeStyle('jquery.ajaxtable.css');
$_Engine->includeScript('jquery.ajaxtable.js');

if (isset($_REQUEST['addGroup'])) {
    CORE_add_group();
} else if (isset($_REQUEST['delGroup'])) {
    CORE_delete_group($_REQUEST['id']);
} else if (isset($_REQUEST['modGroup'])) {
    CORE_modify_group($_REQUEST['id']);
}

?>
