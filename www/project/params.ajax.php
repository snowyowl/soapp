<?php
/**
 * Report project progress (completed steps).
 * 
 * @package SnowyOwl
 * @subpackage Ajax
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 * @see /www/js/user/list.js
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

$params = array();
$params[] = (int) $_REQUEST['id'];
$sql = "SELECT core_project_param.*, core_refseq.title AS refseq_db
        FROM core_project_param
        JOIN core_refseq
            ON core_project_param.refseq = core_refseq.id
        WHERE project_id = $1";
$row = $_Engine->selectOne($sql, $params);
$pDir = DAT . 'project' . DS . $_REQUEST['id'] . DS;
$row['genome'] = array('name' => $row['genome'], 'size' => filesize($pDir . $row['genome']));
$row['rnaseq'] = array('name' => $row['rnaseq'], 'size' => filesize($pDir . $row['rnaseq']));
if ($row['masked'])
    $row['masked'] = array('name' => $row['masked'], 'size' => filesize($pDir . $row['masked']));
if ($row['mrna'])
    $row['mrna'] = array('name' => $row['mrna'], 'size' => filesize($pDir . $row['mrna']));

$_Engine->assign('params', $row);
$_Engine->data = $_Engine->template('project/params.tpl');

?>
