<?php
/**
 * Fetch Project Results.
 * 
 * @package SnowyOwl
 * @subpackage Interface
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

if (!isset($_REQUEST['id'])) {
    header('Location: list.php');
}

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';
require_once LIB . 'core.func.php';

$_Engine->requireLogin();

$pid = (int) $_REQUEST['id'];

$params = array($pid);
$sql = "SELECT status FROM core_project_status WHERE project_id = $1";
$row = $_Engine->selectOne($sql, $params);
if ($row['status'] != P_COMPLETED) {
    $_Engine->data = "<p>Results files are not yet available for this project.</p>";
    exit();
}

$output = CORE_get_output_files($_REQUEST['id']);
if (empty($output)) {
    $_Engine->data = "<p>There are no result files available for this project.</p>";
    exit();
}

$_Engine->assign('project', CORE_get_project($_REQUEST['id']));
$_Engine->assign('output', $output);
$_Engine->data = $_Engine->template('project/output.tpl');

?>
