<?php
/**
 * Project List.
 * 
 * @package SnowyOwl
 * @subpackage Interface
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

require_once LIB . 'core.func.php';

$_Engine->requireLogin();

$_Engine->data = false;

if (isset($_REQUEST['id']) && isset($_REQUEST['file'])) {
    $_Engine->data = CORE_delete_output_file($_REQUEST['id'], $_REQUEST['file']);
}

?>
