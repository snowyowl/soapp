<?php
/**
 * Project List.
 * 
 * This script provides table data for the Project List interface file.
 * 
 * @package SnowyOwl
 * @subpackage Ajax
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 * @see /www/project/list.php
 * @see /www/js/project/list.js
 * @uses AjaxTable
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

$_Engine->requireLogin();

// save results per page setting
$_SESSION['rpp'] = $_REQUEST['limit'];

$params = array();
$params[0] = (int) $_SESSION['user']['id'];

$sql = 'SELECT core_project.id AS "ID",
               core_project.title AS "Title",
               core_project.description AS "Description",
               auth_group.title AS "Group",
               TO_CHAR(core_project.created_on, \'Mon DD, YYYY HH24:MI\') || \' (\' || auth_user.name || \')\' AS "Created",
               project_status.title AS "Status"
        FROM core_project
        JOIN core_project_status
            ON core_project.id = core_project_status.project_id
        JOIN project_status 
            ON core_project_status.status = project_status.status
        JOIN auth_usergroup 
            ON core_project.group_id = auth_usergroup.group_id
        JOIN auth_group 
            ON auth_usergroup.group_id = auth_group.id
        JOIN auth_user 
            ON core_project.created_by = auth_user.id
        WHERE auth_usergroup.user_id = $1';

$projectTable = new AjaxTable();
$projectTable->submitQuery($sql, $params);
$projectTable->sendJSON();

?>
