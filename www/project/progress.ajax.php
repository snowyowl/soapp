<?php
/**
 * Report project progress (completed steps).
 * 
 * @package SnowyOwl
 * @subpackage Ajax
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 * @see /www/js/user/list.js
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

$params = array();
$params[] = (int) $_REQUEST['id'];
$sql = "SELECT id, 
               title, 
               TO_CHAR(completed_on, 'DD-Mon-YYYY (Dy) HH24:MI') AS completed_on
        FROM project_progress
        LEFT JOIN core_project_progress 
          ON id = progress_id 
          AND project_id = $1
          ORDER BY id";
$_Engine->assign('steps', $_Engine->selectAll($sql, $params));

$sql = "SELECT project_id as id,
               status IN (2, 3) AS restart,
               TO_CHAR(started_on, 'DD-Mon-YYYY (Dy) HH24:MI') AS started_on,
               TO_CHAR(completed_on, 'DD-Mon-YYYY (Dy) HH24:MI') AS completed_on
        FROM core_project_status
        WHERE project_id = $1";
$_Engine->assign('project', $_Engine->selectOne($sql, $params));

$_Engine->data = $_Engine->template('project/progress.tpl');

?>
