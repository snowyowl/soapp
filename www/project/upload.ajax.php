<?php
/**
 * Upload sequence file.
 * 
 * @package SnowyOwl
 * @subpackage Ajax
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 * @see /www/js/project/add.js
 */

require_once '../../init.php';

$_Engine->requireLogin();

$result = array();

$filename = UPL . $_REQUEST['uuid'] . '_' . basename($_REQUEST['qqfilename']);

$totalParts = (int) $_REQUEST['qqtotalparts'];
$partIndex = (int) $_REQUEST['qqpartindex'];
$uuid = $_REQUEST['qquuid'];

$targetDir = UPL . 'chunks' . DS . $uuid;
if (!file_exists($targetDir)) {
    mkdir($targetDir);
    @chmod($targetDir, 02770);
}

$target = $targetDir . DS . $partIndex;


if (move_uploaded_file($_FILES['qqfile']['tmp_name'], $target)) {
    
    @chmod($target, 0660);
    
    // combine chuncks
    if ($partIndex == $totalParts - 1) {
        
        $dst = fopen($filename, 'wb');
        
        for ($i = 0; $i < $totalParts; $i++) {
            $src = fopen($targetDir . DS . $i, "rb");
            stream_copy_to_stream($src, $dst);
            fclose($src);
        }
        
        fclose($dst);
        
        // give owner and group rw access
        @chmod($filename, 0660);
        
        // clean up
        for ($i = 0; $i < $totalParts; $i++) {
            unlink($targetDir . DS . $i);
        }
        
        rmdir($targetDir);
    }
    
    $result['success'] = true;
} else {
    $result['error'] = 'failed to save uploaded file.';
}

$_Engine->data = $result;
        
?>
