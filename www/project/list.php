<?php
/**
 * Project List.
 * 
 * @package SnowyOwl
 * @subpackage Interface
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

require_once LIB . 'core.func.php';

$_Engine->requireLogin();

$_Engine->includeStyle('jquery.ajaxtable.css');
$_Engine->includeScript('jquery.ajaxtable.js');

if (isset($_REQUEST['addProject'])) {
    CORE_add_project();
} else if (isset($_REQUEST['delProject']) && isset($_REQUEST['id'])) {
    CORE_delete_project($_REQUEST['id']);
} else if (isset($_REQUEST['restart'])) {
    CORE_restart_project($_REQUEST['restart']);
}

?>
