<?php
/**
 * Check project title.
 * 
 * @package SnowyOwl
 * @subpackage Ajax
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 * @see /www/js/project/add.js
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

$_Engine->requireLogin();

$params = array();
$params[] = trim($_REQUEST['title']);
$params[] = (int) $_REQUEST['gid'];
$sql = "SELECT id FROM core_project WHERE title = $1 and group_id = $2";
$res = $_Engine->query($sql, $params);
$_Engine->data = ($res->num_rows() == 0);
        
?>
