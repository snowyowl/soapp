<?php
/**
 * Download Project Results.
 * 
 * @package SnowyOwl
 * @subpackage Interface
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

if (!isset($_REQUEST['id']) || !isset($_REQUEST['file'])) {
    header('Location: list.php');
    exit();
}

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';
require_once LIB . 'core.func.php';

$_Engine->requireLogin();

$filename = CORE_get_output_filepath($_REQUEST['id'], $_REQUEST['file']);

if (file_exists($filename)) {
    $_Engine->addFile($filename);
} else {
    header('Location: output.php?id=' . $_REQUEST['id']);
    exit();
}

?>
