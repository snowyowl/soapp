<?php
/**
 * Add Project Form.
 * 
 * @package SnowyOwl
 * @subpackage Interface
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

require_once LIB . 'core.func.php';

$_Engine->requireLogin();

$_Engine->includeScript('jquery.fineuploader-3.5.0.js');
$_Engine->includeStyle('fineuploader-3.5.0.css');

$_Engine->assign('groups', CORE_get_user_groups());
$_Engine->assign('refseq', CORE_get_refseq_dbs());
$_Engine->assign('output', CORE_get_output_files());

$_Engine->assign('uuid1', uniqid()); // form 1 unique id
$_Engine->assign('uuid2', uniqid()); // form 2 unique id

?>
