<?php
/**
 * Fetch user session.
 * 
 * @package SnowyOwl
 * @subpackage Ajax
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
require_once '../init.php';

$_Engine->data = $_SESSION;

?>
