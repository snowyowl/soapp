<?php
/**
 * Snowy Owl Home Page.
 *  
 * @package SnowyOwl
 * @subpackage Interface
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../init.php';

?>
