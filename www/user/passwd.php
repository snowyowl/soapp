<?php
/**
 * Change Password Form.
 * 
 * @package SnowyOwl
 * @subpackage Interface
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

require_once '../../init.php';
require_once LIB . 'core.func.php';

if (isset($_REQUEST['id'])) {
    $_Engine->requireAdmin();
} else {
    $_Engine->requireLogin();
}

$user_id = isset($_REQUEST['id']) ? (int) $_REQUEST['id'] : $_SESSION['user']['id'];
if (isset($_REQUEST['changePassword'])) {
    CORE_change_user_password($user_id);
}

$_Engine->assign('user', CORE_get_user($user_id));

?>
