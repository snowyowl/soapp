<?php
/**
 * User Accounts List.
 * 
 * This script provides table data for the User Account List interface file.
 * 
 * @package SnowyOwl
 * @subpackage Ajax
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 * @see /www/user/list.php
 * @see /www/js/user/list.js
 * @uses AjaxTable
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

$_Engine->requireAdmin();


$params = array();

$sql = 'SELECT auth_user.id AS "ID",
               auth_user.name || \' (\' || auth_user.username || \')\'  AS "User",
               auth_user.email AS "Email",
               auth_user.login_count || \' (\' || TO_CHAR(auth_user.login_last, \'Mon DD, YYYY HH24:MI\') || \')\' AS "Logins (Lastest)",
               auth_role.title AS "Role",
               user_status.title AS "Status",
               COUNT(auth_usergroup.group_id) AS "Groups"
        FROM auth_user
        JOIN auth_usergroup 
            ON auth_user.id = auth_usergroup.user_id
        JOIN auth_role 
            ON auth_user.role = auth_role.id
        JOIN user_status
            ON auth_user.status = user_status.status
        GROUP BY auth_user.id, 
                 auth_user.name, 
                 auth_user.username, 
                 auth_user.email, 
                 auth_user.login_count, 
                 auth_user.login_last,
                 auth_role.title,
                 user_status.title';

$projectTable = new AjaxTable();
$projectTable->submitQuery($sql, $params);
$projectTable->sendJSON();

?>
