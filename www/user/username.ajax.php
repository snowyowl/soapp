<?php
/**
 * Check user account username.
 * 
 * Check if the username exists on the in the password file of the 
 * file (sftp) server (providing access to group shares / upload directories)
 * 
 * @package SnowyOwl
 * @subpackage Ajax
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 * @see /www/js/user/add.js
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

$_Engine->requireAdmin();

$username = trim($_REQUEST['username']);

// check file server
$cmd = escapeshellcmd("id $username");
$output = shell_exec($cmd);
$exists_on_system = !empty($output);

// check database
$params = array($username);
$sql = "SELECT id FROM auth_user WHERE username = $1";
$res = $_Engine->query($sql, $params);
$row = $res->fetch();
$exists_in_database = !empty($row);

if (!$exists_on_system) {
    $_Engine->data = 'Username does not exist on the file server.';
} else if ($exists_in_database) {
    $_Engine->data = 'Username is already in use.';
} else {
    $_Engine->data = true;
}
    


?>
