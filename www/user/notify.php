<?php
/**
 * Change My Password Form.
 * 
 * @package SnowyOwl
 * @subpackage Interface
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';
require_once LIB . 'core.func.php';

$_Engine->requireLogin();

if (isset($_REQUEST['changeNotification'])) {
    CORE_change_email_notification();
}

$_Engine->assign('user', CORE_get_user($_SESSION['user']['id']));

?>
