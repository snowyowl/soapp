<?php
/**
 * Add Project Form.
 * 
 * @package SnowyOwl
 * @subpackage Interface
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

require_once LIB . 'core.func.php';

$_Engine->requireAdmin();

$_Engine->assign('groups', CORE_get_groups());
$_Engine->assign('roles', CORE_get_roles());

?>
