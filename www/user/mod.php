<?php
/**
 * Modify User Account Form.
 * 
 * @package SnowyOwl
 * @subpackage Interface
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

require_once LIB . 'core.func.php';

$_Engine->requireAdmin();

if (!isset($_REQUEST['id']) || !is_numeric($_REQUEST['id'])) {
    header('Location: list.php');
    exit();
}

// mark groups for which the user is currently a member of
$groups = CORE_get_groups();
$user_groups = CORE_get_user_groups($_REQUEST['id']);

for ($i = 0; $i < count($groups); $i++) {
    $groups[$i]['user'] = false;
    foreach ($user_groups as $user_group) {
        if ($groups[$i]['id'] == $user_group['id']) {
            $groups[$i]['user'] = true;
        }
    }
}

$_Engine->assign('groups', $groups);
$_Engine->assign('roles', CORE_get_roles());
$_Engine->assign('statuses', CORE_get_user_statuses());
$_Engine->assign('user', CORE_get_user($_REQUEST['id']));

?>
