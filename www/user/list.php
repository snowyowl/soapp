<?php
/**
 * User Account List.
 * 
 * @package SnowyOwl
 * @subpackage Interface
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

require_once LIB . 'core.func.php';

$_Engine->requireAdmin();

$_Engine->includeStyle('jquery.ajaxtable.css');
$_Engine->includeScript('jquery.ajaxtable.js');

if (isset($_REQUEST['addUser'])) {
    CORE_add_user();
} else if (isset($_REQUEST['delUser']) && isset($_REQUEST['id'])) {
    CORE_delete_user($_REQUEST['id']);
} else if (isset($_REQUEST['modUser']) && isset($_REQUEST['id'])) {
    CORE_modify_user($_REQUEST['id']);
}

?>
