<?php
/**
 * Check user account email.
 * 
 * Check if the email address provided has not been used already by
 * another account.
 * 
 * @package SnowyOwl
 * @subpackage Ajax
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 * @see /www/js/user/add.js
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

$_Engine->requireAdmin();

$params = array();
$params[] = trim($_REQUEST['email']);
$sql = "SELECT id FROM auth_user WHERE email = $1";
$res = $_Engine->query($sql, $params);
$_Engine->data = ($res->num_rows() == 0);
        
?>
