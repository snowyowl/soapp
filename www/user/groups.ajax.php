<?php
/**
 * Generate a list of groups for a given user (in html).
 * 
 * @package SnowyOwl
 * @subpackage Ajax
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 * @see /www/js/user/list.js
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

$_Engine->requireAdmin();

$params = array();
$params[] = (int) $_REQUEST['id'];
$sql = "SELECT auth_group.id, 
               TRIM(auth_group.title) AS title
        FROM auth_usergroup
        JOIN auth_group
          ON auth_group.id = auth_usergroup.group_id
        WHERE auth_usergroup.user_id = $1";
$_Engine->assign('groups', $_Engine->selectAll($sql, $params));
$_Engine->data = $_Engine->template('user/groups.tpl');

?>
