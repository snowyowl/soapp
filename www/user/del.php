<?php
/**
 * Delete User Account Form.
 * 
 * @package SnowyOwl
 * @subpackage Interface
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * Initialize SnowOwl Engine
 */
require_once '../../init.php';

require_once LIB . 'core.func.php';

$_Engine->requireAdmin();

if (!isset($_REQUEST['id']) || !is_numeric($_REQUEST['id'])) {
    header('Location: list.php');
    exit();
}

$_Engine->assign('user', CORE_get_user($_REQUEST['id']));

?>
