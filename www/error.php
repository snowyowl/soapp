<?php
/**
 * Snowy Owl Error Page.
 *  
 * @package SnowyOwl
 * @subpackage Interface
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

require_once '../init.php';

$_Engine->alert("SnowyOwl encountered an error.", Engine::ERROR);

?>
