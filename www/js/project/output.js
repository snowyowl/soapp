$(function() {
    
   $('#toggleAll').toggle(
      function (e) {
          $('input[type=checkbox]').prop('checked', false);
      }, 
      function (e) {
          $('input[type=checkbox]').prop('checked', true);
      }
   );
    
});