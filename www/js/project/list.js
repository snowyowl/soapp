$(function() {
    
    var session = {
        rpp: 10
    };
    
    $.ajax({
        url: '../session.ajax.php',
        success: function (data) {
            session = data;
        },
        async: false
    });
    
    $('#projectList').ajaxtable({
        
        source: '/project/list.ajax.php',
        caption: 'Projects',
        sortby: {
            5: 'created_on'
        },
        limit: session.rpp,
        actions: [
            
        {
            type: 'dropdown',
            name: 'Restart',
            handler: function(id, row) {
                var yes = confirm(
                    'Are you sure you want to restart this project?\nAll generated data will be destroyed.\n');
                if (yes) {
                    window.location = '/project/list.php?restart=' + id;
                }
            }
        },
            
        {
            type: 'dropdown',
            name: 'Delete',
            handler: function(id, row) {
                window.location = 'del.php?id=' + id;
            }
        },
                        
        {
            type: 'column',
            colnum: 1,
            title: 'Show/Hide Project Parameters',
            handler: function(id, row, colnum) {
                var expClass = 'expand-col-' + colnum + '-' + id;
                if (row.nextAll('tr').hasClass(expClass)) {
                    row.nextAll('tr.' + expClass).remove();
                } else {
                    $.ajax({
                        url: '/project/params.ajax.php',
                        data: {
                            id: id
                        },
                        success: function (data) {
                            row.after($('<tr>').addClass(expClass).append(
                                $('<td>').attr('colspan', 7).html(data)
                                ));
                        }
                    });
                }
            }
        },
            
        {
            type: 'column',
            colnum: 2,
            title: 'Show/Hide Project Results',
            handler: function(id, row, colnum) {
                var expClass = 'expand-col-' + colnum + '-' + id;
                if (row.nextAll('tr').hasClass(expClass)) {
                    row.nextAll('tr.' + expClass).remove();
                } else {                    
                    $.ajax({
                        url: '/project/output.ajax.php',
                        data: {
                            id: id
                        },
                        success: function (data) {
                            row.after($('<tr>').addClass(expClass).append(
                                $('<td>').attr('colspan', 7).html(data)
                                ));
                        }
                    });
                }
            }
        },
            
        {
            type: 'column',
            colnum: 6,
            title: 'Show/Hide Project Progress',
            handler: function(id, row, colnum) {
                var expClass = 'expand-col-' + colnum + '-' + id;
                if (row.nextAll('tr').hasClass(expClass)) {
                    row.nextAll('tr.' + expClass).remove();
                } else {                    
                    $.ajax({
                        url: '/project/progress.ajax.php',
                        data: {
                            id: id
                        },
                        success: function (data) {
                            row.after($('<tr>').addClass(expClass).append(
                                $('<td>').attr('colspan', 7).html(data)
                                ));
                        }
                    });
                }
            }
        }
            
        ]

    });
    
});

function deleteOutputFile(project_id, file_id, colnum) {
        
    if (confirm('Are you sure you want to delete the file permanently?')) {
        $.ajax({
           url: 'output_del.ajax.php',
           data: {
               id: project_id,
               file: file_id
           },
           success: function (data) {
               if (data) {
                   $('tr.expand-col-' + colnum + '-' + project_id).prev().find('.at-col-' + colnum + ' a').click().click();
               } else {
                   alert('error: cannot delete file');
               }
           }
        });
    }
}