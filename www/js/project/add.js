var FASTA = ['fa', 'fsa', 'fna', 'fasta', 'fa.gz', 'fsa.gz', 'fna.gz', 'fasta.gz'];
var FASTQ = ['fq', 'fastq', 'fq.gz', 'fastq.gz'];

var errMsg = {
    title: 'Please provide a short title for your project.',
    titleRemote: 'This project title has already been used by this group. Please choose another.',
    genome: 'Please upload a target genome file.',
    rnaseq: 'Please upload an RNA-Seq reads file.',
    masked: 'Please provide a FASTA file.',
    mrna: 'Please upload a pre-assembled transcripts file.'
}

function validateForm(form) {

    return form.validate({ 
        ignore: '.ignore',
        messages: {
            title: { required: errMsg.title, remote: errMsg.titleRemote },
            genome: { required: errMsg.genome },
            rnaseq: { required: errMsg.rnaseq },
            mrna: { required: errMsg.mrna },
            masked: { required: errMsg.masked }
        },
        rules: {
            title: { required: true, remote: { url: '/project/title.ajax.php', type: 'post', data: { gid: function() { return $('#gid-a').val() } } } },
            maskseq: { range: [90,100] },
            imin: { min: 0, max: parseInt($('input[name=imax]', form).val()) - 1 },
            imax: { min: parseInt($('input[name=imin]', form).val()) + 1 },
            masked: { required: { depends: function(e) { return $('input[name=mask_it]:checked', form).val() != "0"; } } }
        }
    });
}

function createUploader(id, allowedExtensions, targetTextField, uuid) {
    return $('#' + id).fineUploader({
        request: {
            endpoint: '/project/upload.ajax.php',
            params: {
                'uuid': uuid,
                'exts': allowedExtensions
            }
        },
        chunking: {
            enabled: true
        },
        validation: {
            allowedExtensions: allowedExtensions
        },
        text: {
            uploadButton: 'Upload'
        },
        multiple: false
    }).on('complete', function(event, id, filename, response) {
        $('#' + targetTextField).val(filename);
    }).on('cancel', function(event, id, filename) {
        $('#' + targetTextField).val('');
    });
}

$(function() {

    $(document).tooltip();
    $('#tabs').tabs();
    $('input.uploader').hide();
        
    // create "Select All" button
    $('#form-output table.output').before($('<button type="button">Select All</button>').click(function (e) { 
        e.preventDefault();
        $('#form-output input[type=checkbox]').prop('checked', true);
    }));

    // create "Unselect All" button
    $('#form-output table.output').before($('<button type="button">Unselect All</button>').click(function (e) {
        e.preventDefault();
        $('#form-output input[type=checkbox]:enabled').prop('checked', false);
    }));
        
    // list accepted formats
    var sPrefix = 'accepted filename extentions: '
    $('.ext-fasta').text(sPrefix + FASTA.join(' '));
    $('.ext-fastq').text(sPrefix + FASTQ.join(' '));
        
    // create uploaders for form a
    var nUniqeID_A = $('#uuid-a').val();
    createUploader('mrna-uploader-a', FASTA, 'mrna-a', nUniqeID_A);
    createUploader('genome-uploader-a', FASTA, 'genome-a', nUniqeID_A);
    createUploader('masked-uploader-a', FASTA, 'masked-a', nUniqeID_A);
    createUploader('rnaseq-uploader-a', FASTQ, 'rnaseq-a', nUniqeID_A);

    // create uploaders for form b
    var nUniqeID_B = $('#uuid-b').val();
    createUploader('genome-uploader-b', FASTA, 'genome-b', nUniqeID_B);
    createUploader('masked-uploader-b', FASTA, 'masked-b', nUniqeID_B);
    createUploader('rnaseq-uploader-b', FASTQ, 'rnaseq-b', nUniqeID_B);

    // validate forms
    var formA = validateForm($('#form-a'));
    var formB = validateForm($('#form-b'));
    
    // fetch output options
    $('#form-a').add('#form-b').submit(function(e) {
        var outputFiles = [];
        $('#form-output input:checked').each(function(i, checked) {
            outputFiles.push($(checked).val());
        });
        $('#output-a').add('#output-b').val(outputFiles.join(','));
        return true;
    });
    
    // reset form after switching tabs
    $('#tabs').find('ul a').click(function(e) {
       formA.resetForm();
       formB.resetForm();
    });
        
    // fix tab indexing for form a
    $('#gid-a').attr('tabindex', 1);
    $('#title-a').attr('tabindex', 2);
    $('#desc-a').attr('tabindex', 3);
    $('#genome-uploader-a input[type=file]').attr('tabindex', 4);
    $('#mask_it-0-a').attr('tabindex', 5);
    $('#mask_it-1-a').attr('tabindex', 6);
    $('#mask_it-2-a').attr('tabindex', 7);
    $('#masked-uploader-a input[type=file]').attr('tabindex', 8);
    $('#maskseq-a input[type=file]').attr('tabindex', 9);
    $('#rnaseq-uploader-a input[type=file]').attr('tabindex', 10);
    $('#mrna-uploader-a input[type=file]').attr('tabindex', 11);
    $('#map-a').attr('tabindex', 12);
    $('#phred64-a').attr('tabindex', 13);
    $('#refseq-a').attr('tabindex', 14);
    $('#imin-a').attr('tabindex', 15);
    $('#imax-a').attr('tabindex', 16);
    $('#add-project-a').attr('tabindex', 17);
    
    // fix tab indexing for form b
    $('#gid-b').attr('tabindex', 1);
    $('#title-b').attr('tabindex', 2);
    $('#desc-b').attr('tabindex', 3);
    $('#genome-uploader-b input[type=file]').attr('tabindex', 4);
    $('#mask_it-0-b').attr('tabindex', 5);
    $('#mask_it-1-b').attr('tabindex', 6);
    $('#mask_it-2-b').attr('tabindex', 7);
    $('#masked-uploader-b input[type=file]').attr('tabindex', 8);
    $('#maskseq-b input[type=file]').attr('tabindex', 9);
    $('#rnaseq-uploader-b input[type=file]').attr('tabindex', 10);
    $('#mrna-uploader-b input[type=file]').attr('tabindex', 11);
    $('#map-b').attr('tabindex', 12);
    $('#phred64-b').attr('tabindex', 13);
    $('#refseq-b').attr('tabindex', 14);
    $('#imin-b').attr('tabindex', 15);
    $('#imax-b').attr('tabindex', 16);
    $('#add-project-b').attr('tabindex', 17);
    
    // toggle masking field (form a)
    $('#masked-field-a').hide();    
    $('#maskseq-field-a').hide();    
    $('#mask_it-0-a').click(function(e) {
        $('#masked-field-a').hide();
        $('#maskseq-field-a').hide();
    });
    $('#mask_it-1-a').click(function(e) {
        $('#masked-field-a').show();
        $('#maskseq-field-a').show();
    });
    $('#mask_it-2-a').click(function(e) {
        $('#masked-field-a').show();
        $('#maskseq-field-a').hide();
    });
    
    // toggle masking field (form b)
    $('#masked-field-b').hide();    
    $('#maskseq-field-b').hide();    
    $('#mask_it-0-b').click(function(e) {
        $('#masked-field-b').hide();
        $('#maskseq-field-b').hide();
    });
    $('#mask_it-1-b').click(function(e) {
        $('#masked-field-b').show();
        $('#maskseq-field-b').show();
    });
    $('#mask_it-2-b').click(function(e) {
        $('#masked-field-b').show();
        $('#maskseq-field-b').hide();
    });
    
    
    // toggle fastq quality score field
    $('#phred64-a').add('#phred64-b').parents('tr').hide();
    $('#map-a').change(function(e) {
       $('#phred64-a').parents('tr').toggle();
    });
    $('#map-b').change(function(e) {
       $('#phred64-b').parents('tr').toggle();
    });
});