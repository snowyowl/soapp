$(function() {
    
    $('#groupList').ajaxtable({
        
        source: '/group/list.ajax.php',
        caption: 'User Groups',

        actions: [
            
        {
            type: 'dropdown',
            name: 'Modify',
            handler: function(id, row) {
                window.location = 'mod.php?id=' + id
            }
        },
            
        {
            type: 'dropdown',
            name: 'Delete',
            handler: function(id, row) {
                window.location = 'del.php?id=' + id
            }
        },
                        
        {
            type: 'column',
            colnum: 5,
            handler: function(id, row, colnum) {
                    
                if (row.nextAll('tr').hasClass('expand-col-' + colnum)) {
                    row.nextAll('tr.expand-col-' + colnum).remove();
                } else {
                    $.ajax({
                        url: '/group/users.ajax.php',
                        data: {
                            id: id
                        },
                        success: function (data) {
                            row.after($('<tr>').addClass('expand-col-' + colnum).append(
                                $('<td>').attr('colspan', 6).html(data)
                                ));
                        }
                    });
                }
            }
        }
            
        ]

    });    
});
    