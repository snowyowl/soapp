$(function() {
    
    $('#addGroupForm').validate({
   
        messages: {
            title: {
                required: 'Please provide a title for the group.',
                remote: 'This group name is already in use. Please use another.'
            },
            updir: {
                required: 'Please provide a path (absolute) to an upload directory.',
                remote: 'This path is not accesible (check if path is readible and within {updir_root}).'
            },
            users: 'Please choose at least one user to add to the group.'
        },
        
        rules: {
            title: {
                remote: {
                    url: '/group/title.ajax.php',
                    type: 'post'
                }
            },
            updir: {
                remote: {
                    url: '/group/updir.ajax.php',
                    type: 'post'
                }
            }
        }
    });
    
});