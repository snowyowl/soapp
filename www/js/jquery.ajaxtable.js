/**
 * AjaxTable 0.1
 * 
 * Author: Omar Zabaneh <zabano@gmail.com>
 */

(function( $ ) {
    
    $.fn.ajaxtable = function(options) {
        
        // check for source
        if (options.source == undefined)
            return _error("option: 'source' not provided");
        
        /**
         * private members
         */
        var $_this = null;
        var $_rows = [];
        var $_ncol = 0;
        var $_offset = 0;
        var $_total = 0;
        var $_icol = 0; // sort column
        var $_limits = [5, 10, 15, 20, 25, 50];
        var $_asc = false;
        var $_dropdown = [];
        var $_handlers = {};
        var $_titles = {}; // column action title
        var $_labels = {
            'first': $('<a>').attr('href', '').text('<<'),
            'previous': $('<a>').attr('href', '').text('<'),
            'next': $('<a>').attr('href', '').text('>'),
            'last': $('<a>').attr('href', '').text('>>')
        };
        
        var $_callback = function () { 
            _updateTable() 
        };
        
        // set options
        var opts = $.extend({}, $.fn.ajaxtable.defaults, options);
        
        // import actions
        _importActions();

        
        // implementation
        return this.filter('div').each(function() {
            
            $_this = $(this);

            _initTable();
                        
        });
        
        /**
         * private functions
         */
        
        function _error(msg) {
            console.error('[ajaxtable:error] ' + msg);
        }
        
        function _warn(msg) {
            console.warn('[ajaxtable:warning] ' + msg);
        }
                        
        function _updateHeader() {
            
            if (opts.headers.length == 0) return;
            
            var $row = $('<tr>').addClass('at-table-header');
            for (var i = (opts.showrowid) ? 0 : 1; i < opts.headers.length; i++) {
                var $th = $('<th>').addClass('at-col-' + (i + 1));
                if (opts.sortable) {
                    $th.append($('<a>').click({
                        icol: (opts.sortby[i+1] == undefined) ? i : opts.sortby[i+1],
                        asc: !$_asc
                    }, function (e) {
                        e.preventDefault();
                        $_icol = e.data.icol;
                        $_asc = e.data.asc;
                        _fetchData(true);
                    }).attr('href', '').text(opts.headers[i]));
                } else {
                    $th.text(opts.headers[i]);
                }
                $row.append($th)
            }
            
            if ($_dropdown.length > 0) $row.append($('<th>')).addClass('at-action');
            
            $('thead', $_this).append($row);
        }
        
        function _updatePagination() {
            
            if (!opts.paginate) return;
           
            var from = $_offset + 1;
            var to = $_offset + opts.limit;
            if (to > $_total) to = $_total;
            var display = 'Displaying ' + from + ' to ' + to + ' of ' + $_total;
           
            // results per page dropdown
            var page = Math.floor($_offset / opts.limit) + 1;
            var pages = Math.ceil($_total / opts.limit);
            var $limits = $('<select>').change(function (e) {
                $_offset = 0;
                opts.limit = parseInt($(e.target).val());
                $_callback();
            }).addClass('at-page-show');
            for (var i = 0; i < $_limits.length; i++) {
                var $opt = $('<option>').attr('value', $_limits[i]).text($_limits[i]);
                if ($_limits[i] == opts.limit) $opt.attr('selected', 'selected');
                $limits.append($opt);
            }
           
            // override labels if necessary
            if (opts.paginateLabels != undefined) {
                $_labels.first = $(opts.paginateLabels.first);
                $_labels.previous = $(opts.paginateLabels.previous);
                $_labels.next = $(opts.paginateLabels.next);
                $_labels.last = $(opts.paginateLabels.last);
            }
                      
            var $pagination = $('<tr>').addClass('at-table-pagination').append(
                $('<td>').attr('colspan', $_ncol)
                .append($limits)
                .append($_labels.first.addClass('at-page-first').click(function(e) {
                    e.preventDefault();
                    $_offset = 0;
                    $_callback();
                }))                    
                .append($_labels.previous.addClass('at-page-previous').click(function(e) {
                    e.preventDefault();
                    $_offset -= opts.limit;
                    if ($_offset < 0) $_offset = 0;
                    $_callback();
                }))
                .append('<span>Page</span>')
                .append($('<input>').attr('type', 'text').val(page).addClass('at-page-jump').bind('keypress', function(e) {
                    if(e.keyCode == 13) {
                        $_offset = ($(this).val() * opts.limit) - opts.limit;
                        if ($_offset < 0 || isNaN($_offset)) $_offset = 0;
                        else if ($_offset >= $_total) $_offset = (Math.ceil($_total / opts.limit) * opts.limit) - opts.limit;
                        $_callback();
                    }
                }))
                .append('<span>of ' + pages + '</span>')
                .append($_labels.next.addClass('at-page-next').click(function(e) {
                    e.preventDefault();
                    $_offset += opts.limit;
                    if ($_offset >= $_total) $_offset -= opts.limit;
                    $_callback();
                }))
                .append($_labels.last.addClass('at-page-last').click(function(e) {
                    e.preventDefault();
                    $_offset = (Math.ceil($_total / opts.limit) * opts.limit) - opts.limit;
                    $_callback();
                }))
                .append($('<span>').text(display))
                );
           
            // pagination row placement
            if (opts.paginatePlacement == 'top') {
                $('thead', $_this).append($pagination);
            } else if (opts.paginatePlacement == 'top') {
                $('thead', $_this).append($pagination);
            } else {
                $('thead', $_this).append($pagination);
                $('tfoot', $_this).append($pagination.clone(true, true));
                if (opts.paginatePlacement != 'both' )
                    _warn("invalid paginatePlacement value: '" + opts.paginatePlacement + "' (ignored)");
            }           
        }
        
        function _fetchData() {
            
            var $asc = ($_asc) ? 1 : 0;
            
            $.ajax({
                url: options.source,
                type: 'post',
                dataType: 'json',
                data: {
                    limit: opts.limit,
                    offset: $_offset,
                    sortby: $_icol,
                    asc: $asc
                },
                success: function(data) {
                    
                    $_rows = data.rows;
                    $_total = data.total;
                    $_ncol = data.rows[0].length;
                    
                    if (opts.headers.length == 0 && data.headers != undefined) 
                        opts.headers = data.headers;
                    
                    _updateTable();
                }
            });
        }
        
        function _updateTable() {
            
            // extract rows (local and preload modes)
            if ($.isArray(opts.source)) {
                $_rows = opts.source.slice($_offset, $_offset + opts.limit);
            }
            
            // adjust number of columns to display
            if (!opts.showrowid) $_ncol--;
            if (opts.actions.length != 0) $_ncol++;
            
            // reset table
            $('tbody', $_this).empty();
            $('thead', $_this).empty();
            $('tfoot', $_this).empty();
            
            // add data rows
            for (var i = 0; i < $_rows.length; i++) {
                
                // create row
                var $row = $('<tr>').addClass('at-table-data');
                
                // set even/odd class tags
                if (i % 2 == 0) {
                    $row.addClass('at-table-even');                    
                } else {
                    $row.addClass('at-table-odd');
                }
                
                // calculate default row width
                var w = Math.floor(100/$_ncol) + '%'

                // traverse row data
                for (var j = (opts.showrowid) ? 0 : 1; j < $_rows[i].length; j++) {
                    
                    // get cell content
                    var $content = $_rows[i][j];
                    
                    // check for column actions
                    if ($_handlers[j] != undefined) {
                        $content = $('<a>').click({ 
                            index: j, 
                            id: $_rows[i][0],
                            row: $row
                        }, function(e) {
                            e.preventDefault();
                            $_handlers[e.data.index](e.data.id, e.data.row, e.data.index + 1);
                        }).attr('href', '#').attr('title', $_titles[j]).text($_rows[i][j]);
                    }
                    
                    // append to row
                    $row.append($('<td>').addClass('at-col-' + (j + 1)).html($content));
                
                }
                
                // add dropdown actions
                if ($_dropdown.length != 0) {
                    
                    // create dropdown
                    var $dropdown = $('<select>').change({
                        row: $row
                    }, function (e) {
                        $_handlers[$('option:selected', e.target).text()]($(e.target).val(), e.target.row);
                        $(e.target).val(0);
                    }).append($('<option>').val(0).text('Action'))
                    
                    // add action labels
                    $.each($_dropdown, function(k, name) {
                        $dropdown.append($('<option>').val($_rows[i][0]).text(name));
                    });
                    
                    // append to row
                    $row.append($('<td>').addClass('at-action').append($dropdown));
                }
                
                // append tbody row
                $('tbody', $_this).append($row);
                
            }

            _updateHeader();
            _updatePagination();
        }
        
        function _importActions() {
            
            $.each(opts.actions, function(i, action) {

                // set action type to 'dropdown' by default
                if (action.type == undefined) action.type = 'dropdown';
                    
                // check handler
                if (action.handler == undefined)
                    return _error("parameter: 'handler' not set for action");
                
                if (action.type == 'dropdown') {
                    if (action.name == undefined)
                        return _error("parameter: 'name' not set for dropdown action");
                    $_handlers[action.name] = action.handler;
                    $_dropdown.push(action.name)
                    
                } else if (action.type == 'column') {
                    
                    if (action.colnum == undefined) {
                        action.colnum = 0;
                    }
                    
                    var j = parseInt(action.colnum) - 1;
                    $_handlers[j] = action.handler;
                    
                    // import optional title
                    if (action.title != undefined) {
                        $_titles[j] = action.title;
                    }
                    
                } else {
                    return _error("action type: '" + action.type + "' is unknown");
                }
                
            });
            
        }
        
        function _initTable() {
            
            var $table = $('<table>').addClass('at-table')
            .width('100%').css('border-spacing', '0');
            
            // add caption
            $table.append($('<caption>').text(opts.caption));
            
            // add other sections
            $table.append('<thead>');
            $table.append('<tfoot>');
            $table.append('<tbody>');
            
            // attach table
            $_this.append($table);

            /**
             * select mode
             */
            if (typeof opts.source === 'string') {
                
                // preload mode
                if (opts.preload) {
                    $.ajax({
                        url: options.source,
                        type: 'post',
                        dataType: 'json',
                        success: function(data) {
                            opts.source = data.rows;
                            $_total = opts.source.length;
                            $_ncol = opts.source[0].length;
                            if (opts.headers.length == 0 && data.headers != undefined) 
                                opts.headers = data.headers
                            opts.sortable = false;
                            _updateTable();
                        }
                    });
                    
                // ajax mode
                } else {
                    $_callback = function () {
                        _fetchData()
                    };
                    _fetchData();
                }
                
            // local mode
            } else if ($.isArray(opts.source)) {
                $_total = opts.source.length;
                $_ncol = opts.source[0].length;
                opts.sortable = false;
                _updateTable();
            }
        };
    };
    
}( jQuery ));

// defaults
$.fn.ajaxtable.defaults = {
    source: [],
    caption : '',
    preload: false,
    showrowid: true,
    actions: {},
    
    headers: [],
    sortable: true,
    sortby: {},
    
    paginate: true,
    paginatePlacement: 'both',
    paginateLabels: undefined,
    limit: 10
};
                