$(function() {
    
    $('#changePasswordForm').validate({
   
        messages: {
            confirm: {
                equalTo: 'The passwords do not match.'
            }
        },
        
        rules: {
            confirm: {
                equalTo: '#pass'
            }
        }
        
    });
    
});
