$(function() {
    
    $('#addUserForm').validate({
   
        messages: {
            name: 'Please provide the user\'s full name (ex. John Smith).',
            email: {
                required: 'Please provide an email address.'
            },
            confirm: {
                equalTo: 'The passwords do not match.'
            },
            username: {
                required: 'Please provide a username for the account.'
            },
            'groups[]': {
                required: 'Please select at least one group.'
            },
            pass: {
                required: 'Please provide a password.'
            },
            confirm: {
                required: 'Please reenter password.'
            }
        },
        
        rules: {
            email: {
                remote: {
                    url: '/user/email.ajax.php',
                    type: 'post'
                }
            },
            confirm: {
                equalTo: '#pass'
            },
            username: {
                rangelength: [6, 10]
            }
        }
        
    });
    
});
