$(function() {
    
    $('#userList').ajaxtable({
        
        source: '/user/list.ajax.php',
        caption: 'User Accounts',

        actions: [
            
        {
            type: 'dropdown',
            name: 'Modify',
            handler: function(id, row) {
                window.location = 'mod.php?id=' + id
            }
        },
            
        {
            type: 'dropdown',
            name: 'Delete',
            handler: function(id, row) {
                window.location = 'del.php?id=' + id
            }
        },
        
        {
            type: 'dropdown',
            name: 'Password',
            handler: function(id, row) {
                window.location = 'passwd.php?id=' + id
            }
        },
                        
        {
            type: 'column',
            colnum: 7,
            handler: function(id, row, colnum) {
                    
                if (row.nextAll('tr').hasClass('expand-col-' + colnum)) {
                    row.nextAll('tr.expand-col-' + colnum).remove();
                } else {
                    $.ajax({
                        url: '/user/groups.ajax.php',
                        data: {
                            id: id
                        },
                        success: function (data) {
                            row.after($('<tr>').addClass('expand-col-' + colnum).append(
                                $('<td>').attr('colspan', 8).html(data)
                                ));
                        }
                    });
                }
            }
        }
            
        ]

    });    
});
    