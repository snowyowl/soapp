$(function() {
    
    $(document).tooltip();
    
    $('a[title]').click(function(e) {
        e.preventDefault();
    });
    
    // 
    // login form validation
    //
    
    $('#loginForm').validate({
        
        // custom error messages
        messages: {
            pass: "Please provide a password.",
            email: {
                required: "Please provide an email address to login.",
                email: "Your email address must be in the format of name@domain.com"
            }
        },

        // custom error reporting
        invalidHandler: function(form, validator) {
            var errors = validator.numberOfInvalids();
            if (errors) {
                $('#msg').attr('class', 'error').find('p').text(validator.errorList[0].message);
                validator.errorList[0].element.focus();
            }
        },
        errorPlacement: function(error, element) {
        // prevent default display
        }
    });
    
    // set ajax table defaults if necessary
    if ($.fn.ajaxtable != undefined) {
        $.fn.ajaxtable.defaults.paginateLabels = {
            first: '<a href="#">First</a>',
            previous: '<a href="#">Previous</a>',
            next: '<a href="#">Next</a>',
            last: '<a href="#">Last</a>'
        };
    }
});

