<?php
/**
 * Main index page.
 * 
 * This page should only redirect to a specific interface file.
 * 
 * 
 * @package SnowyOwl
 * @subpackage Interface
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

header('Location: home.php');

?>
