Welcome to SnowyOwl WebApp's documentation!
===========================================

.. sectionauthor:: Omar Zabaneh <ozabaneh@ucalgary.ca>

Introduction
------------

The daemon script calls the start and stop methods of :class:`SnowyOwl.App.SOApp`. This class is derived from
:class:`SnowyOwl.Daemon.Daemon` developed by Sander Marechal. The base class has been modified however.

The :class:`SnowyOwl.DB.PgSQL` class provides a simple interface to the PostgreSQL database.

The :class:`SnowyOwl.Daemon.Daemon` class implements low level daemon functionality.
The :class:`SnowyOwl.App.SOApp` class processes pending jobs and handles errors.
It uses a :class:`SnowyOwl.App.SOProjectManager` object to run each selected project.
The :class:`SnowyOwl.App.SOProjectManager` object implements the details of the web app pipeline
(masking, assembly, mapping, and interacting with the SnowyOwl pipeline itself).
In turn, it uses a :class:`SnowyOwl.App.SOProject` object to store each project parameters and paths.

The following is a detailed list of classes and methods available.

Application Module
------------------
.. automodule:: SnowyOwl.App

Application Classes
+++++++++++++++++++

.. autoclass:: SnowyOwl.App.SOHost
    :members: __init__, run, terminate

.. autoclass:: SnowyOwl.App.SOProject
    :members: __init__, _init_log, _gunzip

.. autoclass:: SnowyOwl.App.SOProjectManager
    :members: __init__, run, terminate,
              _mask_genome,
              _assemble_transcripts,
              _generate_mapping_files_using_tophat,
              _generate_mapping_files_using_tuque,
              _run_snowyowl,
              _check_snowyowl_progress

.. autoclass:: SnowyOwl.App.SOApp
    :members: __init__, run, _init_log, _query, _check_projects, _sendmail

Database Module
---------------
.. automodule:: SnowyOwl.DB

.. autoclass:: SnowyOwl.DB.PgSQL
    :members: __init__, dsn, query, func, select

Daemon Module
-------------
.. automodule:: SnowyOwl.Daemon

.. autoclass:: SnowyOwl.Daemon.Daemon
    :members: __init__, run, start, stop, daemonize, delpid

Exceptions
----------

.. autoexception:: SnowyOwl.App.SOHostError

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

