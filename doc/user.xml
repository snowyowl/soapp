<?xml version='1.0' encoding='UTF-8'?>
<!-- This document was created with Syntext Serna Free. --><!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "docbookV4.5/docbookx.dtd" []>
<article>
  <title>Snowy Owl Web Application</title>
  <subtitle>User Manual</subtitle>
  <articleinfo>
    <copyright>
      <year>2012</year>
      <holder>University of Calgary</holder>
    </copyright>
    <author>
      <firstname>Omar</firstname>
      <surname>Zabaneh</surname>
      <email>ozabaneh@ucalgary.ca</email>
    </author>
  </articleinfo>
  <section>
    <title>Introduction</title>
    <para>Snowy Owl web application provides a database-driven user-friendly web interface to run Snowy Owl gene prediction pipeline. Users submit input files and parameters through a simple web form. The requests are stored in a queue and later processed by the application&apos;s back-end process. The queue and other related information are stored in a relational database. Users can monitor job progress and download final results directly from the web interface. The web interface, the back-end process, and the database, in addition to the actual command line  pipeline package, represent the different integrated but separate components of the web application. The application keeps track of uploaded as well as generated file and makes them available for download through the web interface.</para>
  </section>
  <section>
    <title>The Web Interface</title>
    <para>The web interface gives users access to different aspects of the application. Different users  have different roles. Visitors  have access to public information contained mainly on the Home Page. Other parts of the application require the user to  log in.</para>
    <section>
      <title>User Accounts</title>
      <para>Currently, users are registered manually. Each user is identified by an email address and a password. The email address is used to communicate with the user (for example, for sending out job and service notifications, if requested). A user must belong to one or more groups, and each group must contain one or more users. Organizing users in groups allows flexible access controls and filtering of data displayed by the application (for example listing projects for one of the groups to which the user belongs). Additionally, some users will have an administrative role to manage different aspects of the application.</para>
      <section>
        <title>User Registration</title>
        <para>Currently, users are registered manually. To register a user, user information must be entered into the relevant database tables. Shortly, administrators will be able to manage user account through the web interface.</para>
      </section>
      <section>
        <title>User Login</title>
        <para>Login form appears on the top right corner of the web interface. Placeholders &quot;Email&quot; and &quot;Password&quot; are used to identify each field to the user as shown in <xref linkend="userLogin"/>.</para>
        <figure id="userLogin">
          <title>User Login Form</title>
          <mediaobject>
            <imageobject>
              <imagedata fileref="images/login.jpg" format="JPEG"/>
            </imageobject>
            <textobject>
              <phrase>A snapshot of the empty Login Form with placeholders.</phrase>
            </textobject>
            <caption>
              <para>Simplified user login form using placeholders instead of labels for input fields.</para>
            </caption>
          </mediaobject>
        </figure>
        <para>Input fields are highlighted in red, and an error message is displayed if there is an error (incorrect email address format or bad email/password combination) as shown in <xref linkend="loginFormError"/>.</para>
        <figure id="loginFormError">
          <title>Login Form Error</title>
          <mediaobject>
            <imageobject>
              <imagedata fileref="images/loginErr.jpg" format="JPEG"/>
            </imageobject>
            <textobject>
              <phrase>A snapshot of a Login Form in error state.</phrase>
            </textobject>
            <caption>
              <para>The &quot;Email&quot; field is highlighted initially, once the focus is out of the field. Clicking the Login button will display the  error message. Icons and colors are used to differentiate error, warning, and informative (success) messages.</para>
            </caption>
          </mediaobject>
        </figure>
        <para>Once logged in, the login form is replaced by message displaying the full name of the logged in user along with a Logout button, which are displayed on every page hereafter, until the user logs out, as shown in <xref linkend="logoutForm"/>.</para>
        <figure id="logoutForm">
          <title>Logout Form</title>
          <mediaobject>
            <imageobject>
              <imagedata fileref="images/logout.jpg" format="JPEG"/>
            </imageobject>
            <textobject>
              <phrase>A snapshot of the Logout Form</phrase>
            </textobject>
            <caption>
              <para>The user&apos;s full name and logout button are displayed on every page until the user logs out.</para>
            </caption>
          </mediaobject>
        </figure>
      </section>
    </section>
    <section>
      <title>Projects</title>
      <para>This section provides the user with tools to view and manage Snowy Owl projects.</para>
      <section>
        <title>Project List</title>
        <para>Once logged in, the user is redirected to a listing of all projects available to his or her groups. The list is paginated (see <xref linkend="projectList"/>). The user may change the number of rows displayed per page using the Batch Size drop down menu. Additionally the user may use filters (not implemented yet) and column sorting to find the projects of interest. To sort by a column, simply click on the corresponding column heading. Click again to reverse the sorting order.</para>
        <figure id="projectList">
          <title>Project List</title>
          <mediaobject>
            <imageobject>
              <imagedata fileref="images/projectList.jpg" format="JPEG"/>
            </imageobject>
            <textobject>
              <phrase>A snapshot of the Project List page.</phrase>
            </textobject>
            <caption>
              <para>Project List page displays all projects available to the user in a paginated format. The user may list entries by any column in ascending or descending order.</para>
            </caption>
          </mediaobject>
        </figure>
        <para>The user can track the status and progress of the job and download result files directly from this listing (columns not shown here). Users can submit new projects by following the The New Project link on this page or Train Data  navigation item from any page. <xref linkend="addProjectSection"/> explains how to submit a new project.</para>
      </section>
      <section id="addProjectSection">
        <title>Add Project Form</title>
        <para>The create input form is divided into sections for organization and simplicity. In the future optional (expandable) sections can be added. Currently the form consist of two sections: Basic Information and Sequence Files (see <xref linkend="projectAdd"/>).</para>
        <figure id="projectAdd">
          <title>Add Project Form</title>
          <mediaobject>
            <imageobject>
              <imagedata fileref="images/projectAdd.jpg" format="JPEG"/>
            </imageobject>
            <textobject>
              <phrase>A snapshot of the Add Project form.</phrase>
            </textobject>
            <caption>
              <para>The project form is divided into sections with help messages and tool tips to guide the user. As in the rest of forms on this site, the form uses validation to ensure correct user input.</para>
            </caption>
          </mediaobject>
        </figure>
        <para>The form is supplemented with inline help messages and tooltips. The form is validated to ensure correct user input.</para>
        <para>A title is required for a new project and must be unique within a group. The user can provide a longer project description as well. The user can choose which group to add the project to, if he or she belong in multiple groups.</para>
        <para>The form implements a custom jQuery plugin that allows the user to choose files stored on the server.  Each group is assigned a separate upload file repository on the server to which files must be copied to (could be accessible as a Windows share on the user&apos;s machine). The form checks the file extension and uses AJAX to verify that the file  exists on the server. This method simplifies the submission process, since the files are already on the server and need not be uploaded through the web server. This is very useful if files are very large and need to be submitted multiple times. Currently, the accepted file extension are: fasta, fas, fa, fastq, and gz.</para>
        <para>The user creates a project but is not the owner of the project. Once the project is created it is assigned to the group. If the user account is disabled, or the user is no longer a member of the group, other members in the group can still access the project.</para>
      </section>
    </section>
  </section>
</article>
