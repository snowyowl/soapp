<?php
/**
 * Global initialization file.
 * 
 * @package SnowyOwl
 * @subpackage Engine
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * Alias for DIRECTORY_SEPARATOR
 */
define('DS', DIRECTORY_SEPARATOR);

/**
 * Application base directory.
 * 
 * Parent directory of all application files and directories,
 * including Apache DocumentRoot (/www).
 */
define('ROOT', dirname(__FILE__) . DS);

/**
 * PHP library directory.
 */
define('LIB', ROOT . 'lib' . DS . 'php' . DS);

/**
 * Template directory. 
 */
define('TPL', ROOT . 'tpl' . DS);

/**
 * Data directory. 
 */
define('DAT', ROOT . 'data' . DS);

/**
 * Project directory. 
 */
define('PRJ', DAT . 'project' . DS);

/**
 * User upload directory. 
 */
define('UPL', ROOT . 'upload' . DS);

/**
 * Log directory. 
 */
define('LOG', ROOT . 'log' . DS);

/**
 * Smarty initializatoin
 * 
 * Must be done before spl_autoload_register
 */
define('SMARTY_DIR', LIB . 'smarty' . DS);
require_once SMARTY_DIR . 'Smarty.class.php';

/**
 * Set PHP include path for the application. 
 */
set_include_path(
    join(PATH_SEPARATOR,array(
        LIB,
        LIB . 'db',
        LIB . 'engine',
        LIB . 'smarty')
    )
);

/**
 * Autoload classes.
 * 
 * Search PHP include paths for a class definition file.
 * For example, calling "new MyClass()" will automatically
 * search PHP inlcude path for MyClass.class.php.
 */
function load_class($className) { require_once "$className.class.php"; }
spl_autoload_register('load_class');

/**
 * Application global error function.
 * 
 * Raises the last error/warning message as E_USER_ERROR.
 * Terminates execution and appends an optional message between
 * square brakets.
 * All user (application) errors can be handled the same way by catching
 * all E_USER_ERROR exceptions.
 * 
 * @param string $msg optional error details.
 */
function error($msg = "") {
    $err = error_get_last();
    trigger_error($err['message'] . " [$msg]", E_USER_ERROR);
}

/**
 * Application global warning function.
 * 
 * Raises E_USER_WARNING usingn the supplied message.
 * Does not terminate execution.
 * E_USER_WARNING messages should be hidden from production environments.
 * 
 * @param string $msg required warning message.
 */
function warn($msg) {
    trigger_error($msg, E_USER_WARNING);
}

/**
 * Autoload script engine based on file extension.
 * 
 * Create an instance of an Engine subclass based on the script extension.
 * The object is then available in the global scope 
 * using the generic name $_Engine.
 * 
 * <script_name>.ajax.php --> AjaxEngine
 * default: WebEngine
 */
if (strpos($_SERVER['SCRIPT_NAME'], '.ajax.php') !== false) {
    $_Engine = new AjaxEngine();
} else if (strpos($_SERVER['SCRIPT_NAME'], '.download.php') !== false) {
    $_Engine = new DownloadEngine();
} else {
    $_Engine = new WebEngine();
    $_Engine->includeScript('jquery-ui.js');
    $_Engine->includeStyle('jquery-ui.css');
}

?>
