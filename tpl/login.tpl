<div id="so-user">
{if isset($smarty.session.user.id)}
<form id="so-user-login" name="logoutForm" action="" method="post">
  <div>
  Logged in as <strong>{$smarty.session.user.name}</strong>
  <input id="so-logout-button" name="logoutBtn" value="Logout" type="submit"></input>
  </div>
  <div>
      <a href="/user/passwd.php">Change My Password</a> | 
      <a href="/user/notify.php">Email Notification</a>
  </div>
</form>
{else}
<form id="so-user-logout" name="loginForm" action="" method="post">
  <input id="username" name="username" type="text" placeholder="Username" class="required"></input>
  <input id="pass" name="pass" type="password"  placeholder="Password" class="required"></input>
  <input id="so-login-button" name="loginBtn" value="Login" type="submit"></input>
</form>
{/if}
</div>