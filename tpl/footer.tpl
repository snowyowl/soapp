<div id="so-footer">
    <p>Copyright © 2012 - {'Y'|date} University of Calgary. All rights reserved.
        <a href="#">Terms of Use</a> |
        <a href="#">Privacy Policy</a>
    </p>
</div>