<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$lang|default:'en'}">
    <head>
        <title>{$title}</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    {foreach from=$css item=style}<link rel="stylesheet" href="{$style.href}" {if isset($syle.media)}media="$style.media" {/if}/>
    {/foreach}
    {foreach from=$js item=href}<script type="text/javascript" src="{$href}"></script>
    {/foreach}
</head>
<body>
    <div id="so-wrapper">
        {include file="header.tpl"}
        <div id="so-content">
        <div id="msg" class="{$msg_class|default:"none"}"><p>{$msg|default:""}</p></div>
        {$body}
        </div>
        {include file="footer.tpl"}
    </div>
</body>
</html>
