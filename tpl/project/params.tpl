<table class="params">
    <tr>
        <td>
            <label>Target genome filename:</label>
            {$params.genome.name} ({$params.genome.size|du})
        </td>
    </tr>
    {if $params.masked}
        <tr>
            <td>
                {if $params.maskseq}
                    <label>Genomic sequences to mask:</label> {$params.masked.name} ({$params.maskseq}% identity threshold)
                {else}
                    <label>Masked genome filename:</label> {$params.masked.name}
                {/if}
                ({$params.masked.size|du})
            </td>
        </tr>
    {/if}
    <tr>
        <td>
            <label>RNA-Seq filename:</label> 
            {$params.rnaseq.name} ({$params.rnaseq.size|du})
            {if $params.tophat eq 1}{if $params.phred64 eq 1}Phred-64{else}Phred-32{/if}{/if}
        </td>
    </tr>
    <tr>
        <td>
            {if $params.mrna}
              <label>Transcripts filename:</label> {$params.mrna.name} ({$params.mrna.size|du})
            {else}
              <label>Transcript assembler:</label> Trinity
            {/if}
        </td>
    </tr>
    <tr>
        <td>
            <label>Read mapping program:</label>
            {if $params.tophat eq 1}Tophat{else}Tuque{/if}
        </td>
    </tr>
    <tr>
        <td>
            <label>RefSeq database:</label>
            {$params.refseq_db}
        </td>
    </tr>
    <tr>
        <td>
            <label>Transcript intron alignment size:</label>
            {$params.imin} to {$params.imax} bp
        </td>
    </tr>
</table>