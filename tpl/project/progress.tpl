<table class="progress">
{if $project.started_on or $project.completed_on}
<tr>
    <td colspan="{$steps|@count}">
    {if $project.started_on}<label>Started:</label> {$project.started_on}{/if}
    {if $project.completed_on}<label>Completed:</label> {$project.completed_on} {/if}
    </td>
</tr>
{/if}
<tr>
{foreach from=$steps item=step}
    <td{if $step.completed_on} class="step{$step.id} completed" title="{$step.completed_on}"{/if}>{$step.title}</td>
{/foreach}
</tr>
</table>
