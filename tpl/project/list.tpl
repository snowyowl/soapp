<h1>Project List</h1>
<p class="help">Refresh the page to update project status.<br />
Click on project status to display current progress and project parameters. Toggle status to update progress.<br />
Click on the project identification number to display project parameters.<br />
Click on the project name to display project results.<br />
Completed steps are highlighted. Hover over to display completion date and time.<br />
</p>
<p class="so-right"><a href="/project/add.php">Create a New Project</a></p>
<div id="projectList"></div>