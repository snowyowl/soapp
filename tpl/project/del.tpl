<form id="delProjectForm" name="delProjectForm" action="list.php" method="post">
    <p class="warning">Are you sure you want to delete this project?</p>
    
    <p><label>Project ID: </label>{$smarty.get.id}</p>
    <input type="submit" name="delProject" value="Delete Project" tabindex="1"></input>
    <input type="submit" value="Cancel" tabindex="2"></input>
    <input name="id" type="hidden" value="{$smarty.get.id}"></input>
</form>