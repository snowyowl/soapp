<h1>Create a New Project</h1>

<p class="help">Please fill out one of the following forms to start a new project.<br />
    You may drag and drop your sequence files (supported browsers only).<br />
    For additional help filling out the form, hover over the appropriate "?" symbol below.
</p>

<div id="tabs">

    <ul>
        <li><a href="#tab-a">Start with Pre-Assembled Transcripts</a></li>
        <li><a href="#tab-b">Start with Transcript Assembly</a></li>
        <li><a href="#tab-c">Output Options</a></li>
    </ul>

    <div id="tab-a">
        <form id="form-a" action="list.php" method="post" enctype="multipart/form-data">
            {include file="project/add/header.tpl"}
            <table>
                {include file="project/add/basic.tpl" form_id="a"}
                {include file="project/add/genome.tpl" form_id="a"}
                {include file="project/add/rnaseq.tpl" form_id="a"}
                {include file="project/add/mrna.tpl" form_id="a"}
                {include file="project/add/map.tpl" form_id="a"}
                {include file="project/add/qual.tpl" form_id="a"}
                {include file="project/add/refseq.tpl" form_id="a"}
                {include file="project/add/intron.tpl" form_id="a"}
                {include file="project/add/hidden.tpl" form_id="a"}
            </table>
            <p class="so-center">
                <input id="add-project-a" type="submit" name="addProject" value="Create Project"></input>
            </p>
            <input id="uuid-a" name="uuid" type="hidden" value="{$uuid1}"></input>
        </form>
    </div>

    <div id="tab-b">
        <form id="form-b" action="list.php" method="post" enctype="multipart/form-data">
            {include file="project/add/header.tpl"}
            <table>
                {include file="project/add/basic.tpl" form_id="b"}
                {include file="project/add/genome.tpl" form_id="b"}
                {include file="project/add/rnaseq.tpl" form_id="b"}
                {include file="project/add/asm.tpl" form_id="b"}
                {include file="project/add/map.tpl" form_id="b"}
                {include file="project/add/qual.tpl" form_id="b"}
                {include file="project/add/refseq.tpl" form_id="b"}
                {include file="project/add/intron.tpl" form_id="b"}
                {include file="project/add/hidden.tpl" form_id="b"}
            </table>
            <p class="so-center">
                <input id="add-project-b" type="submit" name="addProject" value="Create Project"></input>
            </p>
            <input id="uuid-b" name="uuid" type="hidden" value="{$uuid2}"></input>
        </form>
    </div>
    
    <div id="tab-c">
        {include file="project/add/output.tpl"}
    </div>
        
</div>