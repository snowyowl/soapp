<table class="output">
    <tbody>
        {foreach from=$output item=files key=category}
            {foreach from=$files item=file name=file}
                <tr>
                    {if $smarty.foreach.file.first}
                        <td class="label" rowspan={$files|@count}>
                            {$category}
                        </td>
                    {/if}
                    <td> {$file.name} ({$file.size|du})</td>
                    <td>{$file.description}</td>
                    <td class="action">
                        <a href="output.download.php?id={$smarty.get.id}&file={$file.id}">Download</a>
                    </td>
                    <td class="action">
                        <a href="javascript:deleteOutputFile({$smarty.get.id},{$file.id},2)">Delete</a>
                    </td>
                </tr>
            {/foreach}
        {/foreach}
    </tbody>
</table>
