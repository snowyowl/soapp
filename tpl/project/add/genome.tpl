<tr>
    <td class="label">
        <label for="genome-{$form_id}">Upload Target Genome Sequence</label> <em>*</em>
    </td>
    <td>
        <input id="genome-{$form_id}" name="genome" type="text" class="uploader required" readonly="readonly"></input>
        <div id="genome-uploader-{$form_id}"></div>
        <div class="ext-fasta"></div>
    </td>
</tr>
<tr>
    <td class="label">
        <label for="masked-{$form_id}">Mask Target Genome (Optional)</label>
    </td>
    <td>
        <p>
            <input id="mask_it-0-{$form_id}" type="radio" name="mask_it" value="0" checked="checked">No Masking</input>
            <input id="mask_it-1-{$form_id}" type="radio" name="mask_it" value="1">Upload Target Sequences to Mask</input>
            <a href="#" title="Upload target sequence fragments to mask in FASTA. Sequences must be contiguous (must include intronic sequence for genes).">?</a>
            <input id="mask_it-2-{$form_id}" type="radio" name="mask_it" value="2">Upload a Masked Copy of the Target Genome</input>
            <a href="#" title="Upload a FASTA file of the complete genome sequence with N's masking regions where gene predictions are not wanted.">?</a>
        </p>

        <div id="masked-field-{$form_id}">
            <input id="masked-{$form_id}" name="masked" type="text" class="uploader" readonly="readonly"></input>
            <div id="masked-uploader-{$form_id}"></div>
            <div class="ext-fasta"></div>
        </div>

        <div id="maskseq-field-{$form_id}">
            <p>
                <label for="maskseq-{$form_id}">Provide percent identity threshold (90-100):</label>
                <input id="maskseq-{$form_id}" name="maskseq" type="text" value="100" maxlength="3" class="short"></input><br />
            </p>
            <p class="help">A masked copy of the target genome generated will be available to download along with project results.<br />
            The total legnth and number of sequences masked will also be provided in the log files.
            </p>
        </div>
    </td>
</tr>


