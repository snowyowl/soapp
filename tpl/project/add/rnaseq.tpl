<tr>
    <td class="label">
        <label for="rnaseq-{$form_id}">Upload RNA-Seq Reads</label> <em>*</em>
    </td>
    <td>
        <input id="rnaseq-{$form_id}" name="rnaseq" type="text" class="uploader required" readonly="readonly"></input>
        <div id="rnaseq-uploader-{$form_id}"></div>
        <span class="ext-fastq"></span>
    </td>
</tr>
