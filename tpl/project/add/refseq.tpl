<tr>
    <td class="label">
        <label for="refseq-{$form_id}">Choose RefSeq Protein DB</label>
    </td>
    <td>
        <select id="refseq-{$form_id}" name="refseq">
        {foreach from=$refseq item=db}
            <option value="{$db.id}">{$db.title}</option>
        {/foreach}
        </select>
    </td>
</tr>
