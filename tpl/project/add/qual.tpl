<tr>
    <td class="label">
        <label for="phred64-{$form_id}">Choose RNA-Seq Quality Score</label>
    </td>
    <td>
        <select id="phred64-{$form_id}" name="phred64">
            <option value="0">Phred 33</option>
            <option value="1">Phred 64</option>
        </select>
    </td>
</tr>