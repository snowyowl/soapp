{if $groups|@count gt 1}
<tr>
    <td class="label">
        <label for="gid-{$form_id}">Choose a User Group</label>
    </td>
    <td>
        <select id="gid-{$form_id}" name="gid">
            {foreach from=$groups item=group}
                <option value="{$group.id}">{$group.title}</option>
            {/foreach}
        </select>
        <a href="#" title="Every project is assigned to a user group, and all users in this group are able to access the project. A user may be in more than one group. You have access to all the groups listed here.">?</a>
    </td>
</tr>    
{/if}
<tr>
    <td class="label">
        <label for="title-{$form_id}">Provide a Project Title</label> <em>*</em>
    </td>
    <td>
        <input id="title-{$form_id}" name="title" type="text" maxlength="32" class="required"></input>
        <a href="#" title="Please provide a descriptive title for the project, for easy identification.">?</a>
    </td>
</tr>
<tr>
    <td class="label">
        <label for="desc-{$form_id}">Provide a Description<br />(Optional)</label>
    </td>
    <td>
        <textarea id="desc-{$form_id}" name="desc" rows="5" cols="80"></textarea>
        <a href="#" title="Additional information related to the project, for example references and study organism (optional).">?</a>
        {if $groups|@count eq 1}<input type="hidden" id="gid-{$form_id}" name="gid" value="{$groups[0].id}"></input>{/if}
    </td>
</tr>