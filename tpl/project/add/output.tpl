<form id="form-output">
    <p class="help">
        SnowyOwl generates a number of output files. 
        Choose files you wish to save from this list.
        (Not all files listed may be generated for your project,
        depending on your project settings.)
        Saved files will be available for download for a period of 30 days,
        after which the project and all saved output files are deleted 
        and no longer accessible on the server.
    </p>
    <table class="output">
        {foreach from=$output item=files key=category}
            {foreach from=$files item=file name=file}
                <tr>
                    {if $smarty.foreach.file.first}
                    <td class="label" rowspan={$files|@count}>
                        <label>{$category}</label>
                    </td>
                    {/if}
                    
                    <td>
                        <input id="save-{$file.id}" name="save[]" value="{$file.id}" type="checkbox"{if $file.required} checked="checked" disabled="disabled"{/if}></input>
                    </td>
                    <td><label for="save-{$file.id}">{$file.name}</label></td>
                    <td class="description">{$file.description}</td>
                </tr>
            {/foreach}
        {/foreach}
    </table>
</form>