<tr> 
    <td class="label">
        <label for="mrna-{$form_id}">Upload Transcripts</label> <em>*</em>
    </td>
    <td>
        <input id="mrna-{$form_id}" name="mrna" type="text" class="uploader required" readonly="readonly"></input>
        <div id="mrna-uploader-{$form_id}"></div>
        <span class="ext-fasta"></span>
    </td>
</tr>