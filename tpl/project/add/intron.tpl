<tr>
    <td class="label">
        <label for="refseq-{$form_id}">Provide Intron Size Range for Transcript/Read Alignment</label>
        <em>*</em>
    </td>
    <td>
        <span>From</span>
        <input id="imin-{$form_id}" name="imin" type="text" value="50" class="required number short">
        <span>bp to</span>
        <input id="imax-{$form_id}" name="imax" type="text" value="2000" class="required number short">
        <span>bp</span>
        <a href="#" title="Provide the allowed intron size range for alignment of transcripts or RNA-Seq reads to the target genome.">?</a>
    </td>
</tr>
