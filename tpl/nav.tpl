<div id="so-nav">
<nav>
  {if $smarty.server.SCRIPT_NAME eq '/home.php'}Introduction{else}<a href="/home.php">Introduction</a>{/if} |
  {if isset($smarty.session.user)}
    {if $smarty.server.SCRIPT_NAME eq '/project/list.php'}Project List{else}<a href="/project/list.php">Project List</a>{/if} |
    {if $smarty.server.SCRIPT_NAME eq '/project/add.php'}Create Project{else}<a href="/project/add.php">Create Project</a>{/if} |
  {/if}
  {if isset($smarty.session.user) and $smarty.session.user.role eq 1}
    {if $smarty.server.SCRIPT_NAME eq '/user/list.php'}Users{else}<a href="/user/list.php">Users</a>{/if} |
    {if $smarty.server.SCRIPT_NAME eq '/group/list.php'}Groups{else}<a href="/group/list.php">Groups</a>{/if} |
  {/if}
  {if $smarty.server.SCRIPT_NAME eq '/contact.php'}Contact Us{else}<a href="#">Contact Us</a>{/if}
</nav>
</div>