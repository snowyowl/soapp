<h1>Introduction to Snowy Owl</h1>

<p>Snowy Owl is an automated eukaryotic gene predictor.  
   This automated structural annotator can produce high-quality gene models from 
   an assembled eukaryotic genome and RNA-Seq reads quickly and efficiently.  
   Ab initio gene predictors are used in conjunction to aid Snowy Owl’s final 
   annotation.  
   The programs GeneMark-ES and Augustus are used to create an initial 
   prediction.  
   To train Augustus, RNA-Seq alignments are used.  Genes that are concurrent 
   from both predictors are used to retrain Augustus, with optimized parameters 
   and RNA-Seq reads used as hints.
</p>

<p>
Initially, Snowy Owl makes several predictions at a specific loci; 
each model is then scored for translatability, homology to known proteins, 
and alignment to their predicted transcripts from RNA-Seq data. The models 
with the highest score that do not overlap are selected as the final gene 
predictions from Snowy Owl.
</p>

<p>
    <a href="/project/list.php">Go to Project List</a>
</p>
    