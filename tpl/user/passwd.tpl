<h1>Change Password for {$user.name}</h1>

<p class="help">Use this form to change the login password for {$user.name}.<br />
    For additional help filling out the form, hover over the appropriate "?" symbol below.
</p>

<form id="changePasswordForm" name="changePasswordForm" action="" method="post">

    <p><em>*</em> Required Fields</p>

    <fieldset>
        <legend>User Access</legend>
        <table>
            <tr>
                <td class="label">
                    <label for="pass">Password</label> <em>*</em>
                </td>
                <td>
                    <input id="pass" name="pass" type="password" class="required password" tabindex="1"></input>
                    <a href="#" title="New account password.">?</a>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="confirm">Confirm</label> <em>*</em>
                </td>
                <td>
                    <input id="confirm" name="confirm" type="password" class="required confirm"  tabindex="2"></input>
                    <a href="#" title="New password confirmation.">?</a>
                </td>
            </tr>
        </table>
    </fieldset>
    <p class="right">
        <input type="submit" id="changePassword" name="changePassword" value="Change My Password" tabindex="3"></input>
    </p>
</form>