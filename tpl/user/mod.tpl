<h1>Modify Account for {$user.name}</h1>

<p class="help">Use this form to modify this user account.
    A user must be assinged to at least one group.<br />
    For additional help filling out the form, hover over the appropriate "?" symbol below.
</p>

<form id="modUserForm" name="modUserForm" action="list.php" method="post">

    <p><em>*</em> Required Fields</p>

    <fieldset>
        <legend>Basic Information</legend>
        <table>
            <tr>
                <td class="label">
                    <label>User</label>
                </td>
                <td>
                    <span>{$user.name} &lt;{$user.username}&gt;</span>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label>Email Address</label>
                </td>
                <td>
                    <span>{$user.email}</span>
                </td>
            </tr>
        </table>
    </fieldset>

    <fieldset>
        <legend>User Access</legend>
        <table>
            <tr>
                <td class="label">
                    <label for="groups">Group(s)</label> <em>*</em>
                </td>
                <td>
                    <select id="groups" name="groups[]" class="required" multiple="multiple" size="5" tabindex="1">
                        {foreach from=$groups item=group}
                            <option value="{$group.id}"{if $group.user} selected="selected"{/if}>{$group.title}</option>
                        {/foreach}
                    </select>
                    <a href="#" title="Every user must be assinged to at least one group.">?</a>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="role">Role</label>
                </td>
                <td>
                    <select id="role" name="role" tabindex="2">
                        {foreach from=$roles item=role}
                            <option value="{$role.id}"{if $role.id eq $user.role} selected="selected"{/if}>{$role.title}</option>
                        {/foreach}
                    </select>
                    <a href="#" title="Registered users are either administrators or limited users. Most users are limited (the default).">?</a>
                </td>
            </tr>
            <tr>
                <td class="label">    
                    <label for="status">Status</label>
                </td>
                <td>
                    <select id="status" name="status" tabindex="3">
                        {foreach from=$statuses item=status}
                            <option value="{$status.status}"{if $status.status eq $user.status} selected="selected"{/if}>{$status.title}</option>
                        {/foreach}
                    </select>
                    <a href="#" title="Users with disabled account may not log in and submit new projects. Older projects are preserved and still accessible by other group members.">?</a>
                </td>
            </tr>
        </table>
    </fieldset>

    <p>
        <input type="submit" id="modUser" name="modUser" value="Update Account" tabindex="4"></input>
        <input type="reset" value="Reset Form" tabindex="5"></input>
        <input type="submit" value="Cancel" tabindex="6"></input>
    </p>

    <input type="hidden" name="id" value="{$smarty.get.id}"></input>

</form>