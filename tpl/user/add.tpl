<h1>Create a New User Account</h1>

<p class="help">Use this form to create a new user account.
    A user must be assinged to at least one group.<br />
    For additional help filling out the form, hover over the appropriate "?" symbol below.
</p>

<form id="addUserForm" name="addUserForm" action="list.php" method="post" enctype="multipart/form-data">

    <p><em>*</em> Required Fields</p>

    <fieldset>
        <legend>Basic Information</legend>
        <table>
            <tr>
                <td class="label">
                    <label for="name">Name</label> <em>*</em>
                </td>
                <td>
                    <input id="name" name="name" type="text" class="required" tabindex="1"></input>
                    <a href="#" title="The full name of the new user. For example 'Joe Smith'.">?</a>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="username">Username</label> <em>*</em>
                </td>
                <td>
                    <input id="username" name="username" type="text" class="required" tabindex="2"></input>
                    <a href="#" title="A username for the user. Must exist on the file server.">?</a>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="email">Email Address</label> <em>*</em>
                </td>
                <td>
                    <input id="email" name="email" type="text" class="required email" tabindex="3"></input>
                    <a href="#" title="An email address for the user. Must be unique.">?</a>
                </td>
            </tr>
        </table>
    </fieldset>

    <fieldset>
        <legend>User Access</legend>
        <p class="help">Set the groups, role and password for the user account.<br />
            The user can reset his or her password if they wish.
        </p>
        <table>
            <tr>
                <td class="label">
                    <label for="groups">Group(s)</label> <em>*</em>
                </td>
                <td>
                    <select id="groups" name="groups[]" class="required" multiple="multiple" size="5" tabindex="4">
                        {foreach from=$groups item=group}
                            <option value="{$group.id}">{$group.title}</option>
                        {/foreach}
                    </select>
                    <a href="#" title="Every user must be assinged to at least one group.">?</a>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="role">Role</label>
                </td>
                <td>
                    <select id="role" name="role" tabindex="5">
                        {foreach from=$roles item=role}
                            <option value="{$role.id}" selected="selected">{$role.title}</option>
                        {/foreach}
                    </select>
                    <a href="#" title="Registered users are either administrators or limited users. Most users are limited (the default).">?</a>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="pass">Password</label> <em>*</em>
                </td>
                <td>
                    <input id="pass" name="pass" type="password" class="required password" tabindex="6"></input>
                    <a href="#" title="Account password.">?</a>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="confirm">Confirm</label> <em>*</em>
                </td>
                <td>
                    <input id="confirm" name="confirm" type="password" class="required confirm" tabindex="7"></input>
                    <a href="#" title="Account password confirmation.">?</a>
                </td>
            </tr>
        </table>
    </fieldset>

    <p>
        <input type="submit" id="addUser" name="addUser" value="Add User Account" tabindex="8"></input>
    </p>

</form>