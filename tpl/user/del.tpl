<form  id="delUserForm" name="delUserForm" action="list.php" method="post">
    <p class="warning">Are you sure you want to delete this user account?</p>
    <p>{$user.name} &lt;{$user.username}&gt;</p>
    <input type="submit" name="delUser" value="Delete Account" tabindex="1" />
    <input type="submit" value="Cancel" tabindex="2" />
    <input name="id" type="hidden" value="{$smarty.get.id}" />
</form>