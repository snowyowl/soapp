<h1>Email Notification</h1>

<p class="help">Use this form to change your email notification settings.<br />
    For additional help filling out the form, hover over the appropriate "?" symbol below.
</p>

<form id="changeNotificationForm" name="changeNotificationForm" action="" method="post">

    <p><em>*</em> Required Fields</p>

    <fieldset>
        <legend>User Access</legend>
        <table>
            <tr>
                <td class="label">
                    <label for="pass">Email</label> <em>*</em>
                </td>
                <td>
                    <input id="email" name="email" type="text" class="required email" tabindex="1" value="{$user.email}"></input>
                </td>
            </tr>
            <tr>
                <td class="label">
                     <label>Notification</label> <em>*</em>
                </td>
                <td>
                    <input id="notify-on" name="notify" type="radio" value="1" tabindex="2"{if $user.notify == 1} checked="checked"{/if}></input>
                    <label for="notify-on">On</label>
                    <input id="notify-off" name="notify" type="radio" value="0" tabindex="3"{if $user.notify == 0} checked="checked"{/if}></input>
                    <label for="notify-off">Off</label>
                </td>
            </tr>
        </table>
    </fieldset>
    <p class="right">
        <input type="submit" id="changeNotification" name="changeNotification" value="Save" tabindex="4"></input>
    </p>
</form>