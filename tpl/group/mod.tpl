<h1>Modify User Group: {$group.title}</h1>

<p class="help">Use this form to modify this user group.
The group must contain at least one user account.<br />
For additional help filling out the form, hover over the appropriate "?" symbol below.
</p>

<form id="modGroupForm" name="modGroupForm" action="list.php" method="post">
    <p class="right"><em>*</em> Required Fields</p>
    <fieldset>
        <legend>Basic Information</legend>
        <table>
            <tr>
                <td class="label">
                <label>Name</label>
                </td>
                <td>
                <span>{$group.title}</span>
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset>
        <legend>Group Users and Status</legend>
        <table>
            <tr>
                <td class="label">
                    <label for="users">User(s)</label> <em>*</em>
                    </td>
                    <td>
                <select id="users" name="users[]" class="required" multiple="multiple" size="5" tabindex="1">
                    {foreach from=$users item=user}
                    <option value="{$user.id}"{if in_array($user.id, $group.users)} selected="selected"{/if}>{$user.name} &lt;{$user.email}&gt;</option>
                    {/foreach}
                </select>
                <a href="#" title="Every group must have at least one user.">?</a>
                </td>
            </tr>
            <tr>
                <td class="label">
                <label for="status">Status</label>
                </td>
                <td>
                <select id="status" name="status" tabindex="2">
                    {foreach from=$statuses item=status}
                    <option value="{$status.status}"{if $status.status eq $group.status} selected="selected"{/if}>{$status.title}</option>
                    {/foreach}
                </select>
                <a href="#" title="Projects in disabled groups are not accessible. Active members of disabled groups cannot submit new projects in the disabled group.">?</a>
            </td>
            </tr>
        </table>
    </fieldset>
                
    <p>
    <input type="submit" id="modGroup" name="modGroup" value="Update Group" tabindex="3"></input>
    <input type="reset" value="Reset Form" tabindex="4"></input>
    <input type="submit" value="Cancel" tabindex="5"></input>
    </p>
    
    <input type="hidden" name="id" value="{$smarty.get.id}"></input>

</form>