<h1>Create a New Group</h1>

<p class="help">Use this form to create a new group.
You have to choose at least one user for the group.<br />
An upload directory will be created for the group under {$smarty.const.UPL}.<br />
For additional help filling out the form, hover over the appropriate "?" symbol below.
</p>

<form id="addGroupForm" name="addGroupForm" action="list.php" method="post" enctype="multipart/form-data">
    
    <p><em>*</em> Required Fields</p>
    
    <fieldset>
        <legend>Basic Information</legend>
        <table>
            <tr>
                <td class="label">
                    <label for="title">Title</label> <em>*</em>
                </td>
                <td>
                <input id="title" name="title" type="text" class="required" tabindex="1"></input>
                <a class="" href="#" title="A short title for the group, for easy identification (must be unique).">?</a>
                </td>
            </tr>
            <tr>
                <td class="label">
                <label for="users">Users</label> <em>*</em>
                </td>
                <td>
                <select id="users" name="users[]" rows="5" multiple="multiple" class="required" tabindex="2">
                    {foreach from=$users item=user}
                    <option value="{$user.id}">{$user.name}</option>
                    {/foreach}
                </select>
                <a href="#" title="Choose one or more users to add to the group. Hold Control Key to select multiple users.">?</a>
                </td>
            </tr>
        </table>
    </fieldset>
                
    <p>
    <input type="submit" id="addGroup" name="addGroup" value="Create Group" tabindex="3"></input>
    </p>
    
</form>