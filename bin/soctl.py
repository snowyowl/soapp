#!/usr/bin/env python
"""
Snowy Owl - Backend Daemon Controler
"""

import sys
import os
import argparse
import traceback
import logging

from SnowyOwl.App import SOApp

def main(args):

    try:
        root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
        app = SOApp(root)

    except Exception, e:
        print >> sys.stderr, "fatal: %s" % e
        print >> sys.stderr, traceback.format_exc()
        sys.exit(1)

    log = logging.getLogger('root')

    try:
        if args.cmd == 'start':
            app.start()
        elif args.cmd == 'stop':
            app.stop()

    except Exception, e:
        log.error("fatal: %s" % e)
        log.debug(traceback.format_exc())
        sys.exit(1)



if __name__ == '__main__':

    p = argparse.ArgumentParser(description=__doc__)
    p.add_argument('cmd', help='command to run', choices=['start','stop'])
    args = p.parse_args()

    main(args)