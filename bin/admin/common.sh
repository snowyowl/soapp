#
# global configuration
#

LOCALHOST="`hostname`.`dnsdomainname`"

TEST_HOST="coe03.coe.ucalgary.ca"
PROD_HOST="coe03.coe.ucalgary.ca"
REPO_HOST="coe03.coe.ucalgary.ca"
SNOW_HOST="access.fungalgenomics.ca"

TEST_USER="ozabaneh"
PROD_USER="ozabaneh"
REPO_USER="ozabaneh"
SNOW_USER="ozabaneh"

TEST_DIR="/export/geno_new/SOApp/test"
PROD_DIR="/export/geno_new/SOApp/prod"
REPO_DIR="/export/geno_new/SOApp/soapp.git"
SNOW_DIR="/export/geno_new/SOApp/SnowyOwl"

#
# utility functions
#

function die {
    echo "fatal: $1"
    exit 1
}

function prompt {
    while true; do
        echo -n "$1 [y/n] "
        read ans
        if [[ $ans =~ ^[yY]$ ]]; then
            break
        elif [[ $ans =~ ^[nN]$ ]]; then
            echo "Cancelled by user."
            exit
        fi
    done
}
