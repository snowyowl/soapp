#!/bin/bash
###############################################################################
# Synchronize application code with the repository.
################################################################################

. common.sh

# set repository path
if [ $LOCALHOST == $REPO_HOST ]; then
    REPO="$REPO_DIR"
else
    REPO="$REPO_USER@$REPO_HOST:$REPO_DIR"
fi

# set production path
if [ $LOCALHOST == $PROD_HOST ]; then
    PROD="$PROD_DIR"
else
    PROD="$PROD_USER@$PROD_HOST:$PROD_DIR"
fi

#
# create temporary location for the repository code
#
TEMP=`mktemp -d`
if [ $? -ne 0 ]; then
    die "failed to create temporary directory for repository."
fi

trap "rm -rf $TEMP" EXIT

#
# clone git repository into a temporary location
#
echo "Fetch files from git repository at $REPO."
git archive --remote=$REPO HEAD | tar -xf - -C $TEMP
if [ $? -ne 0 ]; then
    die "git-archive failed"
fi

# remove development/admin related directories
rm -rf $TEMP/doc $TEMP/sql

# make sure the application root directory has setuid/setgid bits set 
# and others have no read/write/execute
#
# all files created inside the application root should be assinged to the
# snowyowl group, and anyone in this group can run admin tasks as owner.
chmod u=rwxs,g=rwxs,o= $TEMP
find $TEMP -type d -exec chmod u=rwx,g=rx,o= {} \;
find $TEMP -type f -exec chmod u=rw,g=r,o= {} \;
find $TEMP/bin -type f -exec chmod ug+x,o= {} \;

#
# synch files with repository
#

# ask user to confirm
prompt "Are you sure you want to update the test server?"

# synchronize files
rsync -rptI --delete --delete-after --force --quiet \
      --exclude '.git' \
      --exclude 'data' \
      --exclude 'upload' \
      --exclude 'log' \
      --exclude 'config.ini' \
      --chmod=Dg+s \
      $TEMP/ $PROD
if [ $? -ne 0 ]; then
    die "rsync failed."
fi

# done
echo "Finished successfully."
exit 0
