#!/bin/bash
###############################################################################
# Synchronize SnowyOwl pipeline code on production with repository.
################################################################################

. common.sh

# set repository path
REPO="$SNOW_USER@$SNOW_HOST:calgary-share/repo/SnowyOwl"

# set pipeline path
# on the production machine
if [ $LOCALHOST == $PROD_HOST ]; then
    SNOW="$SNOW_DIR"
else
    SNOW="$PROD_USER@$PROD_HOST:$SNOW_DIR"
fi

#
# create temporary location for the repository code
#
TEMP=`mktemp -d`
if [ $? -ne 0 ]; then
    die "failed to create temporary directory for repository."
fi

trap "rm -rf $TEMP" EXIT

#
# clone git repository into a temporary location
#
echo "Fetch files from git repository at $REPO."
git archive --remote=$REPO HEAD | tar -xf - -C $TEMP
if [ $? -ne 0 ]; then
    die "git-archive failed"
fi

#
# synch files with repository
#

# ask user to confirm
prompt "Are you sure you want to update the test server?"

# synchronize files
rsync -rptI --delete --delete-after --force --quiet \
      --exclude 'CONFIG' \
      $TEMP/ $SNOW
if [ $? -ne 0 ]; then
    die "rsync failed."
fi

# done
echo "Finished successfully."
exit 0
