#!/bin/bash
################################################################################
# Download and Install RefSeq Databases
# Author: Omar Zabaneh <zabano@gmail.com>
################################################################################

. common.sh

#
# utility functions
#

function usage {
    echo "usage:" $0 database
    exit 0
}

#
# get RefSeq database name from command line
#
if [ -z $1 ]; then
    usage
else
    DB=$1
fi

#
# create temporary location for the repository code
#
TEMP=`mktemp -d`
if [ $? -ne 0 ]; then
    die "failed to create temporary directory."
fi
trap "rm -rf $TEMP" EXIT

# set RefSeq FASTA target
FASTA="$PROD_DIR/data/refseq/refseq.$DB.faa"

wget ftp://ftp.ncbi.nih.gov/refseq/release/$DB/$DB.*.protein.faa.gz \
    --directory-prefix=$TEMP
gunzip $TEMP/*

echo -n > $FASTA # truncate
n=`ls $TEMP | cut -f 2 -d '.' | sort -n | tail -1` # number of files
for x in `seq $n`; do 
    cat "$TEMP/$DB.$x.protein.faa" >> $FASTA
done

#
# Install on Decypher TimeLogic
#

TARGET="so_refseq_$DB"
MACH="coedc05"
DESC=`date`

# delete target (if necessary)
dc_delete_target_rt -targ $TARGET -mach $MACH

# create new target
dc_new_target_rt -template format_aa_into_aa -source $FASTA -targ $TARGET -desc "$DESC" -mach $MACH
if [ $? -ne 0 ]; then
    die "failed to create refseq target $TARGET on $MACH."
fi
