#!/bin/bash
###############################################################################
# Initialize application root on production.
#
# This script should be run on an empty application root directory.
# You can run "repo_to_prod.sh" script to sync the initialized root with the 
# application code repository.
#
# Important: set server configuration in common.sh.
################################################################################

. common.sh

if [ $LOCALHOST != $PROD_HOST ]; then
    SSH="ssh $PROD_USER@$PROD_HOST"
fi

# check if application root directory is empty

LIST=`$SSH ls $PROD_DIR`
if [ $? -eq 0 ]; then
    if [ -n "$LIST" ]; then
        die "application root $PROD_HOST:$PROD_DIR is not empty."
    fi
else
    die "command fiailed: $SSH ls $PROD_DIR"
fi


# set permissions for application root
$SSH chmod u=rwxs,g=rxs,o= $PROD_DIR
if [ $? -ne 0 ]; then
    die "failed to set root directory permissions."
fi

# create user upload directory
$SSH mkdir -m ug=rwx,g=rwxs,o=rwx $PROD_DIR/upload
$SSH mkdir -m ug=rwx,g=rwxs,o=rwx $PROD_DIR/upload/tmp
$SSH mkdir -m ug=rwx,g=rwxs,o=rwx $PROD_DIR/upload/chunks
if [ $? -ne 0 ]; then
    die "failed to create user upload directory tree"
fi

# create application data directory
$SSH mkdir -m u=rwx,g=rwxs,o= $PROD_DIR/data
$SSH mkdir -m ug=rx,o= $PROD_DIR/data/session
$SSH mkdir -m u=rwx,g=rwxs,o= $PROD_DIR/data/project
$SSH mkdir -m u=rwx,g=rwxs,o= $PROD_DIR/data/refseq
if [ $? -ne 0 ]; then
    die "failed to create application data directory tree"
fi

# create application log directory
$SSH mkdir -m o=rwx,g=rx,o= $PROD_DIR/log
if [ $? -ne 0 ]; then
    die "failed to create application log directory"
fi

# done
echo "Finished successfully."
exit 0
