<?php
/**
 * Core Funciton Libaray
 * 
 * @package SnowyOwl
 * @subpackage Core
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

// project status codes
define('P_PENDING',   0);
define('P_RUNNING',   1);
define('P_ERROR',     2);
define('P_STOPPED',   3);
define('P_COMPLETED', 4);

// roles
define('R_ADMIN', 1);
define('R_USER',  2);
define('R_GUEST', 3);

/**
 * Fetch all users from database ordered by name.
 * If a group id is provided return only members of the group.
 * 
 * @uses Engine $_Engine
 * @param int $group_id if provided return users with the group.
 * @return array rows each representing a user
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_get_users($group_id=false) {

    global $_Engine;
    
    $params = array();
    $where = "";
    
    if ($group_id) {
        $params = array( (int) $group_id );
        $where = "JOIN auth_usergroup
                   ON auth_user.id = auth_usergroup.user_id
                  WHERE group_id = $1";
    }

    $sql = "SELECT auth_user.id, 
                   auth_user.name, 
                   auth_user.username,
                   auth_user.email
            FROM auth_user 
            $where 
            ORDER BY auth_user.name ASC";
    
    return $_Engine->selectAll($sql, $params);
}

/**
 * Fetch all user roles from database ordered by id.
 * 
 * @uses Engine $_Engine
 * @return array rows each representing a user role
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_get_roles() {

    global $_Engine;

    $sql = "SELECT id, title FROM auth_role ORDER BY id ASC";
    return $_Engine->selectAll($sql);
}

/**
 * Fetch all refseq databases.
 * 
 * @uses Engine $_Engine
 * @return array rows each representing a refseq database
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_get_refseq_dbs() {

    global $_Engine;

    $sql = "SELECT id, name, title FROM core_refseq ORDER BY id ASC";
    return $_Engine->selectAll($sql);
}

/**
 * Fetch all user status codes and labels from database ordered by id.
 * 
 * @uses Engine $_Engine
 * @return array rows each representing a possible user status
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_get_user_statuses() {

    global $_Engine;

    $sql = "SELECT status, title FROM user_status ORDER BY status ASC";
    return $_Engine->selectAll($sql);
}

/**
 * Fetch groups from database in which the given user is a member, 
 * ordered by title. If no user id is given, assume the logged in user.
 * 
 * @param int $user_id user id (optional)
 * @uses Engine $_Engine
 * @uses $_SESSION $_SESSION['user']['id'] the logged in user
 * @return array rows each representing a group
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_get_user_groups($user_id=false) {

    global $_Engine;

    if (empty($user_id)) {
        $user_id = $_SESSION['user']['id'];
    }
    
    $params = array();
    $params[0] = (int) $user_id;

    $sql = "SELECT id, 
                   title
        FROM auth_group
        JOIN auth_usergroup
          ON group_id = id
        WHERE user_id = $1
        ORDER BY title ASC";
    
    return $_Engine->selectAll($sql, $params);
}

/**
 * Fetch user account info.
 * 
 * @uses Engine $_Engine
 * @return array associative array representing user
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_get_user($id) {

    global $_Engine;

    $params = array( (int) $id );
    $sql = "SELECT *
            FROM auth_user
            WHERE id = $1";
    return $_Engine->selectOne($sql, $params);
}

/**
 * Fetch user group info.
 * 
 * @uses Engine $_Engine
 * @return array associative array representing user group
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_get_group($id) {

    global $_Engine;

    $params = array( (int) $id );
    $sql = "SELECT title, status
            FROM auth_group
            WHERE id = $1";
    return $_Engine->selectOne($sql, $params);
}

/**
 * Get project information.
 * 
 * @param int $project_id project identification number
 * @return row representing the project
 */
function CORE_get_project($project_id) {

    global $_Engine;

    $params = array();
    $params[0] = (int) $project_id;
    
    $sql = "SELECT core_project.id,
                   core_project.title,
                   core_project.description,
                   auth_group.title AS group,
                   TO_CHAR(core_project.created_on, 'Mon DD, YYYY HH24:MI') AS created_on,
                   auth_user.name AS creator
            FROM core_project
            JOIN auth_group ON
              core_project.group_id = auth_group.id
            JOIN auth_user ON
              core_project.created_by = auth_user.id
            WHERE core_project.id = $1";
    $res = $_Engine->db->query($sql, $params);
    $row = $res->fetch();
    
    return $row;
}

/**
 * Fetch groups from database.
 * 
 * Get all the groups listed in the database.
 * The array is sorted by group name.
 * 
 * @uses Engine $_Engine
 * @return array rows each representing a group
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_get_groups() {

    global $_Engine;

    $sql = "SELECT id, title, upload_dir
        FROM auth_group
        ORDER BY title ASC";
    
    return $_Engine->selectAll($sql);
}

/**
 * Fetch output files from database.
 * 
 * Get list of output filenames and descriptions from the database.
 * 
 * @uses Engine $_Engine
 * @return array rows each representing an output file indexed by group name
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_get_output_files($project_id=false) {

    global $_Engine;
    
    $join = '';
    $where = '';
    $size = '';
    $params = array();
    
    if ($project_id) {
        $join = "JOIN core_project_file ON core_project_file.file_id = project_file.id";
        $where = "WHERE core_project_file.project_id = $1 AND core_project_file.size <> 0";
        $size = "core_project_file.size,";
        $params = array( (int) $project_id );
    }
    
    $sql = "SELECT project_filegroup.name AS group,
                   project_file.id,
                   project_file.name,
                   project_file.filename,
                   $size
                   project_file.description,
                   project_file.required
        FROM project_file
        JOIN project_filegroup
            ON project_file.file_group = project_filegroup.id
        $join
        $where
        ORDER BY project_filegroup.id ASC, required DESC, project_file.name ASC";
        
    $rows = array();
    
    foreach ($_Engine->selectAll($sql, $params) as $row) {
        if (!key_exists($row['group'], $rows)) $rows[$row['group']] = array();
        array_push($rows[$row['group']], $row);
    }
    
    return $rows;
}

function CORE_get_output_filepath($project_id, $file_id) {
    
    global $_Engine;
    
    $outDir = DAT . 'project' . DS . $project_id . DS . 'out' . DS;
    $params = array( (int) $file_id );
    $sql = "SELECT filename FROM project_file WHERE id = $file_id";
    $row = $_Engine->selectOne($sql);
    return $outDir . $row['filename'];
}

/**
 * Create a project.
 * 
 * A project is created by a specific user but is associated with (belongs to) 
 * one group, determined at project creation.
 * The creator may or may not continue to be a member of the group.
 * 
 * @uses Engine $_Engine
 * @uses $_SESSION $_SESSION['user']['id'] current user id
 * @uses $_REQUEST $_REQUEST['title'] project title (required)
 * @uses $_REQUEST $_REQUEST['desc'] project title (optional)
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_add_project() {
    
    global $_Engine;
        
    $title = trim($_REQUEST['title']);
    $genome = $_REQUEST['genome'];
    $rnaseq = $_REQUEST['rnaseq'];
    $masked = (empty($_REQUEST['masked'])) ? null : $_REQUEST['masked'];
    $mrna = (empty($_REQUEST['mrna'])) ? null : $_REQUEST['mrna'];
    $phred64 = ($_REQUEST['map'] == 1) ? (int) $_REQUEST['phred64'] : null;
    $gid = (int) $_REQUEST['gid'];
    $uuid = $_REQUEST['uuid'];
    $maskseq = ($_REQUEST['mask_it'] == 1) ? (int) $_REQUEST['maskseq'] : 0;
    
    $pid = $_Engine->func('add_project', array(
        $title,
        trim($_REQUEST['desc']),
        $gid,
        (int) $_SESSION['user']['id'],
        $mrna,
        $genome,
        $rnaseq,
        $masked,
        $phred64,
        (int) $_REQUEST['map'],
        $maskseq,
        (int) $_REQUEST['refseq'],
        (int) $_REQUEST['imin'],
        (int) $_REQUEST['imax'],
        $_REQUEST['output'],
    ), false); // do not auto commit
        
    if (is_integer($pid) && $pid > 0) {
        
        $pDir = PRJ . $pid . DS;
        
        // do not set umask and pass mode parameter to mkdir
        // more reliable to use chmod. see php/umask doc.
        @mkdir($pDir);
        @chmod($pDir, 02770); 
        
        $genome_copied = rename(UPL . $uuid . '_' . $genome, $pDir . $genome);
        $rnaseq_copied = rename(UPL . $uuid . '_' . $rnaseq, $pDir . $rnaseq);
        if ($masked) $masked_copied = rename(UPL . $uuid . '_' . $masked, $pDir . $masked);
        if ($mrna) $mrna_copied = rename(UPL . $uuid . '_' . $mrna, $pDir . $mrna);
        if ($genome_copied && $rnaseq_copied && (!$masked || $masked_copied) && (!$mrna || $mrna_copied)) {
            $_Engine->db->commit();   // success
        } else {
            $_Engine->alert('Failed to import project input files.', Engine::ERROR);
            $_Engine->db->rollback(); // error
        }
    }
}

/**
 * Delete a project.
 * 
 * To delete a projcet, the user must be the owner or have an Admin role.
 * The project cannot be deleted if it is currently being processed by the 
 * backend daemon.
 * 
 * @param integer $project_id the id of the project to delete
 * @uses $_SESSION $_SESSION['user']['id'] logged in user id
 */
function CORE_delete_project($project_id) {
    
    global $_Engine;
    
    $_Engine->func('delete_project', array(
        (int) $project_id,             // id of project to delete
        (int) $_SESSION['user']['id']  // deleted by
    ));    
}

/**
 * Create a user account.
 * 
 * @uses Engine $_Engine
 * @uses $_REQUEST $_REQUEST['name'] full name
 * @uses $_REQUEST $_REQUEST['username'] username (login)
 * @uses $_REQUEST $_REQUEST['groups'] a non empty list of group ids
 * @uses $_REQUEST $_REQUEST['role'] user role
 * @uses $_SESSION $_SESSION['user']['id'] created by (user id)
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_add_user() {
    
    global $_Engine;
        
    $_Engine->func('add_user', array(
        trim($_REQUEST['name']),            // new user full name
        trim($_REQUEST['username']),        // new username
        md5(trim($_REQUEST['pass'])),       // encrypted password
        (int) $_REQUEST['role'],            // new user role
        implode(",", $_REQUEST['groups']),  // new user groups (ids)
        (int) $_SESSION['user']['id'],      // created by (user id)
        trim($_REQUEST['email'])            // email address
    ));
}

/**
 * Modify password for current user.
 * 
 * @uses Engine $_Engine
 * @uses $_SESSION $_SESSION['user']['id'] current user id
 * @uses $_REQUEST $_REQUEST['password'] full name
 * @uses $_REQUEST $_REQUEST['confirm'] full name
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_change_user_password($user_id=false) {
    
    global $_Engine;
    
    if (!$user_id) $user_id = (int) $_SESSION['user']['id'];
    
    $_Engine->func('change_password', array(
        $user_id,                            
        md5(trim($_REQUEST['pass'])),        // encrypted password
        md5(trim($_REQUEST['confirm'])),     // encrypted confirmation
        (int) $_SESSION['user']['id']        // changed by (user id)
    ));
}

/**
 * Modify user email notification settings.
 * 
 * @uses Engine $_Engine
 * @uses $_SESSION $_SESSION['user']['id'] current user id
 * @uses $_REQUEST $_REQUEST['notify'] notification flag (0 or 1)
 * @uses $_REQUEST $_REQUEST['email'] notification email address
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_change_email_notification($user_id=false) {
    
    global $_Engine;
    
    if (!$user_id) $user_id = (int) $_SESSION['user']['id'];
    
    $_Engine->func('change_notification', array(
        $user_id,
        trim($_REQUEST['email']),           // notify email address
        (int) ($_REQUEST['notify']),        // notify flag (0 or 1)
        (int) $_SESSION['user']['id']       // changed by (user id)
    ));
}

/**
 * Modify a user account.
 * 
 * Only admin user can modify a user account, and not their own.
 * Disabled user cannot login or submit new jobs; however, other users
 * in the group can still access the projcets owned by the disabled user.
 * 
 * @param integer $user_id the id of the user account to disable
 * @uses $_SESSION $_SESSION['user']['role'] current user role
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_modify_user($user_id) {
    
    global $_Engine;
    
    $_Engine->func('modify_user', array(
        (int) $user_id,                     // id of user account to modify
        (int) $_SESSION['user']['id'],      // modified by
        implode(',', $_REQUEST['groups']),  // new group ids (comma delimited)
        (int) $_REQUEST['role'],            // new role
        (int) $_REQUEST['status']           // new status
    ));
}

/**
 * Delete a user account.
 * 
 * Only admin user can delete a user account, and not their own.
 * An error is reported if the user owns any project.
 * The user can be disabled instead.
 * 
 * @param integer $user_id the id of the user account to delete
 * @uses $_SESSION $_SESSION['user']['id'] deleted by
 */
function CORE_delete_user($user_id) {
    
    global $_Engine;
    
    $_Engine->func('delete_user', array(
        (int) $user_id,                 // id of user account to delete
        (int) $_SESSION['user']['id']   // deleted by
    ));    
}

/**
 * Create a user group.
 * 
 * One or more members must be assigned to a group.
 * 
 * @uses Engine $_Engine
 * @uses $_REQUEST $_REQUEST['title'] group title (required)
 * @uses $_REQUEST $_REQUEST['updir'] path to upload directory (required)
 * @uses $_REQUEST $_REQUEST['users'] a non empty list of user ids (required)
 * @uses $_SESSION $_SESSION['user']['id'] created by
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_add_group() {
    
    global $_Engine;

    $title = trim($_REQUEST['title']);
    $updir = strtolower(str_replace(' ', '_', $title));

    $gid = $_Engine->func('add_group', array(
        $title, // group name
        $updir, // group upload directory basename
        implode(",", $_REQUEST['users']), // initial user id list
        (int) $_SESSION['user']['id']       // deleted by
            ), false);

    if (is_integer($gid) && $gid > 0) {

        // create group upload directory
        $dirpath = UPL . $updir;
        
        $created = true;
        if (!is_dir($dirpath)) 
            $created = @mkdir($dirpath);
        
        if (!$created) {
            $_Engine->db->rollback();
            $_Engine->alert("Failed to create group upload directory '$dirpath'.", Engine::ERROR);
        } else {
            $_Engine->db->commit();
        }
    }
}

/**
 * Delete a user group.
 * 
 * Only admin user can delete a user groups.
 * An error is reported if the group has one or more projects.
 * In this case the group must be disabled.
 * An error is reported if the group has one or more users.
 * Users can be removed from group before it can be deleted. 
 * Note taht all users must belong to at least one group.
 * This should be enfored by the application.
 * 
 * @uses Engine $_Engine
 * @param integer $group_id the id of the group to delete
 * @uses $_SESSION $_SESSION['user']['id'] deleted by
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_delete_group($group_id) {
    
    global $_Engine;
    
    $_Engine->func('delete_group', array(
        (int) $group_id,                 // id of group to delete
        (int) $_SESSION['user']['id']   // deleted by
    ));    
}

/**
 * Modify a user group.
 * 
 * One or more active members must be assigned to a group at all times.
 * 
 * @uses Engine $_Engine
 * @param integer $group_id the id of the group to modify
 * @uses $_REQUEST $_REQUEST['users'] a non empty list of user ids (required)
 * @uses $_REQUEST $_REQUEST['status'] new status code (required)
 * @uses $_SESSION $_SESSION['user']['id'] modified by
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_modify_group($group_id) {
    
    global $_Engine;
    
    $_Engine->func('modify_group', array(
        (int) $group_id,                   // id of group to modify
        implode(",", $_REQUEST['users']),  // list of users
        (int) $_REQUEST['status'],         // new staus code (1 or 2)
        (int) $_SESSION['user']['id']      // modified by
    ));
}

/**
 * Restart a project.
 * 
 * Change the project status to pending.
 * Removes assembly, mapping, and SnowyOwl generated files (log files are kept).
 * Displays an error message if the user is not an administrator,
 * or the project is currently, running, pending, or completed.
 * 
 * @uses Engine $_Engine
 * @param integer $project_id the id of the group to restart
 * @uses $_SESSION $_SESSION['user']['id'] restarted by
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_restart_project($project_id) {
    
    global $_Engine;
    
    // update database records
    $restarted = $_Engine->func('restart_project', array(
        (int) $project_id,            // project to restart
        (int) $_SESSION['user']['id'] // restarted by
    ));
}

function CORE_delete_output_file($project_id, $file_id) {
    
    global $_Engine;
    
    if ($file_id == 1) return false; 
    
    // get filename from database
    $params = array( (int) $file_id );
    $sql = "SELECT filename FROM project_file WHERE id = $1";
    $row = $_Engine->selectOne($sql, $params);
    $filename = $row['filename'];
    
    // delete file
    $outDir = PRJ . $project_id . DS . 'out' . DS;
    $deleted = @unlink($outDir . $filename);
    if (!$deleted) return false;
    
    // remove database entry if successful
    $params[1] = (int) $project_id;
    $sql = "DELETE FROM core_project_file 
            WHERE project_id = $2 
            AND file_id = $1";
    $_Engine->query($sql, $params);
    
    $_Engine->db->commit();
    
    // done
    return true;
}

/**
 * Remove a directory recursively.
 * 
 * Can be used to remove regular files as well.
 * Suppresses PHP Errors, and returns true on success and false on failure.
 * 
 * @param string $path a path to the file/directory to remove (recursively)
 * @return bool true on success and false on failure
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
function CORE_rmtree($path) {
        
    if (is_dir($path)) {
        foreach (scandir($path) as $name) {
            if (in_array($name, array('.', '..'))) continue;
            $subpath = $path . DS . $name;
            return CORE_rmtree($subpath);
        }
        return @rmdir($path);
    } else {
        return @unlink($path);
    }
}

?>
