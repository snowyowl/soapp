<?php
/**
 * AjaxTable Engine
 * 
 * @package SnowyOwl
 * @subpackage Ajax
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * AjaxTable PHP Engine Class.
 * 
 * This class is part of the AjaxTable model.
 * This class ensures correct communication 
 * with the JavaScript AjaxTable object.
 * The object is initialized with an SQL statement, and the result rows
 * are converted into an associative array, that is converted to
 * JSON by the AjaxEngine and sent to the JavaScript AjaxTable object
 * in JSON format.
 * 
 * @package SnowyOwl
 * @subpackage Ajax
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 * @example www/project/list.ajax.php simple usage example
 */
class AjaxTable {

    /**
     * Current batch size (max number of table rows to fetch and display).
     * 
     * @var int
     */
    private $_limit;

    /**
     * The index of the first row to fetch (0-based).
     * 
     * @var int
     */
    private $_offset;

    /**
     * The index of the column to sort by (1-based).
     * 
     * @var int 
     */
    private $_sortby;

    /**
     * The sorting order.
     * 
     * Allowed values are "ASC" and "DESC" 
     * (excluding double quotes and case sensitive).
     * 
     * @var string
     */
    private $_asc;

    /**
     * A database query object.
     *  
     * @var DBQuery
     * @see AjaxTable::submitQuery
     */
    private $_res;

    /**
     * AjaxTable constructor.
     * 
     * @uses $_REQUEST $_REQUEST['limit'] batch limit (defaults to 10)
     * @uses $_REQUEST $_REQUEST['offset'] offset (0-based)
     * @uses $_REQUEST $_REQUEST['sortby'] sorting column index (0-based)
     * @uses $_REQUEST $_REQUEST['asc'] sorting order ('ASC' or 'DESC')
     */
    function __construct() {

        $this->_limit = (isset($_REQUEST['limit'])) ? $_REQUEST['limit'] : 10;
        $this->_offset = (isset($_REQUEST['offset'])) ? $_REQUEST['offset'] : 0;
        $this->_sortby = (isset($_REQUEST['sortby'])) ? $_REQUEST['sortby'] : 0;
        $this->_asc = (isset($_REQUEST['asc']) && $_REQUEST['asc']) ? 'ASC' : 'DESC';

        $this->_res = null;

    }

    /**
     * Register the query to submit to database.
     * 
     * The SQL statement must:
     *  - not include an ORDER BY statement
     *  - give double quoted column aliases to preserve column name case
     *    ex. SELECT auth_user.name AS "Created By" ... displays Created By
     *        heading.
     * The statement can use GROUP BY and nested selects.
     * 
     * @param string $sql SQL statement
     * @param array $params SQL query parameters
     * @uses $_Engine to access database object
     */
    public function submitQuery($sql, $params = array()) {

        global $_Engine;

        $col = (int) $this->_sortby + 1;
        $asc = $this->_asc;
        $sql = "$sql ORDER BY $col $asc";
        $this->_res = $_Engine->query($sql, $params);
        if ($this->_res->errno()) {
            error($this->_res->error());
        }
    }

    /**
     * Fetch data using the registered query, and assign to the global
     * $_Engine object.
     * 
     * The function fetches only the rows to be displayed.
     * LIMIT and OFFSET SQL statements are not used for two reasons:
     *  1) the same statement can be run repeatedly 
     *     (can be cached by the server, paresed only once).
     *  2) for compatability with other major database engines like Oracle,
     *     which does not support LIMIT/OFFSET syntax.
     * 
     * @uses $_Engine to access database object
     */
    public function sendJSON() {

        global $_Engine;

        // fetch total row count (all rows that satisfy the registed SQL query)
        $total = $this->_res->num_rows();
        
        // fetch rows (and extract into local variables)
        $rows = array();        
        $n = $this->_offset + $this->_limit;
        if ($n > $total) $n = $total;
        $this->_res->seek($this->_offset);
        for ($i = $this->_offset; ($i < $n) && ($row = $this->_res->fetch(DBQuery::NUMERIC)); $i++) {
            array_push($rows, $row);
        }

        // fetch column names (to be used as column headings in the HTML table)
        $headers = $this->_res->field_names();

        // collect data in one associative array
        $_Engine->data = array();
        $_Engine->data['headers'] = $headers;
        $_Engine->data['rows'] = $rows;
        $_Engine->data['total'] = $total;
    }

}

?>
