<?php
/**
 * Web inteface engine.
 * 
 * @package SnowyOwl
 * @subpackage Engine
 * @author Omar Zabaneh <ozabaneh@calgary.ca> 
 */

/**
 * Web interface engine class.
 * 
 * Implements web page interface file basic functionality.
 * 
 * @package SnowyOwl
 * @subpackage Engine
 * @author Omar Zabaneh <ozabaneh@calgary.ca> 
 */
class WebEngine extends Engine {
    
    /**
     * Path to public JavaScript directory
     */
    const JSC = 'www/js/';
    
    /**
     * Path to public CSS/styles directory
     */
    const CSS = 'www/css/';
    
    /**
     * Holds relative paths to JavaScript files.
     * @var type array
     */
    private $_js;
    
    /**
     * Holds relative paths to CSS stylesheets.
     * @var type array
     */
    private $_css;
    
    /**
     * Name of current module (based on public path).
     * @var type string
     */
    private $_name;
    
    /**
     * Relative path to parent web directory.
     * 
     * Used to construct src attribute of script and link tags.
     * May not be the best approach. Should be revised (use base element?).
     * 
     * @var type string
     */
    private $_rel;
    
    /**
     * Sets module name and includes standard templates, scripts,
     * and stylesheets.
     * 
     * Included files, in the order listed:
     * 
     * Included Scripts: jQuery, jQuery.Validate, and global.js
     * Included Sytles: global.js
     * 
     * If a stylesheet and/or a script corresponding to the current module
     * exist, they are included.
     * 
     * Each module (interface PHP file under the web root: www) require
     * a corresponding template.
     * 
     * For example:
     * 
     * www/home.php requires tpl/home.tpl.
     * www/js/home.js and www/css/home.css will be automatically included
     * if they are created. To include other scripts/stylesheets, use
     * includeScript and includeStyle methods explicitly.
     */
    function __construct() {
        
        parent::__construct();
        
        $this->assign('title', $this->config('title'));

        $this->_js = array();
        $this->_css = array();
        
        // set engine name
        $name = str_replace(ROOT . 'www' . DS, '', $_SERVER['SCRIPT_FILENAME']);
        $this->_name = str_replace('.php', '', $name);
        $this->_rel = str_repeat('..' . DS, substr_count($this->_name, DS));
        
        // include default global scripts
        $this->includeScript('jquery.js');
        $this->includeScript('jquery.validate.js');
        $this->includeScript('global.js');
    
        // include global default styles
        $this->includeStyle('global.css');
                
        // inlcude optional script
        $js = ROOT . WebEngine::JSC . "$this->_name.js";
        if (file_exists($js))
            $this->includeScript("$this->_name.js");
        
        // include optional stylesheet
        $css = ROOT . WebEngine::CSS . "$this->_name.css";
        if (file_exists($css))
            $this->includeStyle("$this->_name.css");
        
    }
    
    /**
     * Include a JavaScript file.
     * 
     * A script tag is appended to the head element of the generated
     * web page. The sequence of calls determines the order of the
     * included scripts. The provided path must be relative to the 
     * public javascrpt directory (e.g. www/js).
     * 
     * @param string $filename relative path to javascript file
     */
    public function includeScript($filename) {
        
        if (file_exists(ROOT . WebEngine::JSC . $filename)) {
            array_push($this->_js, $this->_rel . 'js' . DS . $filename);
        } else {
            warn("failed to attach script file '$filename'.");
        }
    }
    
    /**
     * Include a CSS stylesheet.
     * 
     * A link tag is appended to the head element of the generated
     * web page. The sequence of calls determines the order of the
     * included stylesheets. The provided path must be relative to the 
     * public styles directory (e.g. www/css). 
     * By default, no media tag is set.
     * 
     * @param string $filename relative path to stylesheet
     * @param string $media value for stylesheet media.
     */
    public function includeStyle($filename, $media=false) {
        
        if (file_exists(ROOT . WebEngine::CSS . $filename)) {
            $css = array();
            $css['href'] = $this->_rel . 'css' . DS . $filename;
            if ($media) $css['media'] = $media;
            array_push($this->_css, $css);
        } else {
            warn("failed to attach stylesheet '$filename'.");
        }
        
    }
    
    /**
     * Display the web page.
     * 
     * Smarty is used to load the module template into
     * a standard site wide template: 'tpl/web.tpl'.
     */
    function __destruct() {
        
        $this->assign('css', $this->_css);
        $this->assign('js', $this->_js);
        
        // look for messages
        if (isset($_SESSION['error']) && !empty($_SESSION['error'])) {
            $this->alert($_SESSION['error'], Engine::ERROR);
            unset($_SESSION['error']);
        } else if (isset($_SESSION['warn']) && !empty($_SESSION['warn'])) {
            $this->alert($_SESSION['warn'], Engine::WARNING);
            unset($_SESSION['warn']);
        }
        
        // include main template
        $tpl = "$this->_name.tpl";
        if (!is_readable(TPL . $tpl))
            error("failed to load main template file '$tpl'.");
        $this->assign('body', $this->template($tpl));
        
        echo $this->template('web.tpl');
    }
    
}

?>