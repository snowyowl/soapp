<?php
/**
 * AJAX script engine.
 * 
 * @package SnowyOwl
 * @subpackage Engine
 * @author Omar Zabaneh <ozabaneh@calgary.ca> 
 */

/**
 * AJAX Script Engine Class.
 * 
 * Implements AJAX script basic functionality.
 * 
 * @package SnowyOwl
 * @subpackage Engine
 * @author Omar Zabaneh <ozabaneh@calgary.ca> 
 *
 */
class AjaxEngine extends Engine {
    
    /**
     * Store data to be encoded into JSON
     * @var type mixed
     */
    public $data;
    
    /**
     * AjaxEngine constructor.
     */
    function __construct() {

        parent::__construct();

        $this->data = array();
    }

    /**
     * AjaxEngine destructor.
     */
    function __destruct() {

        $json = json_encode($this->data);

        header('Content-Type: application/json');
        header('Content-Length: ' . strlen($json));
        echo $json;
    }

}

?>