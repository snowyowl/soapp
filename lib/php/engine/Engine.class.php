<?php
/**
 * Abstract application engine.
 * 
 * @package SnowyOwl
 * @subpackage Engine
 * @author Omar Zabaneh <ozabaneh@calgary.ca>
 */

/**
 * Engine base class.
 * 
 * Provides basic website facilities (for example, configurations, 
 * template system, database connections, etc.) to all types of scripts 
 * (web interface file, ajax backend script, download scripts, etc.)
 * Each page type is handled by a sub classes of this abstact Engine class.
 * 
 * @package SnowyOwl
 * @subpackage Engine
 * @author Omar Zabaneh <ozabaneh@calgary.ca>
 */
abstract class Engine {

    const CONFIG_FILENAME = 'config.ini';
    
    const DBMS = 'db_engine';
    const DBHOST = 'db_host';
    const DBUSER = 'db_webuser';
    const DBPASS = 'db_webpass';
    const DBNAME = 'db_name';
    
    const ERROR = 0;
    const SUCCESS = 1;
    const WARNING = 2;
    
    const LIMITED = 0;
    const ADMIN = 1;
    
    /**
     * Global configuration object
     * @var array 
     */
    private $_config;

    /**
     * Smarty template engine
     * @var Smarty
     */
    private $_smarty;

    /**
     * Default database object
     * @var DB
     */
    public $db;
    
    /**
     * Engine constructor.
     * 
     * Site wide engine initialization, inlcuding session handling, 
     * template engine, default database object, configuration module,
     * and authentication.
     */
    function __construct() {
        
        // setup session handling
        session_save_path(DAT . 'session');
        ini_set('session.gc_maxlifetime', 3*60*60); // 3 hours
        ini_set('session.gc_probability', 1);
        ini_set('session.gc_divisor', 100);
        ini_set('session.cookie_secure', false);
        ini_set('session.use_only_cookies', true);
        session_start();
        
        // error logging
        ini_set('log_errors', 1);
        
        // set HTTP response headers
        header("Pragma: public");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
                
        // load global configuration
        $this->_loadConfig();

        // set default timezone
        date_default_timezone_set($this->config('timezone'));
        
        // init default database object
        $this->_initDB();

        // init Smarty template engine
        $this->_initSmarty();
        
        // authenticate user
        $this->_loginUser();
    }
    
    /**
     * Restrict access to logged in (authenticated) users.
     * 
     * Unauthorized requests are redirected to the index page.
     */
    public function requireLogin() {
        
        if (!isset($_SESSION['user'])) {
            header('Location: /index.php');
            exit();
        }
        
    }
    
    /**
     * Restrict access to administrative users.
     * 
     * Unauthorized requests are redirected to the index page.
     */
    public function requireAdmin() {
        
        if (!isset($_SESSION['user']) ||
               (isset($_SESSION['user']) &&
                      $_SESSION['user']['role'] != Engine::ADMIN)) {
            header('Location: /index.php');
            exit();
        }
        
    }
    
    /**
     * Load configuration variable.
     * 
     * Performes error handling and displays appropriate error messages.
     * 
     * @param string $name configuration variable name
     * @param bool $required if true (default) a value is required
     * @return mixed configuration variable value
     */
    public function config($name, $required=true) {
        
        // check if directive is defined
        if (!isset($this->_config[$name])) {
            error("Configuration error: missing '$name' directive.");
            
        } else if ($required) {
            if (empty($this->_config[$name])) {
                error("Configuration error: missing value for '$name' directive.");
            } else {
                return $this->_config[$name];
            }
            
        } else {
            return "";
        }
    }

    /**
     * Fetch (render) template file.
     * 
     * @param string $tpl_rel_path relative path to template file (under TPL)
     * @return string rendered template output
     */
    public function template($tpl_rel_path) {
        return $this->_smarty->fetch($tpl_rel_path);
    }
    
    /**
     * Assign a variable to the template system.
     * 
     * A variable must be assigned to the template system (object)
     * before it is accessible in a template file.
     * The current template system (object) in use is Smarty.
     * 
     * @see http://www.smarty.net/docs/en/
     * @param string $name
     * @param mixed $value
     */
    public function assign($name, $value) {
        $this->_smarty->assign($name, $value);
    }
    
    /**
     * An alias to db->query.
     * 
     * @see DBQuery::query()
     * @param string $sql sql statement
     * @param array $params sql statement parameters
     * @return DBQuery database query object
     */
    public function query($sql, $params=array()) {
        return $this->db->query($sql, $params);
    }

    /**
     * Perform a select query and return the first row.
     * 
     * Returns an array representing the first row, or false if no 
     * matching rows are found.
     * 
     * @param string $sql the select query
     * @param array $params parameters to be bound to the query
     * @return mixed array representing the first row, or false.
     */
    public function selectOne($sql, $params=array()) {
        
        $res = $this->query($sql, $params);
        if ($res->errno()) error($res->error());
        return $res->fetch();
    }
    
    /**
     * Perform a select query and return all matching rows.
     * 
     * @param string $sql the select query
     * @param array $params parameters to be bound to the query
     * @return array all result rows.
     */
    public function selectAll($sql, $params=array()) {
        
        $rows = array();
        $res = $this->query($sql, $params);
        if ($res->errno()) error($res->error());
        
        while ($row = $res->fetch()) {
            array_push($rows, $row);
        }
        
        return $rows;
    }
    
    /**
     * Call a database function.
     * 
     * The database function must return exception on error
     * or notice on success. If the function returns an integer it is returned 
     * on success
     * 
     * @param string $name procedure name
     * @param array $params parameters to be bound to the procedure
     * @param bool $autocommit autocommit transaction if true (default)
     * @return mixed false on failure, true or id on success
     */
    public function func($name, $params, $autocommit=true) {
        
        // build query
        $columns = array();
        for ($i = 1; $i <= count($params); $i++) {
            array_push($columns, "$$i");
        }
        $column_list = implode(',', $columns);
        $sql = "SELECT * FROM $name($column_list) AS id";
        
        $res = $this->query($sql, $params);
        if (!$res->errno()) {
            // success
            $row = $res->fetch();
            if ($autocommit) $this->db->commit();
            $this->alert($res->notice(), Engine::SUCCESS);
            return (!empty($row) && isset($row['id'])) ? (int) $row['id'] : true;
        
        } else if ($res->errno() == PgSQLQuery::RAISE_EXCEPTION) {
            // non-fatal error
            if ($autocommit) $this->db->rollback();
            $this->alert($res->error(), Engine::ERROR);
            return false; 
            
        } else {
            // fatal error
            error($res->error());
        }
    }    
    
    /**
     * Alert message to the user.
     * 
     * Assigns a message (and a message type) to the template system.
     * Message types include ERROR, SUCCESS, and WARNNING.
     * 
     * @param string $message passed to the template system as $msg
     * @param string $type passed to the template system as $msg_class
     */
    public function alert($message, $type=Engine::SUCCESS) {
        
        $this->assign('msg', htmlentities($message));
        switch ($type) {
            case Engine::ERROR:
                $this->assign('msg_class', 'error');
                break;
            case Engine::SUCCESS:
                $this->assign('msg_class', 'success');
                break;
            case Engine::WARNING:
                $this->assign('msg_class', 'warn');
                break;
            default:
                
        }
    }
        
    /**************************************************************************
     * Private Methods
     *************************************************************************/
    
    /**
     * Load the site configuration file.
     */
    private function _loadConfig() {

        // load global configurations
        $conf = ROOT . DS . Engine::CONFIG_FILENAME;
        $this->_config = @parse_ini_file($conf);
        if ($this->_config === false) // strict equality test
            error("Error loading global configuration file $conf.");
    }

    /**
     * Load Smarty template engine.
     */
    private function _initSmarty() {
        
        // basic setup
        $this->_smarty = new Smarty();
        $this->_smarty->setCompileDir(sys_get_temp_dir());
        $this->_smarty->setCacheDir(sys_get_temp_dir());
        $this->_smarty->setTemplateDir(TPL);
        
        // load some config sections into smarty
        $conf = ROOT . DS . Engine::CONFIG_FILENAME;
        $this->_smarty->configLoad($conf, 'main');
        $this->_smarty->configLoad($conf, 'user');
                
        // register modifiers
        function smarty_modifier_du($size)
        {
          $size = max(0, (int)$size);
          $units = array( 'b', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb');
          $power = $size > 0 ? floor(log($size, 1024)) : 0;
          return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
        }
        $this->_smarty->registerPlugin('function', 'du', 'smarty_modifier_du');

    }
    
    /**
     * Initialize the default database object.
     * 
     * Available as $_Engine->db.
     */
    private function _initDB() {

        // load database object
        $engine = $this->config(Engine::DBMS);
        switch ($engine) {
            case 'mysql': $this->db = new MySQL();
                break;
            case 'pgsql': $this->db = new PgSQL();
                break;
            case 'oracle': $this->db = new Oracle();
                break;
            default:
                $name = Engine::DBMS;
                error("Configuration error: invalid value '$engine' for '$name' directive.");
                break;
        }

        // set connection parameters
        $this->db->host = $this->config(Engine::DBHOST, false);
        $this->db->user = $this->config(Engine::DBUSER);
        $this->db->pass = $this->config(Engine::DBPASS);
        $this->db->name = $this->config(Engine::DBNAME);
        
        // set default time zone for session
        $timezone = pg_escape_string($this->config('timezone')); // not portable
        $sql = "SET TIME ZONE '$timezone'";
        $params = array();
        $this->db->query($sql);
    }

    /**
     * User authentication.
     * 
     * Handles user login and logout, if necessary.
     * @see Engine::requireLogin()
     */
    private function _loginUser() {
                
        if (isset($_SESSION['user']) && isset($_REQUEST['logoutBtn'])) {

            session_unset($_SESSION['user']); // log user out
            
        } else if (!isset($_SESSION['user']) && isset($_REQUEST['loginBtn']) 
                && isset($_REQUEST['username']) && isset($_REQUEST['pass'])) {

            // log user in
            $params = array();
            $params[] = trim($_REQUEST['username']);
            $params[] = md5($_REQUEST['pass']);
            $sql = "SELECT * FROM authenticate($1, $2)";
            $row = $this->selectOne($sql, $params);

            if (empty($row['id'])) {

                // login failed
                $this->alert('login failed. please try again.', Engine::ERROR);
            
            } else {

                // login successful
                $_SESSION['user'] = $row;
                
                // redirect to home page
                $home = $this->config('web_home');
                header("Location: http://$_SERVER[HTTP_HOST]/$home");
                exit();
            }
        }
    }

}

?>
