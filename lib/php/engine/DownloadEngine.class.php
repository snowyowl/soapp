<?php
/**
 * Download script engine.
 * 
 * @package SnowyOwl
 * @subpackage Engine
 * @author Omar Zabaneh <ozabaneh@calgary.ca> 
 */

/**
 * Download Script Engine Class.
 * 
 * Implements download script basic functionality.
 * 
 * @package SnowyOwl
 * @subpackage Engine
 * @author Omar Zabaneh <ozabaneh@calgary.ca> 
 *
 */
class DownloadEngine extends Engine {
    
    /**
     * Store filename to send to client
     * @var string
     */
    private $_filename;
    
    /**
     * AjaxEngine constructor.
     */
    function __construct() {

        parent::__construct();
        
        $this->_filename = '';
    }

    /**
     * Add path to file to send to client.
     * Nothing is added if file does not exist, and a warning is triggered.
     * 
     * @param string $filename path to file
     */
    public function addFile($filename) {
        if (file_exists($filename)) {
            $this->_filename = $filename;
        } else {
            warn("file not found: '$filename'");
        }
    }
    
    /**
     * AjaxEngine destructor.
     */
    function __destruct() {
        

        if (!empty($this->_filename)) {
            
            $path = $this->_filename;
            $name = basename($this->_filename);
                        
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
        
            header('Content-Disposition: attachment; filename=' . $name);
            header('Content-Length: ' .  filesize($path));
            
            @ob_end_clean();
            readfile($path);
            exit;
        }
    }

}

?>