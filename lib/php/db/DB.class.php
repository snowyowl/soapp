<?php
/**
 * Abstract database interface file.
 * 
 * This file provides the abstract database classes that implement
 * basic database connection and query functionality, and ensures common
 * interface while maintaining a thin layer of abstraction to allow subclasses
 * to implement advance features that are specific to the DBMS they represent.
 * 
 * @package SnowyOwl
 * @subpackage DB
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * Abstact database class.
 * 
 * Provides subclasses with database connectivity facilities.
 * All public members (host, user, ... etc) must be set before the first query
 * is sent (a connection is made).
 * 
 * @package SnowyOwl
 * @subpackage DB
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
abstract class DB {
    
    const OK = 0;
    
    /**
     * Database name.
     * @var string 
     */
    public $name;
    
    /**
     * Database host.
     * @var string
     */
    public $host;
    
    /**
     * Database user.
     * @var string
     */
    public $user;
    
    /**
     * Database (clear text) password.
     * @var string
     */
    public $pass;
    
    /**
     * Database connection resource.
     * @var resource
     */
    protected $_conn;
    
    /**
     * Database class constructor.
     */
    function __construct() {
        
        $this->host = 'localhost';
        $this->name = '';
        $this->user = '';
        $this->pass = '';
        
        $this->_conn = null;
    }
    
    /**
     * Open database connection.
     * 
     * This function must be implemented by all sub classes. 
     */
    abstract protected function _connect();
    
    /**
     * Query database.
     * 
     * This function must be implemented by all sub classes. 
     * @param string $sql SQL statement
     * @param array $params optional parameters for the SQL statement
     * @return DBQuery a database query object
     */
    abstract public function query($sql, $params);
    
    /**
     * Rollback transaction. 
     * 
     * This function must be implemented by all sub classes. 
     */
    abstract public function rollback();
    
    /**
     * Commit transaction. 
     * 
     * This function must be implemented by all sub classes. 
     */
    abstract public function commit();
}

/**
 * Abstract database query class.
 * 
 * Provides facilities to access query results.
 * 
 * @package SnowyOwl
 * @subpackage DB
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
abstract class DBQuery {
    
    const BOTH = 0;
    const NUMERIC = 1;
    const ASSOC = 2;
    
    /**
     * Database connection resourse
     * @var resourse
     */
    protected $_conn;
    
    /**
     * Database query result resource
     * @var resource 
     */
    protected $_res;
    
    /**
     * Query string (unparsed)
     * @var string
     */
    protected $_sql;
    
    /**
     * Query input parameters.
     * @var array
     */
    protected $_params;
    
    /**
     * Query error code
     * @var string
     */
    protected $_errno;
    
    /**
     * Query error message
     * @var string
     */
    protected $_errmsg;
    
    /**
     * Database query constructor.
     * 
     * Must be called by all subclasses before any initialization 
     * to ensure proper functioning.
     * 
     * @param resource $conn database connection resource
     * @param string $sql query string (unparsed)
     * @param type $params query input parameters
     */
    function __construct($conn, $sql, $params) {
        
        $this->_conn = $conn;
        $this->_sql = $sql;
        $this->_params = $params;
        $this->_res = null;
        $this->_errno = '';
        $this->_errmsg = '';
    }

    /**
     * Fetch next row from the query result.
     * 
     * Must be implemented by all base classes.
     * 
     * @param int $mode DBQuery::BOTH | DBQuery::NUMERIC | DBQuery::ASSOC
     */
    abstract public function fetch($mode);
    
    /**
     * Get error code.
     * 
     * @return string error code.
     */
    public function errno() {
        return $this->_errno;
    }
    
    /**
     * Get error message from dbms.
     * 
     * @return string error code.
     */
    public function error() {
        return $this->_errmsg;
    }
    
    /**
     * Get number of rows in a result set.
     */
    abstract function num_rows();
    
    /**
     * Get number of fields in a result set.
     */
    abstract function num_fields();
    
    /**
     * get the names of all fields in a result set.
     */
    abstract function field_names();
    
    /**
     * Change the next row to fetch to $row_index
     * @param int row index (0-based)
     * @see DBQuery::fetch()
     */
    abstract function seek($row_index);
}

?>
