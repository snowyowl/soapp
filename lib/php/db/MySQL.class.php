<?php
/**
 * Oracle database interface file.
 * 
 * Provides a thin database connection and query layer to interface with MySQL
 * database engine.
 * 
 * @package SnowyOwl
 * @subpackage DB
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * MySQL database class. 
 * 
 * @package SnowyOwl
 * @subpackage DB
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
class MySQL extends DB {

    /**
     * Create a connection to a MySQL database.
     * 
     * Create a new connection to ensure transactional isolation.
     * 
     * @return boolean true if a connection is created.
     */
    protected function _connect() {

        $db = "$this->host/$this->name";
        $this->_conn = @mysqli_connect($this->host, $this->user, $this->pass, $this->name);
        if (!$this->_conn)
            error("host=$this->host user=$this->user db=$this->db");
    }

    /**
     * Run database query.
     * 
     * Accepts any query string and optional query input parameters.
     * Create a connection just before running the first query.
     * 
     * @param string $sql query string (unparsed)
     * @param array $params optional query input parameters.
     * @return OracleQuery
     */
    public function query($sql, $params=array()) {

        // connect if necessary
        if (!$this->_conn)
            $this->_connect();

        return new MySQLQuery($this->_conn, $sql, $params);
    }

    /**
     * Rollback current transaction. 
     */
    public function rollback() {
        if ($this->_conn)
            mysqli_rollback($this->_conn);
    }

    /**
     * Commit current transaction. 
     */
    public function commit() {
        if ($this->_conn)
            mysqli_commit($this->_conn);
    }

    /**
     * MySQL class destructor.
     */
    function __destruct() {
        if ($this->_conn)
            mysqli_close($this->_conn);
    }

}

/**
 * MySQL database query class.
 * 
 * @package SnowyOwl
 * @subpackage DB
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
class MySQLQuery extends DBQuery {

    /**
     * MySQL database query object constructor.
     * 
     * @param resource $conn database connection resource
     * @param string $sql query string (unparsed)
     * @param type $params query input parameters
     */
    function __construct($conn, $sql, $params) {

        parent::__construct($conn, $sql, $params);

        // prepare statement
        $stmt = @mysqli_prepare($this->_conn, $this->_sql);
        if (!$stmt)
            $this->_error();

        // bind params
        $args = array($stmt, str_repeat('s', count($this->_params)));
        for ($i = 0; $i < count($this->_params); $i++) {
            $args[] = &$this->_params[$i];
        }
        $bound = @call_user_func_array('mysqli_stmt_bind_param', $args);
        if (!$bound)
            $this->_error();

        // execute statement
        $executed = @mysqli_stmt_execute($stmt);
        if (!$executed)
            $this->_error();
        
        // load result set from staement and save
        $this->_res = @mysqli_stmt_get_result($stmt);
        if (!$this->_res)
            $this->_error();
                
        @mysqli_stmt_close($stmt);
    }

    /**
     * Fetch result row.
     * 
     * Returns associative array representing the next row in the query result
     * or false (if no more rows are available).
     * 
     * @return mixed associative array or false.
     */
    public function fetch() {
        return mysqli_fetch_assoc($this->_res);
    }
    
    /**
     * Mysql database query object destructor.
     */
    function __destruct() {
        mysqli_free_result($this->_res);
    }

}

?>
