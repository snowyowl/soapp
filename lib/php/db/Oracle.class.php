<?php

/**
 * Oracle database interface file.
 * 
 * Provides a thin database connection and query layer to interface with Oracle
 * database engine.
 * 
 * @package SnowyOwl
 * @subpackage DB
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * Oracle database class. 
 * 
 * @package SnowyOwl
 * @subpackage DB
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
class Oracle extends DB {

    /**
     * Create connection to an Oracle database.
     * 
     * Create a new connection to ensure transactional isolation.
     * 
     * @return boolean true if a connection is created.
     */
    protected function _connect() {

        $db = "$this->host/$this->name";
        $this->_conn = @oci_new_connect($this->user, $this->pass, "$db");
        if (!$this->_conn)
            error("user=$this->user, db=$db");
    }

    /**
     * Run database query.
     * 
     * Accepts any query string and optional query input parameters.
     * Create a connection just before running the first query.
     *
     * @param string $sql query string (unparsed)
     * @param array $params optional query input parameters.
     * @return OracleQuery 
     */
    public function query($sql, $params=array()) {

        // connect if necessary
        if (!$this->_conn)
            $this->_connect();

        return new OracleQuery($this->_conn, $sql, $params);
    }

    /**
     * Rollback current transaction. 
     */
    public function rollback() {
        if ($this->_conn)
            oci_rollback($this->_conn);
    }

    /**
     * Commit current transaction. 
     */
    public function commit() {
        if ($this->_conn)
            oci_commit($this->_conn);
    }

    /**
     * Oracle class destructor.
     */
    function __destruct() {
        if ($this->_conn)
            oci_close($this->_conn);
    }

}

/**
 * Oracle database query class.
 * 
 * @package SnowyOwl
 * @subpackage DB
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
class OracleQuery extends DBQuery {

    /**
     * Oracle database query object constructor.
     * 
     * @param resource $conn database connection resource
     * @param string $sql query string (unparsed)
     * @param type $params query input parameters
     */
    function __construct($conn, $sql, $params) {

        parent::__construct($conn, $sql, $params);

        // parse sql
        $this->_res = @oci_parse($conn, $sql);
        if (!$this->_res)
            $this->_error();

        // bind params
        foreach (array_keys($params) as $name) {
            oci_bind_by_name($this->_res, $name, $params[$name]);
        }

        // execute
        $executed = @oci_execute($this->_res);
        if (!$executed)
            $this->_error();
    }

    /**
     * Oracle database query object destructor.
     */
    function __destruct() {
        oci_free_statement($this->_res);
    }

    /**
     * Fetch result row.
     * 
     * Returns associative array representing the next row in the query result
     * or false (if no more rows are available).
     * 
     * @return mixed associative array or false.
     */
    public function fetch() {
        return oci_fetch_assoc($this->_res);
    }

}

?>
