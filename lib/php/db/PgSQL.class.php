<?php
/**
 * PostgreSQL database interface file.
 * 
 * Provides a thin database connection and query layer to interface with 
 * PostgreSQL database engine.
 * 
 * @package SnowyOwl
 * @subpackage DB
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */

/**
 * PostgreSQL database class.
 * 
 * @package SnowyOwl
 * @subpackage DB
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
class PgSQL extends DB {

    /**
     * Create a connection to a PostgreSQL database.
     * 
     * Create a new connection to ensure transactional isolation.
     * 
     * @return boolean true if a connection is created.
     */
    protected function _connect() {
        
        if (!empty($this->host)) {
            $cs = "host=$this->host dbname=$this->name user=$this->user password=$this->pass";
        } else {
            $cs = "dbname=$this->name user=$this->user password=$this->pass";
        }
        
        $this->_conn = @pg_connect($cs, PGSQL_CONNECT_FORCE_NEW);
        if (!$this->_conn)
            error("host=$this->host dbname=$this->name user=$this->user password=*****");
    }

    /**
     * Run database query.
     * 
     * Accepts any query string and optional query input parameters.
     * Create a connection just before running the first query.
     * 
     * @param string $sql query string (unparsed)
     * @param array $params optional query input parameters.
     * @return OracleQuery 
     */
    public function query($sql, $params=array()) {

        // connect if necessary
        if (!$this->_conn)
            $this->_connect();
            $this->begin();

        return new PgSQLQuery($this->_conn, $sql, $params);
    }
    
    /**
     * Rollback current transaction. 
     */
    public function begin() {
        if ($this->_conn)
            pg_query($this->_conn, 'BEGIN');
    }

    /**
     * Rollback current transaction. 
     */
    public function rollback() {
        if ($this->_conn)
            pg_query($this->_conn, 'ROLLBACK');
    }

    /**
     * Commit current transaction.
     */
    public function commit() {
        if ($this->_conn)
            pg_query($this->_conn, 'COMMIT');
    }

    /**
     * PostgreSQL class destructor.
     */
    function __destruct() {
        if ($this->_conn)
            pg_close($this->_conn);
    }

}

/**
 * PostgreSQL database query class.
 * 
 * @package SnowyOwl
 * @subpackage DB
 * @author Omar Zabaneh <ozabaneh@ucalgary.ca>
 */
class PgSQLQuery extends DBQuery {
    
    const SUCCESSFUL_COMPLETION = '00000';
    const UNIQUE_VIOLATION = '23505';
    const RAISE_EXCEPTION = 'P0001';

    /**
     * PostgreSQL database query object constructor.
     * 
     * @param resource $conn database connection resource
     * @param string $sql query string (unparsed)
     * @param type $params query input parameters
     */
    function __construct($conn, $sql, $params) {

        parent::__construct($conn, $sql, $params);

        // send query
        $sent = @pg_send_query_params($conn, $sql, $params);
        if (!$sent) error(pg_last_error($conn));
        
        // get result
        $this->_res = @pg_get_result($conn);
        if (!$this->_res) error(pg_last_error($conn));
        
        // check for errors
        $code = pg_result_error_field($this->_res, PGSQL_DIAG_SQLSTATE);
        $this->_errno = ($code) ? $code : DB::OK;
        $this->_errmsg = trim(@pg_result_error($this->_res));
    }

    /**
     * PostgreSQL database query object destructor.
     */
    function __destruct() {
        pg_free_result($this->_res);
    }

    /**
     * Fetch result row.
     * 
     * Returns an array representing the next row in the query result
     * or false (if no more rows are available). The array may be indexed by
     * field name (default), by field index (numeric) or both.
     * 
     * @param int $mode DBQuery::BOTH | DBQuery::NUMERIC | DBQuery::ASSOC (default)
     * @return mixed array of field values
     */
    public function fetch($mode=DBQuery::ASSOC) {

        switch ($mode) {
            case DBQuery::ASSOC:
                return pg_fetch_array($this->_res, null, PGSQL_ASSOC);
            case DBQuery::NUMERIC:
                return pg_fetch_array($this->_res, null, PGSQL_NUM);
            default:
                return pg_fetch_array($this->_res, null, PGSQL_BOTH);
        }
    }

    public function notice() {
        return @pg_last_notice($this->_conn);
    }
    
    /**
     * Change the next row to fetch to $row_index
     * @param int row index (0-based)
     * @see PgSQLQuery::fetch()
     */
    public function seek($row_index) {
        pg_result_seek($this->_res, $row_index);
    }
    
    /**
     * Get number of rows in a result set.
     * @return int number of rows
     */
    public function num_rows() {
        return pg_num_rows($this->_res);
    }
    
    /**
     * Get number of fields in a result set.
     * @return int number of fields
     */
    public function num_fields() {
        return pg_num_fields($this->_res);
    }
    
    /**
     * Get the names of all fields in a result set.
     * @return array field names
     */
    public function field_names() {
        $names = array();
        for ($i = 0; $i < $this->num_fields(); $i++) {
            array_push($names, pg_field_name($this->_res, $i));
        }
        return $names;
    }
}

?>
    