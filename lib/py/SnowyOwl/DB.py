"""
.. module:: DB
    :platform: Unix
    :synopsis: PostgreSQL client library

Provides an interface to PostgreSQL database.
"""

import psycopg2
import psycopg2.extras

class PgSQL:
    """
    PostgreSQL client.
    """

    FETCH_ARRAY = 0
    FETCH_ASSOC = 1 # both

    IntegrityError = psycopg2.IntegrityError

    def __init__(self, host, name, user, password):
        """
        Create a connection to PostgreSQL database.
        A begin statement is implicitly sent once connected.

        :param host: hostname - if host parameter is empty, use local socket
        :type host: string

        :param name: database name
        :type name: string

        :param user: login user name (role)
        :type user: string

        :param password: (unencrypted) user login password
        :type password: string
        """

        self._conn = None

        if host:
            dsn = 'host=%s dbname=%s user=%s password=%s' % (host, name, user, password)
        else:
            dsn = 'dbname=%s user=%s password=%s' % (name, user, password)

        self._conn = psycopg2.connect(dsn)
        self._conn.autocommit = True

    def dsn(self):
        """
        :returns: a the connection string used by the connection
        :rtype: string
        """
        if self._conn:
            return self._conn.dsn

    def query(self, sql, params):
        """
        Submit a query to the database.

        :param sql: sql statement
        :type sql: string

        :param params: list of paramters (optional)
        :type params: list

        :returns: row count
        :rtype: int
        """
        curs = self._conn.cursor()
        curs.execute(sql, params)
        rowcount = curs.rowcount
        curs.close()
        return rowcount

    def _get_cursor(self, result_type=FETCH_ARRAY):
        if result_type == PgSQL.FETCH_ASSOC:
            return self._conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        return self._conn.cursor()

    def select(self, sql, params=[], result_type=FETCH_ARRAY):
        """
        Submit a select query to the database and return all rows.

        :param sql: sql statement
        :type sql: string

        :param params: list of paramters (optional)
        :type params: list

        :returns: generate rows from result set
        :rtype: tuple
        """
        curs = self._get_cursor(result_type)
        curs.execute(sql, params)
        rows = curs.fetchall()
        curs.close()
        return rows

    def selectOne(self, sql, params=[], result_type=FETCH_ARRAY):
        """
        Submit a select query to the database and return one row.

        :param sql: sql statement
        :type sql: string

        :param params: list of paramters (optional)
        :type params: list

        :returns: generate rows from result set
        :rtype: tuple
        """
        curs = self._get_cursor(result_type)
        curs.execute(sql, params)
        row = curs.fetchone()
        curs.close()
        return row

    def func(self, funcname, params=[]):
        """
        Call a database function.

        :param sql: function name
        :type sql: string

        :param params: list of paramters (optional)
        :type params: list

        :returns: result set
        :rtype: PgResult
        """
        curs = self._conn.cursor()
        curs.callproc(funcname, params)
        for row in curs.fetchall():
            yield row

    def __del__(self):

        if self._conn and not self._conn.closed:
            self._conn.close()
            
        
            
