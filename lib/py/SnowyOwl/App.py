"""
.. module:: App
    :platform: Unix
    :synopsis: SnowyOwl Daemon Process

.. codeauthor:: Omar Zabaneh <ozabaneh@ucalgary.ca>

"""

import time
import logging
import subprocess
import shlex
import traceback
import sys
import os
import ConfigParser
import gzip
import zipfile
import multiprocessing
import shutil
import tempfile
import csv
import grp
import smtplib

from email.mime.text import MIMEText

from SnowyOwl.Daemon import Daemon
from SnowyOwl.DB import PgSQL

from Bio.SeqIO.FastaIO import SimpleFastaParser
from Bio.SeqIO.QualityIO import FastqGeneralIterator

P_PENDING = 0
P_RUNNING = 1
P_ERROR = 2
P_STOPPED = 3
P_COMPLETED = 4

#
# Exceptions
#

class SOHostError(Exception):
    """
    Raised by SOHost class if exit status of a command is not equal to 0.
    """

    def __init__(self, cmd, cwd, output, retcode):
        self.cmd = cmd
        self.cwd = cwd
        self.output = output
        self.retcode = retcode

    def __str__(self):
        return "command failed [exit status: %d] %s" % (self.retcode, self.output)


class SOConfigParser(ConfigParser.RawConfigParser):
    """
    Modified ConfigParser

    Configuration values (strings) may be double quoted.
    This is important since the configuration file is shared with the web interface code (PHP).
    """

    def get(self, section, option):
        """
        Strip off double quotes.
        """
        return ConfigParser.RawConfigParser.get(self, section, option).strip('"')

#
# Helper Classes
#

class SOHost:
    """
    Interface for running a command locally.
    """

    def __init__(self):
        self.proc = None # current running process

    def run(self, cmd, cwd, shell=False):
        """
        Run command on local host.

        :param cmd: the command line to run
        :type cmd: string

        :param cwd: the directory under which to run the command
        :type cwd: string

        :param shell: allow shell features (try to avoid if possible, optional)
        :type shell: bool

        :raises: :exc:`SOHostError` if the command returns a non-zero exit status
        """

        log = logging.getLogger('soproject')

        # run command locally
        log.info("%s (pwd=%s)" % (cmd, cwd))
        if not shell:
            cmd = shlex.split(cmd)
        self.proc = subprocess.Popen(cmd, cwd=cwd, shell=shell,
                                     stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = self.proc.communicate()
        retcode = self.proc.poll()
        self.proc = None
        if retcode != 0:
            raise SOHostError(cmd, cwd, output + error, retcode)

        log.debug("[retcode] %d" % retcode)
        log.debug("[stdout] %s" % output)
        log.debug("[stderr] %s" % error)

    def terminate(self):

        if self.proc:
            self.proc.terminate()

    def __del__(self):

        self.terminate()

class SOProject:
    """
    Stores project paramters and project related paths.
    """

    def __init__(self, params, root):

        # load project params
        for key in params.keys():
            self.__dict__[key] = params[key]

        # project directory
        self.pDir = os.path.join(root, 'data', 'project', str(self.id))

        # uncompress files
        self.genome = self._gunzip(os.path.join(self.pDir, self.genome))
        self.rnaseq = self._gunzip(os.path.join(self.pDir, self.rnaseq))
        if self.masked:
            self.masked = self._gunzip(os.path.join(self.pDir, self.masked))
        if self.mrna:
            self.mrna = self._gunzip(os.path.join(self.pDir, self.mrna))

        # create (reset) assembly directory
        self.asmDir = os.path.join(self.pDir, 'asm')
        if os.path.exists(self.asmDir):
            shutil.rmtree(self.asmDir)
        os.mkdir(self.asmDir)

        # create (reset) mapping directory
        self.mapDir = os.path.join(self.pDir, 'map')
        if os.path.exists(self.mapDir):
            shutil.rmtree(self.mapDir)
        os.mkdir(self.mapDir)

        # create (reset)  snowyowl output directory
        self.soDir = os.path.join(self.pDir, 'so')
        if os.path.exists(self.soDir):
            shutil.rmtree(self.soDir)
        os.mkdir(self.soDir)

        # create project output save directory
        self.outDir = os.path.join(self.pDir, 'out')
        if os.path.exists(self.outDir):
            shutil.rmtree(self.outDir)
        os.mkdir(self.outDir)

        # initialize logs
        self.sLog = os.path.join(self.pDir, time.strftime("snowyowl.%Y-%b-%d.%H.%M.log"))
        self.pLog = self._init_log() # project log filename

    def _init_log(self):
        """
        Initialise project logger.
        """

        filename = os.path.join(self.pDir, time.strftime("soapp.%Y-%b-%d.%H.%M.log"))
        log = logging.getLogger('soproject')
        formatter = logging.Formatter('[%(asctime)s] %(levelname)s: %(message)s')
        handler = logging.FileHandler(filename)
        #handler.createLock()
        handler.setFormatter(formatter)
        log.addHandler(handler)
        log.setLevel(logging.INFO)
        return filename

    def _gunzip(self, filename):
        """
        Decompress a filename if it ends with a '.gz' extention.

        :param filename: a path to a file
        :param type: string

        :returns: the path the decompressed file
        :rtype: string
        """

        basename, ext = os.path.splitext(filename)
        if ext == '.gz':
            with gzip.open(filename) as f_in:
                with open(basename, 'w') as f_out:
                    f_out.writelines(f_in)
            return basename

        return filename

class SOProjectManager:
    """
    Runs projects.

    Uses parameters and paths provided by SOProject to decide which steps of the pipeline to run.
    It is also responsible for saving project output and removing intermediate files.
    """

    def __init__(self, root, cfg, db):

        self.root = root
        self.cfg = cfg
        self.db = db

        self.snowHome = self.cfg.get('daemon', 'daemon_sohome') # SnowyOwl pipeline install directory
        self.np = self.cfg.getint('daemon', 'daemon_nproc')     # Maximum number of cpus/cores to use
        self.dc_mach = self.cfg.get('daemon', 'daemon_dcmach')  # TimeLogic Decypher machine to use

        self.child = None    # SnowyOwl pipline watchdog process
        self.host = SOHost() # host executing external commands

        self.project = None  # current project
        self.save = {}       # files to save
        self.logs = None     # to hold zipfile object for logs

    def run(self, project_id):

        #########
        # setup #
        #########

        log = logging.getLogger('root')

        # reset progress
        log.debug('reseting project progress status')
        self.db.query("DELETE FROM core_project_progress WHERE project_id = %s", [project_id])

        # load project input parameters
        log.debug('loading project data')
        sql = """ SELECT core_project.id,
                         core_project.title,
                         core_project_param.*,
                         core_refseq.name AS refseq_name
                  FROM core_project
                  JOIN core_project_param
                    ON core_project.id = core_project_param.project_id
                  JOIN core_refseq
                    ON core_project_param.refseq = core_refseq.id
                  WHERE core_project.id = %s """
        params = self.db.selectOne(sql, [project_id], PgSQL.FETCH_ASSOC)
        self.project = SOProject(params, self.root)
        del params

        path = os.path.join(self.project.soDir, 'logs.zip')
        self.logs = zipfile.ZipFile(path, 'w')
        self.save['logs.zip'] = path

        #######
        # run #
        #######

        # mask genome if necessary
        if self.project.maskseq:
            log.info('masking genome ...')
            self._mask_genome()

        # assemble transcripts if necessary
        if not self.project.mrna:
            log.info('assembling transcripts ...')
            self._assemble_transcripts()

        # generate read mapping files
        if self.project.tophat:
            log.info('generating mapping files (Tophat)')
            self._generate_mapping_files_using_tophat()
        else:
            log.info('generating mapping files (Tuque)')
            self._generate_mapping_files_using_tuque()

        self.save['tuque.coverage.wig.gz'] = os.path.join(self.project.mapDir, 'tuque.coverage.wig.gz')
        self.save['classified.juncs.gz'] = os.path.join(self.project.mapDir, 'classified.juncs.gz')

        # run snowyowl and watch its progress
        log.info('running SnowyOwl pipeline')
        self.child = multiprocessing.Process(target=self._check_snowyowl_progress)
        self.child.start()
        self._run_snowyowl()
        self.child.terminate()

        # save logs
        self.logs.write(self.project.pLog, os.path.basename(self.project.pLog))
        self.logs.close()

        sql = """ SELECT project_file.filename
                  FROM project_file
                  JOIN core_project_file ON project_file.id = core_project_file.file_id
                  WHERE core_project_file.project_id = %s """
        filenames = [x[0] for x in self.db.select(sql, [project_id])]

        # save output files
        log.info('saving output files')
        for basename, src in self.save.items():
            log.debug("name: '%s' source (exists): '%s' (%s)" % (basename, src,os.path.exists(src)))
            if basename in filenames and os.path.exists(src):
                dst = os.path.join(self.project.outDir, basename)
                os.rename(src, dst)
                self.db.query("""
                    UPDATE core_project_file
                      SET size = %s
                      WHERE project_id = %s
                      AND file_id = (
                        SELECT id FROM project_file WHERE filename = %s )""",
                              [os.path.getsize(dst), self.project.id, basename])

        #
        # remove intermediate files
        #
        log.info('removing intermediate output files')
        for dirpath in [self.project.asmDir, self.project.mapDir, self.project.soDir]:
            try:
                shutil.rmtree(dirpath)
            except:
                log.warn('encountered an error while deleting directory: "%s". ignoring error.' % dirpath)
                log.debug(traceback.format_exc())

        # keep logs for reference (do not delete logs)
        # do not remove user input

        # done
        self.db.query("INSERT INTO core_project_progress VALUES (%s, 9)", [self.project.id])

    def terminate(self):

        # kill child process
        if self.child and self.child.is_alive():
            self.child.terminate()

        # kill external command running
        self.host.terminate()

    def _mask_genome(self):

        log = logging.getLogger('soproject')

        # decypher
        tpl = os.path.join(self.root, 'dc', 'dna.blast.tab')
        target = "snowyowl_%s" % os.path.basename(self.project.genome)
        desc = time.strftime('"SnowyOwl %Y-%b-%d.%H.%M"')
        tab = tempfile.mkstemp(prefix='snowyowl_mask_')[1]

        # create temporary target
        cmd = 'dc_new_target_rt -template format_nt_into_nt -source %s -targ %s -desc %s -mach %s' % (
            self.project.genome, target, desc, self.dc_mach)
        self.host.run(cmd, cwd=self.project.pDir)

        # run search
        cmd = 'dc_template_rt -template %s -target %s -query %s -mach %s > %s' % (
            tpl, target, self.project.masked, self.dc_mach, tab)
        self.host.run(cmd, cwd=self.project.pDir, shell=True)

        #
        # parse search results
        #

        # load sequences into a hash
        # key: sequence identifier (string)
        # value: sequence bytearray (mutable)
        recs = {}
        with open(self.project.genome) as fh:
            for rec in SimpleFastaParser(fh):
                seqid = rec[0].split()[0]
                recs[seqid] = bytearray(rec[1])

        n_seq = 0 # sequence counter
        n_len = 0 # total sequence length

        # mask sequence in hash
        # using tab delimited output from Timelogic DeCypher
        with open(tab) as fh:
            for row in csv.DictReader(fh, delimiter='\t'):
                if int(row['PERCENTALIGNMENT']) >= self.project.maskseq:
                    seqid = row['TARGETLOCUS']
                    start = int(row['TARGETSTART']) - 1
                    end = int(row['TARGETEND'])
                    recs[seqid][start:end] = 'N' * (end - start)
                    n_seq += 1
                    n_len += (end - start)

        # set as project masked genome and save to disk
        self.project.masked = "%s.masked.fa" % self.project.genome
        with open(self.project.masked, 'w') as fh:
            for seqid in recs.keys():
                fh.write(">%s\n%s\n" % (seqid, recs[seqid]))

        log.info('masked %d bases in %d sequences' % (n_len, n_seq))

        # delete search results
        os.unlink(tab)

        # remove target
        cmd = 'dc_delete_target_rt -targ %s -mach %s' % (target, self.dc_mach)
        self.host.run(cmd, cwd=self.project.pDir)

        # save masked genome
        self.save['masked_target.fa'] = self.project.masked


    def _assemble_transcripts(self):

        # todo: clean read sequences (quality trimming, etc)

        # run Trinity
        cmd = 'Trinity.pl --seqType fq --CPU %d --JM 128G --single %s' % (self.np, self.project.rnaseq)
        self.host.run(cmd, cwd=self.project.asmDir)
        self.db.query("INSERT INTO core_project_progress VALUES (%s, 1)", [self.project.id])

        self.project.mrna = os.path.join(self.project.asmDir, 'trinity_out_dir', 'Trinity.fasta')

        # save assembled transcripts
        self.save['Trinity.fa'] = self.project.mrna

    def _generate_mapping_files_using_tophat(self):

        bam_filename = 'accepted_hits.bam'

        # prepare Bowtie reference database
        cmd = 'bowtie-build %s btref' % self.project.genome
        self.host.run(cmd, cwd=self.project.mapDir)

        imin = self.project.imin
        imax = self.project.imax

        # run TopHat
        opts = "-p %d -i %d -I %d --max-insertion-length 0 --max-deletion-length 0 -g 25 --microexon-search" \
               " --min-segment-intron %d --max-segment-intron %d" % (self.np, imin, imax, imin, imax)
        if self.project.phred64:
            opts += " --phred64-quals"
        cmd = "tophat %s btref %s" % (opts, self.project.rnaseq)
        self.host.run(cmd, cwd=self.project.mapDir)
        self.db.query("INSERT INTO core_project_progress VALUES (%s, 2)", [self.project.id])

        # create mapping files
        os.link(os.path.join(self.project.mapDir, 'tophat_out', bam_filename), os.path.join(self.project.mapDir, bam_filename))
        bam_to_juncs = os.path.join(self.snowHome, 'bin', 'Merge_bin', 'scripts', 'BAM_to_juncs_and_coverage.sh')
        cmd = "%s %s %s" % (bam_to_juncs, bam_filename, self.project.genome)
        self.host.run(cmd, cwd=self.project.mapDir)
        self.db.query("INSERT INTO core_project_progress VALUES (%s, 3)", [self.project.id])

    def _generate_mapping_files_using_tuque(self):

        #
        # split fastq
        #

        tmpDir = os.path.join(self.project.mapDir, 'tuque')
        if not os.path.exists(tmpDir):
            os.mkdir(tmpDir)

        basepath = os.path.join(tmpDir, os.path.basename(self.project.rnaseq))

        # open output files
        files = []
        for i in xrange(self.np):
            files.append(open("%s.%d" % (basepath, i + 1), 'w'))

        # distribute fastq records over the open output files
        with open(self.project.rnaseq, 'rU') as fastq:
            for i, rec in enumerate(FastqGeneralIterator(fastq)):
                files[i % self.np].write("@%s\n%s\n+\n%s\n" % rec)

        # close output files
        for file in files:
            file.close()

        #
        # create mapping files
        #

        # run Tuque command
        cmd = "tuqueSplice -g %s -o %s -n %d %s" % (self.project.genome, tmpDir, self.np, basepath)
        self.host.run(cmd, cwd=self.project.mapDir)

        # save mapping and coverage files
        for filename in ['classified.juncs.gz', 'classified.juncs.gz.tbi',
                         'tuque.coverage.wig.gz', 'tuque.coverage.wig.gz.tbi']:
            os.rename(os.path.join(tmpDir, filename), os.path.join(self.project.mapDir, filename))

        # clear other tuque output
        shutil.rmtree(tmpDir)

        # update progress
        self.db.query("INSERT INTO core_project_progress VALUES (%s, 2)", [self.project.id])
        self.db.query("INSERT INTO core_project_progress VALUES (%s, 3)", [self.project.id])

    def _run_snowyowl(self):

        snowyowl = os.path.join(self.snowHome, 'snowyowl.pl')
        refseq = 'so_refseq_%s' % self.project.refseq_name

        opts = "-dc -np %d -p PID%d -g %s -r %s -t %s -m %s -dc_blastp_targ %s -imin %d -imax %d" % (
            self.np, self.project.id, self.project.genome, self.project.rnaseq,
            self.project.mrna, self.project.mapDir, refseq, self.project.imin, self.project.imax)

        if self.project.masked:
            opts += " -n %s" % self.project.masked

        cmd = "%s %s -outdir %s &>%s" % (snowyowl, opts, self.project.soDir, self.project.sLog)
        self.host.run(cmd, cwd=self.project.pDir, shell=True)

        # save predictions
        outDir = os.path.join(self.project.soDir, 'Merge_files', 'SnowyOwl', 'Predictions')
        self.save['accepted.gff3'] = os.path.join(outDir, 'accepted.gff3')
        self.save['accepted.CDS.fna'] = os.path.join(outDir, 'accepted.CDS.fna')
        self.save['accepted.Proteins.faa'] = os.path.join(outDir, 'accepted.Proteins.faa')
        self.save['representatives.gff3'] = os.path.join(outDir, 'representatives.gff3')

        # save other files
        self.save['Contig_models.gff'] = os.path.join(self.project.soDir, 'Contigs', 'Contig_models.gff')
        self.save['genemark_hmm.gtf'] = os.path.join(self.project.soDir, 'GeneMark', 'genemark_hmm.gtf')
        self.save['Consensus_Training_Set.gb'] = os.path.join(self.project.soDir, 'Consensus_Training_Set.gb')
        self.save['Consensus_Training_Set.gff'] = os.path.join(self.project.soDir, 'Consensus_Training_Set.gff')
        self.save['Augustus_Pooled_Models.gff3'] = os.path.join(self.project.soDir, 'Augustus_Pooled_Models.gff3')

        # save logs
        self.logs.write(self.project.sLog, os.path.basename(self.project.sLog))
        for path in ['make_Consensus_training.log',
                     'make_Pooled_models.log',
                     'make_SO_Project.log',
                     'SnowyOwl.log',
                     'AugustusBlatHints/blat2hints.log',
                     'AugustusBlatHints/Hinted_Augustus.log',
                     'AugustusBlatHints/make_Augustus_hints.log',
                     'Unhinted_Augustus_Models/Unhinted_Augustus.log',
                     'Contigs/make_Contig_training.log',
                     'GeneMark/make_GeneMark_training.log',
                     'Merge_files/model_scoring.log',
                     'Merge_files/RNA-Seq/islands2genes.log',
                     'Merge_files/SnowyOwl/predict_genes.log',
                     'Merge_files/SnowyOwl/stderr.log']:
            src = os.path.join(self.project.soDir, path)
            if os.path.exists(src):
                self.logs.write(src, path)

    def _check_snowyowl_progress(self):
        """
        Check progress of SnowyOwl pipeline.

        Checks for success files to detect progress.
        Function terminates when all the success files are generated by snowyowl.
        """

        progress = { 4: os.path.join('Contigs', 'make_Contig_training.success'),
                     5: os.path.join('GeneMark', 'make_GeneMark_training.success'),
                     6: os.path.join('AugustusBlatHints', 'make_Augustus_hints.success'),
                     7: 'make_Consensus_Training.success',
                     8: 'make_Pooled_models.success' }

        while progress:
            for progress_id, filename in progress.items():
                filepath = os.path.join(self.project.soDir, filename)
                if os.path.exists(filepath):
                    del progress[progress_id]
                    self.db.query("INSERT INTO core_project_progress VALUES (%s, %s)", [self.project.id, progress_id])

class SOApp(Daemon):
    """
    Main application class.

    Processes the job queue.
    Responsible for updating job status and catching errors.
    Also handles email notification and the deletion of expired projects and their files.
    """
    
    def __init__(self, root):

        self.root = root # global application root path
        self.cfg = SOConfigParser()

        # load global configuration (shared with PHP)
        filename = os.path.join(root, 'config.ini')
        with open(filename) as fp:
            self.cfg.readfp(fp, filename)

        # switch user group
        group_name = self.cfg.get('daemon', 'daemon_group')
        gid = grp.getgrnam(group_name)[2]
        try:
            os.setgid(gid) # will raise error if the (unpriviliged) user did not switch to snowyowl group
        except OSError, e:
            if e.errno == 1:
                print "You do not have permission to run SnowyOwl."
                sys.exit(1)
            raise

        # set umask (no public access)
        os.umask(007)

        # set timezone
        os.environ['TZ'] = self.cfg.get('global', 'timezone')
        time.tzset()

        # call base class constructor
        pidfile = self.cfg.get('daemon', 'daemon_pidfile')
        Daemon.__init__(self, pidfile)

        # set up logger
        self._init_log()

    def _init_log(self):
        """
        Initialises the daemon log.
        """

        log = logging.getLogger('root')
        formatter = logging.Formatter('[%(asctime)s] %(levelname)s: %(message)s')
        handler = logging.FileHandler(self.cfg.get('daemon', 'daemon_logfile'))
        handler.setFormatter(formatter)
        log.addHandler(handler)
        log.setLevel(logging.INFO)
        if self.cfg.getboolean('global', 'debug'):
            log.setLevel(logging.DEBUG)


    def _query(self, sql, params=[]):
        """
        Submit query to database
        """
        log = logging.getLogger('root')
        log.debug("SQL statement: %s | params: %s", sql, params)
        self.db.query(sql, params)

    def _check_projects(self):
        """
        Delete expired projects and their files (including input files).
        Also removes files for projects deleted through the web interface.
        """

        log = logging.getLogger('root')

        # delete expired projects
        #
        # A project expires if they have been completed for over a certain period.
        # If the project is not complete or currently running, it expires when
        # it is older than the specified period.
        sql = """SELECT core_project.id
                 FROM core_project
                 JOIN core_project_status
                    ON core_project.id = core_project_status.project_id
                 WHERE (core_project_status.status = 4 -- completed
                    AND core_project_status.completed_on + '30 days' < CURRENT_TIMESTAMP)
				    OR (core_project_status.status IN (0,2,3) -- not running nor completed
				    AND core_project.created_on  + '30 days' < CURRENT_TIMESTAMP)"""
        for expired_id, in self.db.select(sql):
            log.info("delete expired project <%s>" % expired_id)
            try:
                # delete project
                self._query('SELECT delete_project(%s)', [expired_id])
            except Exception, e:
                log.warn("failed to delete expired project <%s>: %s" % (expired_id, e))

        # remove deleted project files
        sql = "SELECT project_id FROM project_deleted"
        for deleted_id, in self.db.select(sql):
            log.info("delete project <%s> files" % deleted_id)
            pDir = os.path.join(self.root, 'data', 'project', str(deleted_id))
            if os.path.exists(pDir):
                try:
                    log.debug("remove directory %s" % pDir)
                    shutil.rmtree(pDir)
                except Exception, e:
                    log.warn("failed to remove project <%s> files: %s" % (deleted_id, e))
                else:
                    # removed sucessfully
                    self._query("DELETE FROM project_deleted WHERE project_id = %s", [deleted_id])
            else:
                # project files do not exist
                self._query("DELETE FROM project_deleted WHERE project_id = %s", [deleted_id])

    def _sendmail(self, to, subject, text):
        """
        Send a text email message.

        :param to: a (name, email_address) tuple
        :type to: tuple

        :param subject: email subject
        :type subject: string

        :param text: email message text
        :type text: string
        """

        log = logging.getLogger('root')

        # connect to smtp server
        try:
            hostname = self.cfg.get('email', 'email_smtp')
            username = self.cfg.get('email', 'email_user')
            password = self.cfg.get('email', 'email_pass')
            tls = self.cfg.getboolean('email', 'email_tls')
            smtp = smtplib.SMTP(hostname)
            if tls:
                smtp.starttls()
            if username:
                smtp.login(username, password)
        except:
            log.warn('failed to connect to the mail server: "%s" as user: "%s"' % (hostname, username))
            log.debug(traceback.format_exc())
            smtp.quit()

        # email addresses
        email_from = self.cfg.get('email', 'email_from')
        email_to = to[1]

        # email message
        msg = MIMEText(text)
        msg['Subject'] = subject
        msg['From'] = '"%s" <%s>' % (self.cfg.get('email', 'email_name'), email_from)
        msg['To'] = '"%s" <%s>' % to

        # send email
        try:
            smtp.sendmail(email_from, [email_to], str(msg))
        except:
            log.warn('failed to connect to the mail server')
            log.debug(traceback.format_exc())
        else:
            log.debug('sent email to %s: "%s"' % (msg['To'], msg['Subject']))
        finally:
            smtp.quit() # alway quit connection

    def run(self):
        """
        SnowyOwl daemon main loop.
        """

        log = logging.getLogger('root')
        log.info("SnowyOwl daemon process started.")

        # connect to database
        log.debug("trying to connect to database ...")
        self.db = PgSQL(
            self.cfg.get('db', 'db_host'),
            self.cfg.get('db', 'db_name'),
            self.cfg.get('db', 'db_cmduser'),
            self.cfg.get('db', 'db_cmdpass')
        )
        log.info("connected to database: %s." % self.db.dsn())

        # initialize project manager
        pManager = SOProjectManager(self.root, self.cfg, self.db)

        # main application loop
        while True:

            #
            # check for deleted or expired projects
            #
            try:
                self._check_projects()
            except:
                log.warn("error while checking for expired/deleted projects")
                log.debug(traceback.format_exc())

            #
            # process pending projects
            #

            sql = """ SELECT core_project.id,
                             core_project.title,
                             auth_user.name,
                             auth_user.email,
                             auth_user.notify
                      FROM core_project
                      JOIN core_project_status ON core_project.id = core_project_status.project_id
                      JOIN auth_user ON core_project.created_by = auth_user.id
                      WHERE core_project_status.status = %s
                      ORDER BY core_project.created_on ASC """
            for project_id, title, name, email, notify in self.db.select(sql, (P_PENDING,)):

                try:

                    # process project
                    log.info("processing project: '%s' <%d>" % (title, project_id))

                    # update status to running
                    sql = """ UPDATE core_project_status
                                    SET status = %s, started_on = CURRENT_TIMESTAMP
                                    WHERE project_id = %s """
                    self._query(sql, (P_RUNNING, project_id))

                    # notify by email
                    if notify:
                        self._sendmail((name, email),
                                       "Project: '%s' <%d> status changed to 'Running'" % (title, project_id),"")

                    # run selected project
                    pManager.run(project_id)

                except SystemExit:
                    # stopped
                    log.warn("SIGTERM relieved while processing project: '%s' project ID: %d" % (title, project_id))
                    pManager.terminate()
                    sql = "UPDATE core_project_status SET status = %s WHERE project_id = %s"
                    self._query(sql, (P_STOPPED, project_id))
                    if notify:
                        self._sendmail((name, email),
                                       "Project: '%s' <%d> status changed to 'Stopped'" % (title, project_id),"")
                    sys.exit(0) # daemon process received a SIGTERM, exit!


                except:
                    # error
                    log.warn("error while processing project: '%s' project ID: %d" % (title, project_id))
                    log.debug(traceback.format_exc())
                    pManager.terminate()
                    sql = "UPDATE core_project_status SET status = %s WHERE project_id = %s"
                    self._query(sql, (P_ERROR, project_id))
                    if notify:
                        self._sendmail((name, email),
                                       "Project: '%s' <%d> status changed to 'Error'" % (title, project_id),"")

                else:
                    # success
                    log.info("project: '%s' project ID: %d completed successfully" % (title, project_id))
                    sql = """ UPDATE core_project_status
                                SET status = %s, completed_on = CURRENT_TIMESTAMP
                                WHERE project_id = %s """
                    self._query(sql, (P_COMPLETED, project_id))
                    if notify:
                        self._sendmail((name, email),
                                       "Project: '%s' <%d> status changed to 'Completed'" % (title, project_id),"")

                finally:
                    pass # placeholder for now

            time.sleep(60)

