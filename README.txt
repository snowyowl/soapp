SnowyOwl Web Application
========================

:Author: Omar A. Zabaneh
:Organization: University of Calgary
:Contact: ozabaneh@ucalgary.ca

SnowyOwl Web Application provides a public interface for registered users to access
the SnowyOwl infrastructure. The application manages projects and integrates
different tools with the SnowyOwl pipeline through a simplified interface.

Users can monitor job progress and download results upon completion.
There is a plan to add a benchmarking module to estimate completion time.

For more details refer to /doc or the code repository wiki
at https://bitbucket.org/snowyowl/soapp/wiki/Home.

Software Rquirements
--------------------

PHP 5.3 (pgsql, json)
Python 2.6 (biopython, argparse, psycopg2)
SnowyOwl 1.7 (see README at https://bitbucket.org/snowyowl/snowyowl/src for requirements)
Tuque (http://sourceforge.net/p/tuque/code/ci/master/tree/)
Tophat v1.3.1
Trinity (Trinity-r2012-10-05)
Decypher Command Line Client V 8.0.4.0

Code Structure
--------------

directory               description                   notes
---------------------   ---------------------------   -------------------------------
/bin                    command line tools            should be included in $PATH
/bin/admin              app admin scripts             dont export path, run from here
/www                    web server document root      contains PHP "interface" files
/www/js                 javascript modules
/www/css                css styles
/www/images             images
/lib/py                 Python libraries
/lib/php                PHP libraries
/tpl                    templates (html, etc)
/config.example.ini     example site configuration    copy to config.ini locally
/init.php               PHP global initialization
/sql                    PgSQL scripts                 database initialization
/doc                    user and developer docs
/data                   output save directory         must be writable by the app
/log                    application logs              web, daemon, and database logs
/upload                 user data upload directories
